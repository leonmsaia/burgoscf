var main_path = $('#main_path').html();

$('#share').jsSocials({
    showLabel: false,
    showCount: false,
    shareIn: 'popup',
    shares: [
      {
        share: 'email',
        logo: main_path + 'assets/img/logos/email.svg'
      },
      {
        share: 'twitter',
        logo: main_path + 'assets/img/logos/twitter.svg'
      },
      {
        share: 'facebook',
        logo: main_path + 'assets/img/logos/facebook.svg'
      },
      {
        share: 'whatsapp',
        logo: main_path + 'assets/img/logos/whatsapp.svg'
      }
    ]
});