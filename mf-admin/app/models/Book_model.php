<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Book_model extends CI_Model
{

  // Categories Methods

  public function getAllCategories()
  {
    $this->db->where('book_categorie_status', 1);
    $result = $this->db->get('book_categorie');
    return $result;
  }

  public function getSubcategoriesByCategorieByID($category_id)
  {
    $this->db->where('book_subcategorie_categorie_id', $category_id);
    $this->db->where('book_subcategorie_status', 1);
    $result = $this->db->get('book_subcategorie');
    return $result;
  }

  public function getPricesCategories()
  {
    $this->db->where('book_price_category_status', 1);
    $result = $this->db->get('book_price_category');
    return $result;
  }

  public function getCurrency()
  {
    $this->db->where('currency_status', 1);
    $result = $this->db->get('currency_list');
    return $result;
  }

  public function getAllRequest()
  {
    $this->db->join('currency_list', 'currency_list.currency_id = cart_interaction_sale.cart_interaction_sale_amount_currency');
    $this->db->join('cart_payment_methods', 'cart_payment_methods.cart_payment_methods_id = cart_interaction_sale.cart_interaction_sale_pay_method');
    $this->db->join('users', 'users.id = cart_interaction_sale.cart_interaction_sale_user_id');
    $result = $this->db->get('cart_interaction_sale');
    return $result;
  }

  public function getAllRequestByStatus($status)
  {
    $this->db->join('currency_list', 'currency_list.currency_id = cart_interaction_sale.cart_interaction_sale_amount_currency');
    $this->db->join('cart_payment_methods', 'cart_payment_methods.cart_payment_methods_id = cart_interaction_sale.cart_interaction_sale_pay_method');
    $this->db->join('users', 'users.id = cart_interaction_sale.cart_interaction_sale_user_id');
    $this->db->where('cart_interaction_sale_status', $status);
    $result = $this->db->get('cart_interaction_sale');
    return $result;
  }

  public function getRequestByOrder($order)
  {
    $this->db->join('currency_list', 'currency_list.currency_id = cart_interaction_sale.cart_interaction_sale_amount_currency');
    $this->db->join('cart_payment_methods', 'cart_payment_methods.cart_payment_methods_id = cart_interaction_sale.cart_interaction_sale_pay_method');
    $this->db->join('users', 'users.id = cart_interaction_sale.cart_interaction_sale_user_id');
    $this->db->where('cart_interaction_sale_order', $order);
    $result = $this->db->get('cart_interaction_sale');
    return $result;
  }

  public function getRequestSoldRelationByOrder($order)
  {
    $this->db->join('book_item', 'book_item.book_item_id = cart_interaction_sold_relation.cart_interaction_sold_relation_product_id');
    $this->db->where('cart_interaction_sold_relation_order', $order);
    $result = $this->db->get('cart_interaction_sold_relation');
    return $result;
  }

  public function getRequestSoldNoteByOrder($order)
  {
    $this->db->where('cart_interaction_sale_note_order', $order);
    $result = $this->db->get('cart_interaction_sale_note');
    return $result;
  }

  
}
