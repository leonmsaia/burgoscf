<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$route['default_controller'] = 'home';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;



// ISBN Automatic Search
$route['givemeisbn/(:any)'] = "Crud/book_isbn_search_engine/$1";

// Get Subcategories by Categorie ID
$route['givemecategories/(:any)'] = "Crud/get_subcategories_by_category_id/$1";