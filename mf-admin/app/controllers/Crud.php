<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Crud extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->helper('url');
		$this->load->library('grocery_CRUD');
		$this->load->library('image_CRUD');
	}

	public function renderCrud($output = null)
	{
		if ($this->ion_auth->logged_in()) {
			$this->load->view('example.php',$output);
		}else{
			redirect('/auth/login');
		}
	}

	public function index()
	{
		$this->renderCrud((object)array('output' => '' , 'js_files' => array() , 'css_files' => array()));
	}

	// Statics Pages
	public function static_pages_crud()
	{
	  try{
	    $crud = new grocery_CRUD();
	    $crud->set_table('static_pages');
	    $crud->set_subject('Pagina');
	    $crud->set_field_upload('static_pages_main_image','../assets/bucket/cover');
	    $crud->set_field_upload('static_pages_main_image_2','../assets/bucket/cover');
	    $crud->set_field_upload('static_pages_main_image_3','../assets/bucket/cover');
	    $crud->set_field_upload('static_pages_main_image_4','../assets/bucket/cover');
	    $crud->set_field_upload('static_pages_main_image_5','../assets/bucket/cover');
	    $crud->display_as('static_pages_title','Titulo (Meta)')
	         ->display_as('static_pages_slug','Slug (URL)')
	         ->display_as('static_pages_desc','Descripcion (Meta)')
	         ->display_as('static_pages_keywords','Keywords (Meta)')
	         ->display_as('static_pages_main_title','Encabezado')
	         ->display_as('static_pages_main_image','Imagen Principal')
	         ->display_as('static_pages_main_image_2','Imagen Principal 2 - (Activa Slide)')
	         ->display_as('static_pages_main_image_3','Imagen Principal 3 - (Activa Slide)')
	         ->display_as('static_pages_main_image_4','Imagen Principal 4 - (Activa Slide)')
	         ->display_as('static_pages_main_image_5','Imagen Principal 5 - (Activa Slide)')
			 ->display_as('static_pages_main_text','Cuerpo de Texto')
			 ->display_as('static_pages_map_lat','Mapa - Latitud')
			 ->display_as('static_pages_map_lng','Mapa - Longitud')
			 ->display_as('static_pages_map_zoom','Mapa - Zoom')
			 ->display_as('static_pages_soundcloud_id','ID de Soundcloud')
			 ->display_as('static_pages_navbar_appear','¿Aparece en Menú?')
			 ->display_as('static_pages_navbar_parent','Menú Padre')
			 ->display_as('static_pages_type','Tipo de Pagina')
			 ->display_as('static_pages_status','Estado de Pagina');
		$crud->set_relation('static_pages_navbar_parent','static_pages_navbar','static_pages_navbar_title');
		$crud->field_type(
 	      'static_pages_status',
 	      'dropdown',
 	      array(
 	        '0' => 'Desactivada',
 	        '1' => 'Activada'
 	      )
 	    );
 	    $crud->field_type(
 	      'static_pages_type',
 	      'dropdown',
 	      array(
 	        '1' => 'Página General',
 	        '2' => 'Página Legal'
 	      )
 	    );
 	    $crud->field_type(
 	      'static_pages_navbar_appear',
 	      'dropdown',
 	      array(
 	        '0' => 'No',
 	        '1' => 'Si'
 	      )
 	    );
		$crud->callback_after_insert(array($this, 'gen_static_page_slug'));
		$crud->callback_after_update(array($this, 'gen_static_page_slug'));
	    $output = $crud->render();
	    $this->renderCrud($output);
	  }catch(Exception $e){
	    show_error($e->getMessage().' --- '.$e->getTraceAsString());
	  }
	}
	public function gen_static_page_slug($post_array, $primary_key){
	  $this->load->helper('url');
	  $name = normaliza($post_array['static_pages_title']);
	  $url_title = url_title($name, '-', TRUE);
	  $data = array(
	  	'static_pages_slug' => $url_title
	  );
	  $this->db->where('static_pages_id', $primary_key);
	  $this->db->update('static_pages',$data);
	  return true;
	}
	public function static_pages_holder()
	{
	  if ($this->ion_auth->logged_in()) {
	  	if ($this->ion_auth->in_group(1)) {
		    $data['url'] = 'static_pages_crud';
		    $data['title'] = 'Gestión de Páginas';
		    $data['desc'] = 'Seccion de creacion, modificacion y eliminacion de Páginas en Base de Datos.';
		    $data['secondCrud'] = FALSE;
		    $data['url_second'] = '';
		    $this->load->view('v2/pages/crud-holder', $data);
		}else{
			$this->ion_auth->logout();
			redirect('/auth/login');	
		}
	  }else{
	    redirect('/auth/login');
	  }
	}

	public function static_pages_navbar_crud()
	{
	  try{
	    $crud = new grocery_CRUD();
	    $crud->set_table('static_pages_navbar');
	    $crud->set_subject('Elemento de Menú');
	    $crud->display_as('static_pages_navbar_title','Titulo')
	         ->display_as('static_pages_navbar_slug','Slug (URL)')
	         ->display_as('static_pages_navbar_order','Orden')
	         ->display_as('static_pages_navbar_main_element','Elemento Principal')
	         ->display_as('static_pages_navbar_dropdown','Desplegable')
	         ->display_as('static_pages_navbar_parent','Menú Padre')
			 ->display_as('static_pages_navbar_status','Estado');
		$crud->set_relation('static_pages_navbar_parent','static_pages_navbar','static_pages_navbar_title');
 	    $crud->field_type(
 	      'static_pages_navbar_dropdown',
 	      'dropdown',
 	      array(
 	        '0' => 'No',
 	        '1' => 'Si'
 	      )
 	    );
 	    $crud->field_type(
 	      'static_pages_navbar_main_element',
 	      'dropdown',
 	      array(
 	        '0' => 'No',
 	        '1' => 'Si'
 	      )
 	    );
 	    $crud->field_type(
 	      'static_pages_navbar_status',
 	      'dropdown',
 	      array(
 	        '0' => 'No',
 	        '1' => 'Si'
 	      )
 	    );
	    $output = $crud->render();
	    $this->renderCrud($output);
	  }catch(Exception $e){
	    show_error($e->getMessage().' --- '.$e->getTraceAsString());
	  }
	}

	public function static_pages_navbar_holder()
	{
	  if ($this->ion_auth->logged_in()) {
	  	if ($this->ion_auth->in_group(1)) {
		    $data['url'] = 'static_pages_navbar_crud';
		    $data['title'] = 'Gestión de Elementos de Menú';
		    $data['desc'] = 'Seccion de creacion, modificacion y eliminacion de Elementos de Menú en Base de Datos.';
		    $data['secondCrud'] = FALSE;
		    $data['url_second'] = '';
		    $this->load->view('v2/pages/crud-holder', $data);
		}else{
			$this->ion_auth->logout();
			redirect('/auth/login');	
		}
	  }else{
	    redirect('/auth/login');
	  }
	}

	public function blog_home_suit_config_crud()
	{
	  try{
	    $crud = new grocery_CRUD();
	    $crud->set_table('blog_home_suit_config');
	    $crud->set_subject('Configuracion de Home');
	    $crud->display_as('blog_home_suit_config_single_category','Categoria de Nota Simple')
	         ->display_as('blog_home_suit_config_two_column_category','Categoria de Nota Doble')
	         ->display_as('blog_home_suit_config_two_column_limit','Cantidad en Nota Doble')
	         ->display_as('blog_home_suit_config_three_column_category','Categoria en Nota Triple')
	         ->display_as('blog_home_suit_config_three_column_limit','Cantidad en Nota Triple');
 	    $crud->unset_add();
        $crud->unset_delete();
	    $output = $crud->render();
	    $this->renderCrud($output);
	  }catch(Exception $e){
	    show_error($e->getMessage().' --- '.$e->getTraceAsString());
	  }
	}

	public function blog_home_suit_config_holder()
	{
	  if ($this->ion_auth->logged_in()) {
	  	if ($this->ion_auth->in_group(1)) {
		    $data['url'] = 'blog_home_suit_config_crud';
		    $data['title'] = 'Gestión de Notas en Home';
		    $data['desc'] = 'Seccion de modificación de las notas mostradas en la Home. Para utilizar las categorias como filtro, escribir las mismas en minuscula y sin espacios, reemplazar estos ultimos por guiones. Ejemplo: "Redes Sociales" quedaria como "redes-sociales". Evitar el uso de caracteres especiales. Como guia se puede utilizar el SLUG generado por el mismo sistema Wordpress al visualizar la categoria.';
		    $data['secondCrud'] = FALSE;
		    $data['url_second'] = '';
		    $this->load->view('v2/pages/crud-holder', $data);
		}else{
			$this->ion_auth->logout();
			redirect('/auth/login');	
		}
	  }else{
	    redirect('/auth/login');
	  }
	}



	public function team_crud()
	{
	  try{
	    $crud = new grocery_CRUD();
	    $crud->set_table('team');
	    $crud->set_subject('Equipo');
	    $crud->set_field_upload('team_logo','../assets/bucket/team');
	    $crud->display_as('team_name','Nombre de Club')
	         ->display_as('team_stadium_name','Nombre de Estadio')
	         ->display_as('team_logo','Escudo de Equipo')
	         ->display_as('team_division','Division de Equipo')
	         ->display_as('team_country','Pais')
	         ->display_as('team_city','Ciudad')
			 ->display_as('team_points','Puntos Totales')
			 ->display_as('team_points_plays','Partidos Jugados')
			 ->display_as('team_points_win','Partidos Ganados')
			 ->display_as('team_points_tie','Partidos Empatados')
			 ->display_as('team_points_lose','Partidos Perdidos')
			 ->display_as('team_points_goalup','Goles a Favor')
			 ->display_as('team_points_goaldown','Goles en Contra')
			 ->display_as('team_points_goaldif','Diferencia')
			 ->display_as('team_points_status','Estado');
		$crud->field_type(
 	      'team_points_status',
 	      'dropdown',
 	      array(
 	        '0' => 'Desactivado',
 	        '1' => 'Activado'
 	      )
 	    );
 	    $crud->set_relation('team_division','country_soccer_division','country_soccer_division_title');
 	    $crud->set_relation('team_country','localization_country','localization_country_title');
	    $output = $crud->render();
	    $this->renderCrud($output);
	  }catch(Exception $e){
	    show_error($e->getMessage().' --- '.$e->getTraceAsString());
	  }
	}

	public function team_holder()
	{
	  if ($this->ion_auth->logged_in()) {
	  	if ($this->ion_auth->in_group(1)) {
		    $data['url'] = 'team_crud';
		    $data['title'] = 'Gestión de Equipos';
		    $data['desc'] = 'Seccion de creacion, modificacion y eliminacion de Equipos en Base de Datos.';
		    $data['secondCrud'] = FALSE;
		    $data['url_second'] = '';
		    $this->load->view('v2/pages/crud-holder', $data);
		}else{
			$this->ion_auth->logout();
			redirect('/auth/login');	
		}
	  }else{
	    redirect('/auth/login');
	  }
	}

	public function partners_crud()
	{
	  try{
	    $crud = new grocery_CRUD();
	    $crud->set_table('partners');
	    $crud->set_subject('Partner');
	    $crud->set_field_upload('partners_logo','../assets/bucket/partners');
	    $crud->display_as('team_name','Nombre de Club')
	         ->display_as('partners_name','Nombre')
	         ->display_as('partners_desc','Descripcion')
	         ->display_as('partners_link','URL')
	         ->display_as('partners_level','Nivel de Importancia')
	         ->display_as('partners_footer','¿Visible en Footer?')
	         ->display_as('partners_status','Estado');
		$crud->field_type(
 	      'partners_status',
 	      'dropdown',
 	      array(
 	        '0' => 'Desactivado',
 	        '1' => 'Activado'
 	      )
 	    );
 	    $crud->field_type(
 	      'partners_level',
 	      'dropdown',
 	      array(
 	        '1' => '1',
 	        '2' => '2',
 	        '3' => '3',
 	        '4' => '4'
 	      )
 	    );
 	    $crud->field_type(
 	      'partners_footer',
 	      'dropdown',
 	      array(
 	        '0' => 'Desactivado',
 	        '1' => 'Activado'
 	      )
 	    );
	    $output = $crud->render();
	    $this->renderCrud($output);
	  }catch(Exception $e){
	    show_error($e->getMessage().' --- '.$e->getTraceAsString());
	  }
	}

	public function partners_holder()
	{
	  if ($this->ion_auth->logged_in()) {
	  	if ($this->ion_auth->in_group(1)) {
		    $data['url'] = 'partners_crud';
		    $data['title'] = 'Gestión de Partners';
		    $data['desc'] = 'Seccion de creacion, modificacion y eliminacion de Partners en Base de Datos.';
		    $data['secondCrud'] = FALSE;
		    $data['url_second'] = '';
		    $this->load->view('v2/pages/crud-holder', $data);
		}else{
			$this->ion_auth->logout();
			redirect('/auth/login');	
		}
	  }else{
	    redirect('/auth/login');
	  }
	}

	

	public function country_soccer_division_crud()
	{
	  try{
	    $crud = new grocery_CRUD();
	    $crud->set_table('country_soccer_division');
	    $crud->set_subject('Division General');
	    $crud->set_field_upload('country_soccer_division_logo','../assets/bucket/championship');
	    $crud->display_as('country_soccer_division_title','Nombre de Division')
	         ->display_as('country_soccer_division_desc','Descripcion de Division')
	         ->display_as('country_soccer_division_logo','Logo de Division')
	         ->display_as('country_soccer_division_parent_division','Division Padre')
			 ->display_as('country_soccer_division_status','Estado');
		$crud->field_type(
 	      'country_soccer_division_status',
 	      'dropdown',
 	      array(
 	        '0' => 'Desactivado',
 	        '1' => 'Activado'
 	      )
 	    );
 	    $crud->set_relation('country_soccer_division_parent_division','country_soccer_division','country_soccer_division_title');
	    $output = $crud->render();
	    $this->renderCrud($output);
	  }catch(Exception $e){
	    show_error($e->getMessage().' --- '.$e->getTraceAsString());
	  }
	}

	public function country_soccer_division_holder()
	{
	  if ($this->ion_auth->logged_in()) {
	  	if ($this->ion_auth->in_group(1)) {
		    $data['url'] = 'country_soccer_division_crud';
		    $data['title'] = 'Gestión de Divisiones Generales';
		    $data['desc'] = 'Seccion de creacion, modificacion y eliminacion de Divisiones Generales en Base de Datos.';
		    $data['secondCrud'] = FALSE;
		    $data['url_second'] = '';
		    $this->load->view('v2/pages/crud-holder', $data);
		}else{
			$this->ion_auth->logout();
			redirect('/auth/login');	
		}
	  }else{
	    redirect('/auth/login');
	  }
	}

	public function main_club_stats_crud()
	{
	  try{
	    $crud = new grocery_CRUD();
	    $crud->set_table('main_club_stats');
	    $crud->set_subject('Estadisticas Propias');
	    $crud->display_as('main_club_stats_goals','Goles Totales en 1ra')
	         ->display_as('main_club_stats_suscribers','Abonados')
	         ->display_as('main_club_stats_matches','Partidos Jugados en 1ra')
	         ->display_as('main_club_stats_seasons_1','Temporadas en 1ra')
			 ->display_as('main_club_stats_season_2a','Temporadas en 2da A')
			 ->display_as('main_club_stats_season_2ab','Temporadas en 2da B');
 	    $crud->unset_add();
        $crud->unset_delete();
	    $output = $crud->render();
	    $this->renderCrud($output);
	  }catch(Exception $e){
	    show_error($e->getMessage().' --- '.$e->getTraceAsString());
	  }
	}

	public function main_club_stats_holder()
	{
	  if ($this->ion_auth->logged_in()) {
	  	if ($this->ion_auth->in_group(1)) {
		    $data['url'] = 'main_club_stats_crud';
		    $data['title'] = 'Gestión de Estadisticas Propias';
		    $data['desc'] = 'Seccion de modificacion de Estadisticas Propias en Base de Datos.';
		    $data['secondCrud'] = FALSE;
		    $data['url_second'] = '';
		    $this->load->view('v2/pages/crud-holder', $data);
		}else{
			$this->ion_auth->logout();
			redirect('/auth/login');	
		}
	  }else{
	    redirect('/auth/login');
	  }
	}

	public function team_division_crud()
	{
	  try{
	    $crud = new grocery_CRUD();
	    $crud->set_table('team_division');
	    $crud->set_subject('Division General');
	    $crud->set_field_upload('team_division_picture','../assets/bucket/subdivision');
	    $crud->display_as('team_division_name','Nombre de Division')
	         ->display_as('team_division_desc','Descripcion de Division')
	         ->display_as('team_division_picture','Foto de Division')
			 ->display_as('team_division_status','Estado');
		$crud->field_type(
 	      'team_division_status',
 	      'dropdown',
 	      array(
 	        '0' => 'Desactivado',
 	        '1' => 'Activado'
 	      )
 	    );
	    $output = $crud->render();
	    $this->renderCrud($output);
	  }catch(Exception $e){
	    show_error($e->getMessage().' --- '.$e->getTraceAsString());
	  }
	}

	public function team_division_holder()
	{
	  if ($this->ion_auth->logged_in()) {
	  	if ($this->ion_auth->in_group(1)) {
		    $data['url'] = 'team_division_crud';
		    $data['title'] = 'Gestión de Divisiones Internas';
		    $data['desc'] = 'Seccion de creacion, modificacion y eliminacion de Divisiones Internas en Base de Datos.';
		    $data['secondCrud'] = FALSE;
		    $data['url_second'] = '';
		    $this->load->view('v2/pages/crud-holder', $data);
		}else{
			$this->ion_auth->logout();
			redirect('/auth/login');	
		}
	  }else{
	    redirect('/auth/login');
	  }
	}

	public function team_player_position_crud()
	{
	  try{
	    $crud = new grocery_CRUD();
	    $crud->set_table('team_player_position');
	    $crud->set_subject('Division General');
	    $crud->set_field_upload('team_player_position_picture','../assets/bucket/general_inner');
	    $crud->display_as('team_player_position_name','Nombre de Posicion')
	         ->display_as('team_player_position_navbar_parent','Menú Padre')
	         ->display_as('team_player_position_slug','Slug (URL)')
	         ->display_as('team_player_position_picture','Imagen de Posicion')
			 ->display_as('team_player_position_status','Estado');
		$crud->field_type(
 	      'team_player_position_status',
 	      'dropdown',
 	      array(
 	        '0' => 'Desactivado',
 	        '1' => 'Activado'
 	      )
 	    );
 	    $crud->set_relation('team_player_position_navbar_parent','static_pages_navbar','static_pages_navbar_title');
	    $output = $crud->render();
	    $this->renderCrud($output);
	  }catch(Exception $e){
	    show_error($e->getMessage().' --- '.$e->getTraceAsString());
	  }
	}

	public function team_player_position_holder()
	{
	  if ($this->ion_auth->logged_in()) {
	  	if ($this->ion_auth->in_group(1)) {
		    $data['url'] = 'team_player_position_crud';
		    $data['title'] = 'Gestión de Posiciones de Jugadores';
		    $data['desc'] = 'Seccion de creacion, modificacion y eliminacion de Posiciones en Base de Datos.';
		    $data['secondCrud'] = FALSE;
		    $data['url_second'] = '';
		    $this->load->view('v2/pages/crud-holder', $data);
		}else{
			$this->ion_auth->logout();
			redirect('/auth/login');	
		}
	  }else{
	    redirect('/auth/login');
	  }
	}

	public function team_player_crud()
	{
	  try{
	    $crud = new grocery_CRUD();
	    $crud->set_table('team_player');
	    $crud->set_subject('Jugador');
	    $crud->set_field_upload('team_player_picture','../assets/bucket/players');
	    $crud->display_as('team_player_name','Nombre')
	         ->display_as('team_player_middle','Intermedio')
	         ->display_as('team_player_lastname','Apellido')
	         ->display_as('team_player_slug','Slug (url)')
	         ->display_as('team_player_picture','Fotografia')
	         ->display_as('team_player_phrase','Frase')
	         ->display_as('team_player_position','Posicion')
	         ->display_as('team_player_team_division','Division')
	         ->display_as('team_player_birth_date','Fecha de Nacimiento')
	         ->display_as('team_player_birth_place','Lugar de Nacimiento')
	         ->display_as('team_player_season_in_club','Temporadas en el Club')
	         ->display_as('team_player_original_club','Club Original')
	         ->display_as('team_player_shirt_number','Numero de Dorsal')
	         ->display_as('team_player_goals_out','Goles recibidos')
	         ->display_as('team_player_goals_in','Goles Convertidos')
	         ->display_as('team_player_matchs_play','Partidos Jugados')
	         ->display_as('team_player_matchs_in_team','Partidos Titular')
	         ->display_as('team_player_minutes_play','Minutos Jugados')
	         ->display_as('team_player_card_yellow','Tarjetas Amarillas')
	         ->display_as('team_player_card_red','Tarjetas Rojas')
	         ->display_as('team_player_link_store','Link a Tienda')
			 ->display_as('team_player_status','Estado');
		$crud->field_type(
 	      'team_player_status',
 	      'dropdown',
 	      array(
 	        '0' => 'Desactivado',
 	        '1' => 'Activado'
 	      )
 	    );
 	    $crud->set_relation('team_player_position','team_player_position','team_player_position_name');
 	    $crud->set_relation('team_player_team_division','team_division','team_division_name');
		$crud->callback_after_insert(array($this, 'gen_player_slug'));
		$crud->callback_after_update(array($this, 'gen_player_slug'));
	    $output = $crud->render();
	    $this->renderCrud($output);
	  }catch(Exception $e){
	    show_error($e->getMessage().' --- '.$e->getTraceAsString());
	  }
	}

	public function gen_player_slug($post_array, $primary_key){
	  $this->load->helper('url');
	  $name = normaliza($post_array['team_player_name'] . '_' . $post_array['team_player_lastname']);
	  $url_title = url_title($name, '-', TRUE);
	  $data = array(
	  	'team_player_slug' => $url_title
	  );
	  $this->db->where('team_player_id', $primary_key);
	  $this->db->update('team_player',$data);
	  return true;
	}

	public function team_player_holder()
	{
	  if ($this->ion_auth->logged_in()) {
	  	if ($this->ion_auth->in_group(1)) {
		    $data['url'] = 'team_player_crud';
		    $data['title'] = 'Gestión de Jugadores';
		    $data['desc'] = 'Seccion de creacion, modificacion y eliminacion de Jugadores en Base de Datos.';
		    $data['secondCrud'] = FALSE;
		    $data['url_second'] = '';
		    $this->load->view('v2/pages/crud-holder', $data);
		}else{
			$this->ion_auth->logout();
			redirect('/auth/login');	
		}
	  }else{
	    redirect('/auth/login');
	  }
	}
	// Commercial
	public function ads_module_crud()
	{
	  try{
	    $crud = new grocery_CRUD();
	    $crud->set_table('ads_module');
	    $crud->set_subject('Banner Publicitario');
	    $crud->set_field_upload('ads_module_image','../assets/bucket/ads');
	    $crud->display_as('ads_module_title','Titulo')
	    	 ->display_as('ads_module_image','Imagen')
	    	 ->display_as('ads_module_desc','Descripcion')
	    	 ->display_as('ads_module_slug','Link de Referencia')
	    	 ->display_as('ads_module_prints','Cantidad de Impresiones')
	    	 ->display_as('ads_module_prints_mode','Limite de Impresiones')
	    	 ->display_as('ads_module_category','Categoria')
	    	 ->display_as('ads_module_clicks','Contador de Clicks')
	    	 ->display_as('ads_module_status','Estado');
		$crud->field_type(
 	      'ads_module_status',
 	      'dropdown',
 	      array(
 	        '0' => 'Desactivado',
 	        '1' => 'Activado'
 	      )
 	    );
 	    $crud->field_type(
 	      'ads_module_prints_mode',
 	      'dropdown',
 	      array(
 	        '0' => 'Desactivado',
 	        '1' => 'Activado'
 	      )
 	    );
 	    $crud->set_relation('ads_module_category','ads_module_category','ads_module_category_title');
	    $output = $crud->render();
	    $this->renderCrud($output);
	  }catch(Exception $e){
	    show_error($e->getMessage().' --- '.$e->getTraceAsString());
	  }
	}
	public function ads_module_holder()
	{
	  if ($this->ion_auth->logged_in()) {
	  	if ($this->ion_auth->in_group(1)) {
		    $data['url'] = 'ads_module_crud';
		    $data['title'] = 'Gestión de Publicidades';
		    $data['desc'] = 'Seccion de creacion, modificacion y eliminacion de Publicidades en Base de Datos.';
		    $data['secondCrud'] = FALSE;
		    $data['url_second'] = '';
		    $this->load->view('v2/pages/crud-holder', $data);
		}else{
			$this->ion_auth->logout();
			redirect('/auth/login');	
		}
	  }else{
	    redirect('/auth/login');
	  }
	}

	public function ads_module_category_crud()
	{
	  try{
	    $crud = new grocery_CRUD();
	    $crud->set_table('ads_module_category');
	    $crud->set_subject('Categoria de Publicidad');
	    $crud->display_as('ads_module_category_title','Titulo')
	    	 ->display_as('ads_module_category_status','Estado');
		$crud->field_type(
 	      'ads_module_category_status',
 	      'dropdown',
 	      array(
 	        '0' => 'Desactivado',
 	        '1' => 'Activado'
 	      )
 	    );
	    $output = $crud->render();
	    $this->renderCrud($output);
	  }catch(Exception $e){
	    show_error($e->getMessage().' --- '.$e->getTraceAsString());
	  }
	}
	public function ads_module_category_holder()
	{
	  if ($this->ion_auth->logged_in()) {
	  	if ($this->ion_auth->in_group(1)) {
		    $data['url'] = 'ads_module_category_crud';
		    $data['title'] = 'Gestión de Categorias Publicidades';
		    $data['desc'] = 'Seccion de creacion, modificacion y eliminacion de Categorias Publicidades en Base de Datos.';
		    $data['secondCrud'] = FALSE;
		    $data['url_second'] = '';
		    $this->load->view('v2/pages/crud-holder', $data);
		}else{
			$this->ion_auth->logout();
			redirect('/auth/login');	
		}
	  }else{
	    redirect('/auth/login');
	  }
	}

	public function contact_information_crud()
	{
	  try{
	    $crud = new grocery_CRUD();
	    $crud->set_table('contact_information');
	    $crud->set_subject('Informacion de Contacto');
	    $crud->display_as('contact_information_name','Nombre')
	    	 ->display_as('contact_information_url','Url')
	    	 ->display_as('contact_information_icon','Icono')
	    	 ->display_as('contact_information_html_code_embed','Codigo Embebido')
	    	 ->display_as('contact_information_status','Estado');
		$crud->field_type(
 	      'contact_information_status',
 	      'dropdown',
 	      array(
 	        '0' => 'Desactivado',
 	        '1' => 'Activado'
 	      )
 	    );
	    $output = $crud->render();
	    $this->renderCrud($output);
	  }catch(Exception $e){
	    show_error($e->getMessage().' --- '.$e->getTraceAsString());
	  }
	}
	public function contact_information_holder()
	{
	  if ($this->ion_auth->logged_in()) {
	  	if ($this->ion_auth->in_group(1)) {
		    $data['url'] = 'contact_information_crud';
		    $data['title'] = 'Gestión de Información de Contacto';
		    $data['desc'] = 'Seccion de creacion, modificacion y eliminacion de Informacion de Contacto en Base de Datos.';
		    $data['secondCrud'] = FALSE;
		    $data['url_second'] = '';
		    $this->load->view('v2/pages/crud-holder', $data);
		}else{
			$this->ion_auth->logout();
			redirect('/auth/login');	
		}
	  }else{
	    redirect('/auth/login');
	  }
	}

	public function contact_message_crud()
	{
	  try{
	    $crud = new grocery_CRUD();
	    $crud->set_table('contact_message');
	    $crud->set_subject('Mensaje de Contacto');
	    $crud->display_as('contact_message_first_name','Nombre')
	    	 ->display_as('contact_message_last_name','Apellido')
	    	 ->display_as('contact_message_email','E-Mail')
	    	 ->display_as('contact_message_phone','Telefono')
	    	 ->display_as('contact_message_company','Medio o Club')
	    	 ->display_as('contact_message_text','Mensaje')
	    	 ->display_as('contact_message_date','Fecha')
	    	 ->display_as('contact_message_ip','I.P.')
	    	 ->display_as('contact_message_status','Estado');
		$crud->field_type(
 	      'contact_message_status',
 	      'dropdown',
 	      array(
 	        '0' => 'No Leido',
 	        '1' => 'Leido'
 	      )
 	    );
	    $output = $crud->render();
	    $this->renderCrud($output);
	  }catch(Exception $e){
	    show_error($e->getMessage().' --- '.$e->getTraceAsString());
	  }
	}
	public function contact_message_holder()
	{
	  if ($this->ion_auth->logged_in()) {
	  	if ($this->ion_auth->in_group(1)) {
		    $data['url'] = 'contact_message_crud';
		    $data['title'] = 'Gestión de Contactos de Prensa';
		    $data['desc'] = 'Seccion de creacion, modificacion y eliminacion de Mensajes de Prensa en Base de Datos.';
		    $data['secondCrud'] = FALSE;
		    $data['url_second'] = '';
		    $this->load->view('v2/pages/crud-holder', $data);
		}else{
			$this->ion_auth->logout();
			redirect('/auth/login');	
		}
	  }else{
	    redirect('/auth/login');
	  }
	}

	public function home_slide_crud()
	{
	  try{
	    $crud = new grocery_CRUD();
	    $crud->set_table('home_slide');
	    $crud->set_subject('Ficha de Carousel Home');
	    $crud->display_as('home_slide_title','Titulo')
	    	 ->display_as('home_slide_subtitle','Subtitulo')
	    	 ->display_as('home_slide_btn','Texto de Boton')
	    	 ->display_as('home_slide_link','Link')
	    	 ->display_as('home_slide_image','Imagen')
	    	 ->display_as('home_slide_category','Categoria')
	    	 ->display_as('home_slide_status','Estado');
		$crud->field_type(
 	      'home_slide_status',
 	      'dropdown',
 	      array(
 	        '0' => 'No Leido',
 	        '1' => 'Leido'
 	      )
 	    );
 	    $crud->set_relation('home_slide_category','home_slide_categories','home_slide_categories_title');
	    $output = $crud->render();
	    $this->renderCrud($output);
	  }catch(Exception $e){
	    show_error($e->getMessage().' --- '.$e->getTraceAsString());
	  }
	}
	public function home_slide_holder()
	{
	  if ($this->ion_auth->logged_in()) {
	  	if ($this->ion_auth->in_group(1)) {
		    $data['url'] = 'home_slide_crud';
		    $data['title'] = 'Gestión de Fichas de Carousel Home';
		    $data['desc'] = 'Seccion de creacion, modificacion y eliminacion de Fichas de Carousel Home en Base de Datos.';
		    $data['secondCrud'] = FALSE;
		    $data['url_second'] = '';
		    $this->load->view('v2/pages/crud-holder', $data);
		}else{
			$this->ion_auth->logout();
			redirect('/auth/login');	
		}
	  }else{
	    redirect('/auth/login');
	  }
	}

	public function home_slide_categories_crud()
	{
	  try{
	    $crud = new grocery_CRUD();
	    $crud->set_table('home_slide_categories');
	    $crud->set_subject('Categoria de Carousel Home');
	    $crud->display_as('home_slide_categories_title','Titulo')
	    	 ->display_as('home_slide_categories_status','Estado');
		$crud->field_type(
 	      'home_slide_categories_status',
 	      'dropdown',
 	      array(
 	        '0' => 'Inactivo',
 	        '1' => 'Activo'
 	      )
 	    );
	    $output = $crud->render();
	    $this->renderCrud($output);
	  }catch(Exception $e){
	    show_error($e->getMessage().' --- '.$e->getTraceAsString());
	  }
	}
	public function home_slide_categories_holder()
	{
	  if ($this->ion_auth->logged_in()) {
	  	if ($this->ion_auth->in_group(1)) {
		    $data['url'] = 'home_slide_categories_crud';
		    $data['title'] = 'Gestión de Categoria de Carousel Home';
		    $data['desc'] = 'Seccion de creacion, modificacion y eliminacion de Categoria de Carousel Home en Base de Datos.';
		    $data['secondCrud'] = FALSE;
		    $data['url_second'] = '';
		    $this->load->view('v2/pages/crud-holder', $data);
		}else{
			$this->ion_auth->logout();
			redirect('/auth/login');	
		}
	  }else{
	    redirect('/auth/login');
	  }
	}
	public function home_slide_config_crud()
	{
	  try{
	    $crud = new grocery_CRUD();
	    $crud->set_table('home_slide_config');
	    $crud->set_subject('Configuracion de Carousel Home');
	    $crud->display_as('home_slide_config_blog_post_max','Post de Blog Maximo')
	    	 ->display_as('home_slide_config_custom_post_max','Post Custom Maximo')
	    	 ->display_as('home_slide_config_autoplay','Reproduccion Automatica');
		$crud->field_type(
 	      'home_slide_config_autoplay',
 	      'dropdown',
 	      array(
 	        '0' => 'Inactivo',
 	        '1' => 'Activo'
 	      )
 	    );
 	    $crud->unset_add();
        $crud->unset_delete();
	    $output = $crud->render();
	    $this->renderCrud($output);
	  }catch(Exception $e){
	    show_error($e->getMessage().' --- '.$e->getTraceAsString());
	  }
	}
	public function home_slide_config_holder()
	{
	  if ($this->ion_auth->logged_in()) {
	  	if ($this->ion_auth->in_group(1)) {
		    $data['url'] = 'home_slide_config_crud';
		    $data['title'] = 'Configuracion de Carousel Home';
		    $data['desc'] = 'Modificacion y Configuracion de Carousel Home en Base de Datos.';
		    $data['secondCrud'] = FALSE;
		    $data['url_second'] = '';
		    $this->load->view('v2/pages/crud-holder', $data);
		}else{
			$this->ion_auth->logout();
			redirect('/auth/login');	
		}
	  }else{
	    redirect('/auth/login');
	  }
	}

	// Commercial End
	
	// Event
	
	public function event_crud()
	{
	  try{
	    $crud = new grocery_CRUD();
	    $crud->set_table('event');
	    $crud->set_subject('Evento');
	    $crud->display_as('event_title','Titulo del Evento')
	    	 ->display_as('event_text_body','Cuerpo del Texto')
	    	 ->display_as('event_team_one','Primer Equipo')
	    	 ->display_as('event_team_two','Segundo Equipo')
	    	 ->display_as('event_stadium','Estadio')
	    	 ->display_as('event_date','Fecha')
	    	 ->display_as('event_time','Hora')
	    	 ->display_as('event_detail_link','Slug (URL)')
	    	 ->display_as('event_buy_link','Link de Compra')
	    	 ->display_as('event_status','Estado');
	    $crud->set_relation('event_team_one','team','team_name');
		$crud->set_relation('event_team_two','team','team_name');
		$crud->field_type(
 	      'event_status',
 	      'dropdown',
 	      array(
 	        '0' => 'Inactivo',
 	        '1' => 'Activo'
 	      )
 	    );
		$crud->callback_after_insert(array($this, 'gen_event_slug'));
		$crud->callback_after_update(array($this, 'gen_event_slug'));
	    $output = $crud->render();
	    $this->renderCrud($output);
	  }catch(Exception $e){
	    show_error($e->getMessage().' --- '.$e->getTraceAsString());
	  }
	}

	public function gen_event_slug($post_array, $primary_key){
	  $this->load->helper('url');
	  $name = normaliza($post_array['event_title']);
	  $url_title = url_title($name, '-', TRUE);
	  $data = array(
	  	'event_detail_link' => $url_title
	  );
	  $this->db->where('event_id', $primary_key);
	  $this->db->update('event',$data);
	  return true;
	}

	public function event_holder()
	{
	  if ($this->ion_auth->logged_in()) {
	  	if ($this->ion_auth->in_group(1)) {
		    $data['url'] = 'event_crud';
		    $data['title'] = 'Gestión de Eventos';
		    $data['desc'] = 'Seccion de creacion, modificacion y eliminacion de Eventos en Base de Datos.';
		    $data['secondCrud'] = FALSE;
		    $data['url_second'] = '';
		    $this->load->view('v2/pages/crud-holder', $data);
		}else{
			$this->ion_auth->logout();
			redirect('/auth/login');	
		}
	  }else{
	    redirect('/auth/login');
	  }
	}

	public function event_special_crud()
	{
	  try{
	    $crud = new grocery_CRUD();
	    $crud->set_table('event_special');
	    $crud->set_subject('Evento Especial');
	    $crud->display_as('event_title','Titulo del Evento')
	    	 ->display_as('event_text_body','Cuerpo del Texto')
	    	 ->display_as('event_date','Fecha')
	    	 ->display_as('event_time','Hora')
	    	 ->display_as('event_detail_link','Link a Detalle de Evento')
	    	 ->display_as('event_detail_show','Mostrar Link a Detalle de Evento')
	    	 ->display_as('event_status','Estado');
		$crud->field_type(
 	      'event_detail_show',
 	      'dropdown',
 	      array(
 	        '0' => 'Inactivo',
 	        '1' => 'Activo'
 	      )
 	    );
 	    $crud->field_type(
 	      'event_status',
 	      'dropdown',
 	      array(
 	        '0' => 'Inactivo',
 	        '1' => 'Activo'
 	      )
 	    );
		$crud->callback_after_insert(array($this, 'gen_event_special_slug'));
		$crud->callback_after_update(array($this, 'gen_event_special_slug'));
	    $output = $crud->render();
	    $this->renderCrud($output);
	  }catch(Exception $e){
	    show_error($e->getMessage().' --- '.$e->getTraceAsString());
	  }
	}

	public function gen_event_special_slug($post_array, $primary_key){
	  $this->load->helper('url');
	  $name = normaliza($post_array['event_title']);
	  $code = uniqid();
	  $url_title = url_title($name, '-', TRUE);
	  $data = array(
	  	'event_detail_link' => $url_title . '_' . $code
	  );
	  $this->db->where('event_id', $primary_key);
	  $this->db->update('event_special',$data);
	  return true;
	}

	public function event_special_holder()
	{
	  if ($this->ion_auth->logged_in()) {
	  	if ($this->ion_auth->in_group(1)) {
		    $data['url'] = 'event_special_crud';
		    $data['title'] = 'Gestión de Eventos Especiales';
		    $data['desc'] = 'Seccion de creacion, modificacion y eliminacion de Eventos Especiales en Base de Datos.';
		    $data['secondCrud'] = FALSE;
		    $data['url_second'] = '';
		    $this->load->view('v2/pages/crud-holder', $data);
		}else{
			$this->ion_auth->logout();
			redirect('/auth/login');	
		}
	  }else{
	    redirect('/auth/login');
	  }
	}
	// Event End

	// Config
	public function provider_config_crud()
	{
		try{
			$crud = new grocery_CRUD();
			$crud->set_table('api_external');
			$crud->set_subject('Apis Externa');
			$crud->display_as('api_external_provider','Proveedor')
					 ->display_as('api_external_api_code','API Key')
					 ->display_as('api_external_status','Estado');
			$crud->field_type(
 	      'api_external_status',
 	      'dropdown',
 	      array(
 	        '1' => 'Activo',
 	        '2' => 'Inactivo'
 	      )
 	    );
			$output = $crud->render();
			$this->renderCrud($output);
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
	public function provider_config_holder()
	{
		if ($this->ion_auth->logged_in()) {
			if ($this->ion_auth->in_group(1)) {
				$data['url'] = 'provider_config_crud';
				$data['title'] = 'API Keys';
				$data['desc'] = 'Seccion de Alta, Baja y Modificaciones de APIs externas.';
				$data['secondCrud'] = FALSE;
				$data['url_second'] = '';
				$this->load->view('v2/pages/crud-holder', $data);
			}else{
				$this->ion_auth->logout();
				redirect('/auth/login');	
			}
		}else{
			redirect('/auth/login');
		}
	}
	public function site_config_crud()
	{
		try{
			$crud = new grocery_CRUD();
			$crud->set_table('site_conf');
			$crud->set_subject('Configuracion de Sitio');
			$crud->display_as('site_name','Nombre del Sitio')
					 ->display_as('site_author','Autor del Sitio')
					 ->display_as('site_desc','Descripcion del Sitio')
					 ->display_as('site_keywords','Keywords del Sitio')
					 ->display_as('site_favicon','Favicon del Sitio')
					 ->display_as('site_appleicon','Apple Icon del Sitio')
					 ->display_as('site_logo','Logo del Sitio')
					 ->display_as('site_logofoot','Logo Footer del Sitio')
					 ->display_as('site_charset','Charset del Sitio')
					 ->display_as('site_lang','Lenguaje del Sitio')
					 ->display_as('site_mail','E-Mail del Sitio')
					 ->display_as('cooming_soon','Proximamente/Mantenimiento');
			$crud->field_type(
 	      'cooming_soon',
 	      'dropdown',
 	      array(
 	        '1' => 'Activo',
 	        '2' => 'Inactivo'
 	      )
 	    );
			$crud->unset_add();
			$crud->unset_delete();
			$output = $crud->render();
			$this->renderCrud($output);
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
	public function site_config_holder()
	{
		if ($this->ion_auth->logged_in()) {
			if ($this->ion_auth->in_group(1)) {
				$data['url'] = 'site_config_crud';
				$data['title'] = 'Configuracion de Sitio';
				$data['desc'] = 'Seccion de Modificacion de Variables del Sitio.';
				$data['secondCrud'] = FALSE;
				$data['url_second'] = '';
				$this->load->view('v2/pages/crud-holder', $data);
			}else{
				$this->ion_auth->logout();
				redirect('/auth/login');	
			}
		}else{
			redirect('/auth/login');
		}
	}
	public function mailchimp_config_crud()
	{
		try{
			$crud = new grocery_CRUD();
			$crud->set_table('mailchimp_conf');
			$crud->set_subject('Configuracion de MailChimp');
			$crud->display_as('mailchimp_conf_list_id','Código de Lista')
				 ->display_as('mailchimp_conf_api_url','URL de API')
				 ->display_as('mailchimp_conf_status','Estado de Servicio');
			$crud->field_type(
 	      'mailchimp_conf_status',
 	      'dropdown',
 	      array(
 	        '1' => 'Activo',
 	        '2' => 'Inactivo'
 	      )
 	    );
			$crud->unset_add();
			$crud->unset_delete();
			$output = $crud->render();
			$this->renderCrud($output);
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
	public function mailchimp_config_holder()
	{
		if ($this->ion_auth->logged_in()) {
			if ($this->ion_auth->in_group(1)) {
				$data['url'] = 'mailchimp_config_crud';
				$data['title'] = 'Configuracion de MailChimp';
				$data['desc'] = 'Seccion de Modificacion de Variables del Servicio MailChimp.';
				$data['secondCrud'] = FALSE;
				$data['url_second'] = '';
				$this->load->view('v2/pages/crud-holder', $data);
			}else{
				$this->ion_auth->logout();
				redirect('/auth/login');	
			}
		}else{
			redirect('/auth/login');
		}
	}
	public function smtp_config_crud()
	{
		try{
			$crud = new grocery_CRUD();
			$crud->set_table('smtp_mail_conf');
			$crud->set_subject('Configuracion de SMTP');
			$crud->display_as('smtp_host','Host')
					 ->display_as('smtp_port','Puerto')
					 ->display_as('smtp_timeout','Timeout')
					 ->display_as('smtp_user','Usuario')
					 ->display_as('smtp_pass','Password')
					 ->display_as('smtp_charset','Charset')
					 ->display_as('smtp_newline','Newline')
					 ->display_as('smtp_mailtype','Mailtype')
					 ->display_as('smtp_validation','Validacion')
					 ->display_as('from','Envios desde (noreply@company.com)');
			$crud->unset_add();
			$crud->unset_delete();
			$output = $crud->render();
			$this->renderCrud($output);
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
	public function smtp_config_holder()
	{
		if ($this->ion_auth->logged_in()) {
			if ($this->ion_auth->in_group(1)) {
				$data['url'] = 'smtp_config_crud';
				$data['title'] = 'Configuracion de SMTP';
				$data['desc'] = 'Seccion de Modificacion de Variables del SMTP.';
				$data['secondCrud'] = FALSE;
				$data['url_second'] = '';
				$this->load->view('v2/pages/crud-holder', $data);
			}else{
				$this->ion_auth->logout();
				redirect('/auth/login');	
			}
		}else{
			redirect('/auth/login');
		}
	}

	public function contact_config_crud()
	{
		try{
			$crud = new grocery_CRUD();
			$crud->set_table('smtp_mail_conf');
			$crud->set_subject('Variables de Contacto');
			$crud->display_as('site_contact_phone','Telefono')
					 ->display_as('site_contact_email','E-Mail')
					 ->display_as('site_contact_fb','Facebook')
					 ->display_as('site_contact_tw','Twitter')
					 ->display_as('site_contact_insta','Instagram')
					 ->display_as('site_contact_adress','Direccion');
			$crud->unset_add();
			$crud->unset_delete();
			$output = $crud->render();
			$this->renderCrud($output);
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
	public function contact_config_holder()
	{
		if ($this->ion_auth->logged_in()) {
			if ($this->ion_auth->in_group(1)) {
				$data['url'] = 'contact_config_crud';
				$data['title'] = 'Configuracion de Contacto';
				$data['desc'] = 'Seccion de Modificacion de Variables de Contacto.';
				$data['secondCrud'] = FALSE;
				$data['url_second'] = '';
				$this->load->view('v2/pages/crud-holder', $data);
			}else{
				$this->ion_auth->logout();
				redirect('/auth/login');	
			}
		}else{
			redirect('/auth/login');
		}
	}
	// Config End
	
}
