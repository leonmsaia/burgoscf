<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		if ($this->ion_auth->logged_in()) {
			if ($this->ion_auth->in_group(1)) {
				$this->load->view('v2/home');
			}else{
				$this->ion_auth->logout();
				redirect('/auth/login');	
			}
		}else{
			redirect('/auth/login');
		}
	}
}