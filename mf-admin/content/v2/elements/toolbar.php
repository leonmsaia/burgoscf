<div class="topbar">
  <div class="logo">
    <a href="<?php echo base_url();?>">
        BLANQUINEGRO CMS v.1.0
    </a>
  </div>
  <a class="sidemenu-btn waves-effect waves-light" href="#" title=""><i class="ti-menu"></i></a>
  <div class="topbar-links">
    <div class="launcher">
      <a class="click-btn" href="#" title=""><i class="ti-widget"></i></a>
      <div class="launcher-dropdown z-depth-2">
        <a class="launch-btn" href="<?php echo base_url();?>Crud/site_config_holder">
            <i class="ti-settings cyan-text"></i>
            Configuracion
        </a>
        <a class="launch-btn" href="<?=base_url()?>auth/logout">
            <i class="ti-lock purple-text"></i>
            Cerrar Sesion
        </a>
      </div>
    </div>
  </div>
</div>
