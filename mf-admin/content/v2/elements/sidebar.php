<div class="sidemenu">
  <div class="sidemenu-inner scroll">
    <?php $this->load->view('v2/elements/adminScreen');?>
    <nav class="admin-nav">
      <h6>Herramientas Principales</h6>
      <ul>

        <li>
          <a class="waves-effect" href="<?php echo base_url();?>" title=""><i class="ti-panel red lighten-1"></i> Dashboard</a>
        </li>       
        <li>
          <a class="waves-effect" href="#" title="">
            <i class="ti-file orange lighten-1"></i>
             Gestión de Secciones
          </a>
          <ul>
            <li>
              <a href="<?php echo base_url();?>crud/static_pages_holder" title="">
                Gestión de Páginas
              </a>
            </li>
            <li>
              <a href="<?php echo base_url();?>crud/static_pages_navbar_holder" title="">
                Gestión de Elementos de Menú
              </a>
            </li>
            <li>
              <a href="<?php echo base_url();?>crud/blog_home_suit_config_holder" title="">
                Gestión de Notas en Home
              </a>
            </li>
          </ul>
        </li>

        <li>
          <a class="waves-effect" href="#" title="">
            <i class="ti-blackboard blue lighten-1"></i>
             Gestión de Equipo
          </a>
          <ul>
            <li>
              <a href="<?php echo base_url();?>crud/team_holder" title="">
                Gestión de Equipos Generales
              </a>
            </li>
            <li>
              <a href="<?php echo base_url();?>crud/country_soccer_division_holder" title="">
                Gestión de Divisiones Generales
              </a>
            </li>
            <li>
              <a href="<?php echo base_url();?>crud/main_club_stats_holder" title="">
                Gestión de Divisiones Propias
              </a>
            </li>
            <li>
              <a href="<?php echo base_url();?>crud/main_club_stats_holder" title="">
                Gestión de Estadisticas Propias
              </a>
            </li>
            <li>
              <a href="<?php echo base_url();?>crud/team_player_position_holder" title="">
                Gestión de Posiciones
              </a>
            </li>
            <li>
              <a href="<?php echo base_url();?>crud/team_player_holder" title="">
                Gestión de Jugadores
              </a>
            </li>
          </ul>
        </li>
        
        <li>
          <a class="waves-effect" href="#" title="">
            <i class="ti-bag green lighten-1"></i> 
            Comercial
          </a>
          <ul>
            <li>
              <a href="<?php echo base_url();?>crud/ads_module_holder" title="">
                Gestión de Publicidad
              </a>
            </li>
            <li>
              <a href="<?php echo base_url();?>crud/ads_module_category_holder" title="">
                Gestión de Categoria Publicidad
              </a>
            </li>
            <li>
              <a href="<?php echo base_url();?>crud/partners_holder" title="">
                Gestión de Partners
              </a>
            </li>
            <li>
              <a href="<?php echo base_url();?>crud/contact_information_holder" title="">
                Configuracion de Contacto
              </a>
            </li>
            <li>
              <a href="<?php echo base_url();?>crud/contact_message_holder" title="">
                Contacto de Prensa
              </a>
            </li>
            <li>
              <a href="<?php echo base_url();?>crud/home_slide_holder" title="">
                Gestión Carousel de Home
              </a>
            </li>
            <li>
              <a href="<?php echo base_url();?>crud/home_slide_categories_holder" title="">
                Gestión Categorias Carousel de Home
              </a>
            </li>
            <li>
              <a href="<?php echo base_url();?>crud/home_slide_config_holder" title="">
                Configuracion Carousel de Home
              </a>
            </li>
          </ul>
        </li>

        <li>
          <a class="waves-effect" href="#" title="">
            <i class="ti-blackboard purple lighten-1"></i> 
            Eventos
          </a>
          <ul>
            <li>
              <a href="<?php echo base_url();?>crud/event_holder" title="">
                Gestión de Eventos
              </a>
            </li>
            <li>
              <a href="<?php echo base_url();?>crud/event_special_holder" title="">
                Gestión de Eventos Especiales
              </a>
            </li>
          </ul>
        </li>

        <li>
          <a class="waves-effect" href="#" title="">
            <i class="ti-layout-tab brown lighten-1"></i> 
            Configuracion
          </a>
          <ul>
            <li>
              <a href="<?php echo base_url();?>crud/site_config_holder" title="">
                Configuracion de Sitio
              </a>
            </li>
            <li>
              <a href="<?php echo base_url();?>crud/smtp_config_holder" title="">
                Configuracion SMTP
              </a>
            </li>
            <li>
              <a href="<?php echo base_url();?>crud/provider_config_holder" title="">
                Proveedores Externos
              </a>
            </li>
            <li>
              <a href="<?php echo base_url();?>crud/mailchimp_config_holder" title="">
                MailChimp
              </a>
            </li>
            
          </ul>
        </li>

        <li>
          <a class="waves-effect" href="#" title="">
            <i class="ti-user black lighten-1"></i> 
            Usuarios
          </a>
          <ul>
            <li>
              <a href="<?php echo base_url();?>User/create_user">Crear Usuarios</a>
            </li>
            <li>
              <a href="<?php echo base_url();?>User/list_users">Lista de Usuarios</a>
            </li>
          </ul>
        </li>

      </ul>
      <h6 class="copyright">
        Blanquinegro CMS v1.1
        <br>
        <small>Desarrollado por <a class="purple-text" href="https://www.linkedin.com/in/leonmsaia/" target="_blank">Leon. M. Saia</a> para Burgos C.F.</small>
        <br>
        <small>
          Soporte y consultas <br>
          leonmsaia@gmail.com
        </small>
        <br><br>
        <small>&copy; <?php echo date('Y');?> Todos los Derechos Reservados</small>
      </h6>
    </nav>
  </div>
</div>