<!doctype html>
<html lang="en">
	<?php $this->load->view('v2/elements/head');?>
<body>

<div class="loader-container circle-pulse-multiple">
  <div class="page-loader">
    <div id="loading-center-absolute">
      <div class="object" id="object_four"></div>
      <div class="object" id="object_three"></div>
      <div class="object" id="object_two"></div>
      <div class="object" id="object_one"></div>
    </div>
  </div>
</div>

  <?php $this->load->view('v2/elements/toolbar');?>

  <?php $this->load->view('v2/elements/sidebar');?>

  <div class="content-area">
    <?php $this->load->view('v2/elements/breadcrum');?>
		<div class="widgets-wrapper">
			<div class="row">
        <div class="masonary">
          <div class="col s12">
            <div class="widget z-depth-1">
              <div class="loader"></div>
              <div class="widget-title">
                <h3><?php echo lang('create_user_heading');?></h3>
                <p><?php echo lang('create_user_subheading');?></p>
              </div>
              <div class="widget-crud">
                <div class="col s12">
                  <div id="infoMessage"><?php echo $message;?></div>
                  <?php echo form_open("User/create_user");?>
                        <div class="row">
                            <div class="input-field col s6">
                              <?php echo lang('create_user_fname_label', 'first_name');?> <br />
                              <?php echo form_input($first_name);?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s6">
                              <?php echo lang('create_user_lname_label', 'last_name');?> <br />
                              <?php echo form_input($last_name);?>
                            </div>
                        </div>
                        <?php
                        if($identity_column!=='email') {
                            echo '<p>';
                            echo lang('create_user_identity_label', 'identity');
                            echo '<br />';
                            echo form_error('identity');
                            echo form_input($identity);
                            echo '</p>';
                        }
                        ?>
                        <div class="row">
                            <div class="input-field col s6">
                              <?php echo lang('create_user_company_label', 'company');?> <br />
                              <?php echo form_input($company);?>
                            </div>
                        </div>

                        <div class="row">
                            <div class="input-field col s6">
                              <?php echo lang('create_user_email_label', 'email');?> <br />
                              <?php echo form_input($email);?>
                            </div>
                        </div>

                        <div class="row">
                            <div class="input-field col s6">
                              <?php echo lang('create_user_phone_label', 'phone');?> <br />
                              <?php echo form_input($phone);?>
                            </div>
                        </div>

                        <div class="row">
                            <div class="input-field col s6">
                              <?php echo lang('create_user_password_label', 'password');?> <br />
                              <?php echo form_input($password);?>
                            </div>
                        </div>

                        <div class="row">
                            <div class="input-field col s6">
                              <?php echo lang('create_user_password_confirm_label', 'password_confirm');?> <br />
                              <?php echo form_input($password_confirm);?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s6">
                              <?php echo form_submit('submit', lang('create_user_submit_btn'), 'class="modal-action modal-close waves-effect waves-light btn-flat green lighten-2 white-text save-contact"');?>
                            </div>
                        </div>
                  <?php echo form_close();?>
                </div>
              </div>
            </div>
          </div>
        </div>
			</div>
		</div>
	</div>

<?php $this->load->view('v2/elements/scripts');?>