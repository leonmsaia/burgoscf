<!doctype html>
<html lang="en">
	<?php $this->load->view('v2/elements/head');?>
<body>

<div class="loader-container circle-pulse-multiple">
  <div class="page-loader">
    <div id="loading-center-absolute">
      <div class="object" id="object_four"></div>
      <div class="object" id="object_three"></div>
      <div class="object" id="object_two"></div>
      <div class="object" id="object_one"></div>
    </div>
  </div>
</div>

  <?php $this->load->view('v2/elements/toolbar');?>

  <?php $this->load->view('v2/elements/sidebar');?>

  <div class="content-area">
    <?php $this->load->view('v2/elements/breadcrum');?>
		<div class="widgets-wrapper">
			<div class="row">
        <div class="masonary">
          <div class="col s12">
            <div class="widget z-depth-1">
              <div class="loader"></div>
              <div class="widget-title">
                <h3><?php echo lang('edit_user_heading');?></h3>
                <p><?php echo lang('edit_user_subheading');?></p>
              </div>
              <div class="widget-crud">
                <div class="col s12">

                  <div id="infoMessage"><?php echo $message;?></div>

                  <?php echo form_open(uri_string());?>

                        <div class="row">
                            <div class="input-field col s6">
                              <?php echo lang('edit_user_fname_label', 'first_name');?> <br />
                              <?php echo form_input($first_name);?>
                            </div>
                        </div>

                        <div class="row">
                            <div class="input-field col s6">
                              <?php echo lang('edit_user_lname_label', 'last_name');?> <br />
                              <?php echo form_input($last_name);?>
                            </div>
                        </div>

                        <div class="row">
                            <div class="input-field col s6">
                              <?php echo lang('edit_user_company_label', 'company');?> <br />
                              <?php echo form_input($company);?>
                            </div>
                        </div>

                        <div class="row">
                            <div class="input-field col s6">
                              <?php echo lang('edit_user_phone_label', 'phone');?> <br />
                              <?php echo form_input($phone);?>
                            </div>
                        </div>

                        <div class="row">
                            <div class="input-field col s6">
                              <?php echo lang('edit_user_password_label', 'password');?> <br />
                              <?php echo form_input($password);?>
                            </div>
                        </div>

                        <div class="row">
                            <div class="input-field col s6">
                              <?php echo lang('edit_user_password_confirm_label', 'password_confirm');?><br />
                              <?php echo form_input($password_confirm);?>
                            </div>
                        </div>
      
                        <?php if ($this->ion_auth->is_admin()): ?>
                        <div class="row">
                            <div class="input-field col s6">
                              <h3><?php echo lang('edit_user_groups_heading');?></h3>
                              <?php $counter = 1;?>
                              <?php foreach ($groups as $group):?>

                                  <?php
                                      $gID=$group['id'];
                                      $checked = null;
                                      $item = null;
                                      foreach($currentGroups as $grp) {
                                          if ($gID == $grp->id) {
                                              $checked= ' checked="checked"';
                                          break;
                                          }
                                      }
                                  ?>
                                  <p>
                                    <input type="checkbox" name="groups[]" value="<?php echo $group['id'];?>" id="group-<?php echo $group['id'];?>" <?php echo $checked;?>>
                                    <label for="group-<?php echo $group['id'];?>"><?php echo htmlspecialchars($group['name'],ENT_QUOTES,'UTF-8');?></label>
                                  </p>
                                  <?php $counter++; ?>
                              <?php endforeach?>
                            </div>
                        </div>
                        <?php endif ?>

                        <?php echo form_hidden('id', $user->id);?>
                        <?php echo form_hidden($csrf); ?>

                        <div class="row">
                          <div class="input-field col s6">
                            <?php echo form_submit('submit', lang('edit_user_submit_btn'), 'class="modal-action modal-close waves-effect waves-light btn-flat green lighten-2 white-text save-contact"');?>
                          </div>
                        </div>

                  <?php echo form_close();?>

                </div>
              </div>
            </div>
          </div>
        </div>
			</div>
		</div>
	</div>

<?php $this->load->view('v2/elements/scripts');?>