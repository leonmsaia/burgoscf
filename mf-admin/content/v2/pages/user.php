<!doctype html>
<html lang="en">
	<?php $this->load->view('v2/elements/head');?>
<body>

<div class="loader-container circle-pulse-multiple">
  <div class="page-loader">
    <div id="loading-center-absolute">
      <div class="object" id="object_four"></div>
      <div class="object" id="object_three"></div>
      <div class="object" id="object_two"></div>
      <div class="object" id="object_one"></div>
    </div>
  </div>
</div>

  <?php $this->load->view('v2/elements/toolbar');?>

  <?php $this->load->view('v2/elements/sidebar');?>

  <div class="content-area">
    <?php $this->load->view('v2/elements/breadcrum');?>
		<div class="widgets-wrapper">
			<div class="row">
        <div class="masonary">
          <div class="col s12">
            <div class="widget z-depth-1">
              <div class="loader"></div>
              <div class="widget-title">
                <h3><?php echo $title;?></h3>
                <p><?php echo $desc;?></p>
              </div>
              <div class="widget-crud">
                <div class="col s12">
                  <?php echo form_open_multipart('User/updateProfile', 'id="update_profile_form_trigger"');?>
                      <div class="row">
                        <div class="input-field col s6">
                          <input id="first_name" name="first_name" type="text" class="validate" placeholder="Nombre" value="<?php echo getUserNfo(getMyID())['first_name'];?>">
                          <label for="first_name">Nombre</label>
                        </div>
                        <div class="input-field col s6">
                          <input id="last_name" name="last_name" type="text" class="validate" placeholder="Apellido" value="<?php echo getUserNfo(getMyID())['last_name'];?>">
                          <label for="last_name">Apellido</label>
                        </div>
      									<div class="input-field col s12">
      										<textarea id="about" name="about" class="materialize-textarea"><?php echo getUserNfo(getMyID())['about'];?></textarea>
      										<label for="about">Un poco sobre mi</label>
      									</div>
                      </div>
                      <div class="row">
                        <div class="input-field col s6">
                          <input id="company" name="company" type="text" class="validate" placeholder="Compañia" value="<?php echo getUserNfo(getMyID())['company'];?>">
                          <label for="company">Compañia</label>
                        </div>
                        <div class="input-field col s6">
                          <input id="username" name="username" type="text" class="validate" placeholder="Username" value="<?php echo getUserNfo(getMyID())['username'];?>">
                          <label for="username">Nombre de Usuario</label>
                        </div>
                      </div>
                      <div class="row">
                        <div class="input-field col s12">
                            <input id="email" name="email" type="email" class="validate" placeholder="Email" value="<?php echo getUserNfo(getMyID())['email'];?>">
                            <label for="email">Email</label>
                        </div>
                      </div>
                      <div class="row">
                        <div class="input-field col s12">
                          <div class="button-set">
                            <button id="submit_profile_form" type="submit" class="modal-action modal-close waves-effect waves-light btn-flat green lighten-2 white-text save-contact">Guardar Ajustes</button>
                          </div>
                        </div>
                      </div>
                  <?php echo form_close();?>
                </div>
              </div>
            </div>
          </div>
        </div>
			</div>
		</div>
	</div>

<?php $this->load->view('v2/elements/scripts');?>