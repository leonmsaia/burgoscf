<!doctype html>
<html lang="en">
	<?php $this->load->view('v2/elements/head');?>
<body>

<div class="loader-container circle-pulse-multiple">
  <div class="page-loader">
    <div id="loading-center-absolute">
      <div class="object" id="object_four"></div>
      <div class="object" id="object_three"></div>
      <div class="object" id="object_two"></div>
      <div class="object" id="object_one"></div>
    </div>
  </div>
</div>

  <?php $this->load->view('v2/elements/toolbar');?>

  <?php $this->load->view('v2/elements/sidebar');?>

  <div class="content-area">
    <?php $this->load->view('v2/elements/breadcrum');?>
		<div class="widgets-wrapper">
			<div class="row">
        <div class="masonary">
          <div class="col s12">
            <div class="widget z-depth-1">
              <div class="loader"></div>
              <div class="widget-title">
                <h3><?php echo lang('index_heading');?></h3>
                <p><?php echo lang('index_subheading');?></p>
              </div>
              <div class="widget-crud">
                <div class="col s12">

                  <div id="infoMessage"><?php echo $message;?></div>

                  <table id="stream_table" class="table table-striped table-bordered" cellpadding=0 cellspacing=10>
                    <thead>
                      <tr>
                        <th><?php echo lang('index_fname_th');?></th>
                        <th><?php echo lang('index_lname_th');?></th>
                        <th><?php echo lang('index_email_th');?></th>
                        <th><?php echo lang('index_groups_th');?></th>
                        <th><?php echo lang('index_status_th');?></th>
                        <th><?php echo lang('index_action_th');?></th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php foreach ($users as $user):?>
                        <tr>
                                <td><?php echo htmlspecialchars($user->first_name,ENT_QUOTES,'UTF-8');?></td>
                                <td><?php echo htmlspecialchars($user->last_name,ENT_QUOTES,'UTF-8');?></td>
                                <td><?php echo htmlspecialchars($user->email,ENT_QUOTES,'UTF-8');?></td>
                          <td>
                            <?php foreach ($user->groups as $group):?>
                              <?php echo htmlspecialchars($group->name,ENT_QUOTES,'UTF-8') ;?><br />
                                    <?php endforeach?>
                          </td>
                          <td>
                            <?php echo ($user->active) ? anchor("User/deactivate/" . $user->id, lang('index_active_link')) : anchor("User/activate/" . $user->id, lang('index_inactive_link'));?>
                          </td>
                          <td>
                            <?php echo anchor("User/edit_user/" . $user->id, 'Edit') ;?>
                          </td>
                        </tr>
                      <?php endforeach;?>
                    </tbody>
                  </table>
                  
                  <div class="col s12">
                    <br><br>
                    <p><?php echo anchor('User/create_user', lang('index_create_user_link'), 'class="modal-action modal-close waves-effect waves-light btn-flat green lighten-2 white-text save-contact"')?></p>
                  </div>

                </div>
              </div>
            </div>
          </div>
        </div>
			</div>
		</div>
	</div>

<?php $this->load->view('v2/elements/scripts');?>