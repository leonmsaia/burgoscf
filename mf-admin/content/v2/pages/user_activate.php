<!doctype html>
<html lang="en">
	<?php $this->load->view('v2/elements/head');?>
<body>

<div class="loader-container circle-pulse-multiple">
  <div class="page-loader">
    <div id="loading-center-absolute">
      <div class="object" id="object_four"></div>
      <div class="object" id="object_three"></div>
      <div class="object" id="object_two"></div>
      <div class="object" id="object_one"></div>
    </div>
  </div>
</div>

  <?php $this->load->view('v2/elements/toolbar');?>

  <?php $this->load->view('v2/elements/sidebar');?>

  <div class="content-area">
    <?php $this->load->view('v2/elements/breadcrum');?>
		<div class="widgets-wrapper">
			<div class="row">
        <div class="masonary">
          <div class="col s12">
            <div class="widget z-depth-1">
              <div class="loader"></div>
              <div class="widget-title">
                <h3><?php echo lang('deactivate_heading');?></h3>
                <p><?php echo sprintf(lang('deactivate_subheading'), $user->username);?></p>
              </div>
              <div class="widget-crud">
                <div class="col s12">

                  <?php echo form_open("User/activate/" . $user->id);?>

                    <p>
                      <input id="value_yes" name="confirm" value="yes" type="radio">
                      <label for="value_yes"><?php echo lang('deactivate_confirm_y_label', 'confirm');?></label>
                    </p>
                    <p>
                      <input id="value_no" name="confirm" value="no" type="radio">
                      <label for="value_no"><?php echo lang('deactivate_confirm_n_label', 'confirm');?></label>
                    </p>

                    <?php echo form_hidden($csrf); ?>
                    <?php echo form_hidden(array('id'=>$user->id)); ?>

                    <p><?php echo form_submit('submit', lang('deactivate_submit_btn'), 'class="modal-action modal-close waves-effect waves-light btn-flat green lighten-2 white-text save-contact"');?></p>

                  <?php echo form_close();?>

                </div>
              </div>
            </div>
          </div>
        </div>
			</div>
		</div>
	</div>

<?php $this->load->view('v2/elements/scripts');?>