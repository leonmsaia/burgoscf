<!doctype html>
<html lang="en">
	<?php $this->load->view('v2/elements/head');?>
<body>

<div class="loader-container circle-pulse-multiple">
  <div class="page-loader">
    <div id="loading-center-absolute">
      <div class="object" id="object_four"></div>
      <div class="object" id="object_three"></div>
      <div class="object" id="object_two"></div>
      <div class="object" id="object_one"></div>
    </div>
  </div>
</div>

  <?php $this->load->view('v2/elements/toolbar');?>

  <?php $this->load->view('v2/elements/sidebar');?>

  <div class="content-area">
    <?php $this->load->view('v2/elements/breadcrum');?>
		<div class="widgets-wrapper">
			<div class="row">
        <div class="masonary">
          <div class="col s12">
            <div class="widget z-depth-1">
              <div class="loader"></div>
              <div class="widget-title">
                <h3>Configuracion de Seguridad</h3>
                <p>Cambio y actualizacion de Contraseña</p>
              </div>
              <div class="widget-crud">
                <div class="col s12">

                  <div id="infoMessage"><?php echo $message;?></div>
                  <?php echo form_open("User/change_password_form");?>
                        <div class="row">
                            <div class="input-field col s6">
                                <?php echo lang('change_password_old_password_label', 'old_password');?> <br />
                                <?php echo form_input($old_password);?>
                              </div>
                        </div>
                        <div class="row">
                          <div class="input-field col s6">
                              <label for="new_password"><?php echo sprintf(lang('change_password_new_password_label'), $min_password_length);?></label> <br />
                              <?php echo form_input($new_password);?>
                              </div>
                        </div>
                        <div class="row">
                          <div class="input-field col s6">
                              <?php echo lang('change_password_new_password_confirm_label', 'new_password_confirm');?> <br />
                              <?php echo form_input($new_password_confirm);?>
                            </div>
                        </div>
                        <?php echo form_input($user_id);?>
                        <div class="row">
                          <div class="input-field col s6">
                            <?php echo form_submit('submit', lang('change_password_submit_btn'), 'class="modal-action modal-close waves-effect waves-light btn-flat green lighten-2 white-text save-contact"');?>
                          </div>
                        </div>
                  <?php echo form_close();?>

                </div>
              </div>
            </div>
          </div>
        </div>
			</div>
		</div>
	</div>

<?php $this->load->view('v2/elements/scripts');?>