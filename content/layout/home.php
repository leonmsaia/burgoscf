<!DOCTYPE html>
<html>
	<?php $this->load->view('pages/assets/head');?>
<body>
	
	<div id="loading-animation" class="loading-context">
		<div class="video-holder">
			<video autoplay loop preload="auto">
				<source src="<?php echo base_url()?>assets/video/orgulloytradicion.mp4" type="video/mp4">
				Your browser does not support the video tag.
			</video>
		</div>
	</div>
	

	<div class="container-fluid top-navbar-wrapper">
		<span class="top-navbar-border-white"></span>
		<span class="top-navbar-border-black"></span>
	</div>
	<?php $this->load->view('pages/assets/header-complex');?>

	<?php $this->load->view('pages/home/modules/hero-slider-functional');?>
	
	<div class="container-fluid main-body-container">
		<div class="container">
			<div class="row">
				<?php $this->load->view('pages/home/modules/next-match');?>
				<?php $this->load->view('pages/modules/ads/three-columns-ads');?>
				<?php // $this->load->view('pages/modules/ads/three-columns-ads-custom');?>
				<div class="col-lg-12">
					<div class="row">
						<?php $this->load->view('pages/modules/ads/vertical-ads');?>
						<!-- <div class="col-lg-4 col-md-12">
							<div class="row">
								<?php $this->load->view('pages/modules/positions-table');?>
							</div>
						</div> -->
						<?php $this->load->view('pages/modules/notes/md5-single-note-render');?>
					</div>
					<div class="row module-separator">
						<?php $this->load->view('pages/modules/notes/md6-note-render');?>
					</div>
					<?php $this->load->view('pages/modules/ads/horizontal-ads');?>
					<?php $this->load->view('pages/modules/notes/mediavideo-module');?>
					<?php $this->load->view('pages/modules/twitter-feed-module');?>
					<div class="row module-separator">
						<?php $this->load->view('pages/modules/notes/md4-note-render');?>
					</div>
				</div>
				<div class="col-lg-12">
					<div class="row">
						<div class="col-lg-2"></div>
						<div class="col-lg-8">
							<div class="row">
								<div class="col-lg-12">
									<h3 class="module-title partners-title">
										Nuestros Patrocinadores
									</h3>
								</div>
								<?php foreach ($partners_info->result() as $prtnr): ?>
									<?php if ($prtnr->partners_level == 1): ?>
										<div class="col-lg-12 partner-display-support">
									<?php elseif ($prtnr->partners_level == 2): ?>
										<div class="col-lg-6">
									<?php elseif ($prtnr->partners_level == 3): ?>
										<div class="col-lg-4">
									<?php elseif ($prtnr->partners_level == 4): ?>
										<div class="col-lg-3">
									<?php endif ?>
											<a href="<?php echo $prtnr->partners_link;?>" class="partner-display-wrapper" target="_blank">
												<img class="img-fluid partner-display-img" src="<?php echo base_url() . 'assets/bucket/partners/' . $prtnr->partners_logo;?>" alt="<?php echo $prtnr->partners_name;?>">
											</a>
										</div>
								<?php endforeach ?>
							</div>
						</div>
						<div class="col-lg-2"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php $this->load->view('pages/assets/footer');?>

	<div id="cookie_bar" class="container-fluid alert alert-primary cookie_bar" role="alert">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<p>
						Este portal web únicamente utiliza cookies propias con finalidad técnica, no recaba ni cede datos de carácter personal de los usuarios sin su conocimiento. <br>
						Sin embargo, contiene enlaces a sitios web de terceros con políticas de privacidad ajenas este portal web que usted podrá decidir si acepta o no cuando acceda a ellos.
					</p>
					<button id="cookies_navigate_btn" class="keep_btn">Seguir Navegando</button>
				</div>
			</div>
		</div>
		<script>
			$('#cookies_navigate_btn').click(function() {
				$('#cookie_bar').remove();
			});
		</script>
	</div>
	
	<?php $this->load->view('pages/assets/scripts-bottom');?>
	<script src='<?php echo base_url();?>assets/plyr/plyr.min.js'></script>
	<link rel="stylesheet" href="<?php echo base_url();?>assets/plyr/plyr.css">
	<script>
		var configProfile = {
		  "profile": {"screenName": 'Burgos_CF'},
		  "domId": 'twitter_wrapper',
		  "maxTweets": 6,
		  "showInteraction": false,
		  "enableLinks": true, 
		  "showUser": true,
		  "showTime": true,
		  "showImages": true,
		  "lang": 'es'
		};
		twitterFetcher.fetch(configProfile);
	</script>
	
</body>
</html>
