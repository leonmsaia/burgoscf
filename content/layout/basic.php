<!DOCTYPE html>
<html>
	<?php $this->load->view('pages/assets/head');?>
<body>
	<span id="main_path" style="display: none;"><?php echo base_url();?></span>
	<div class="container-fluid top-navbar-wrapper">
		<span class="top-navbar-border-white"></span>
		<span class="top-navbar-border-black"></span>
	</div>
	
	<?php $this->load->view('pages/assets/header-simplex');?>
	
	<div class="container-fluid main-body-container internal-wrapper">
		<div class="container">
			<!-- Main Web Body  -->
			<div class="row">
				
				<!-- Netx Matchs Modules -->
				<div class="col-md-12 next-match-boxes-container">
					<div class="row">
					</div>
				</div>
				<!-- Netx Matchs Modules End -->
				
				<div class="col-md-12">
					<div class="row">
						<?php $this->load->view('pages/' . $page);?>
					</div>
				</div>
				<!-- General Home Modules -->
				

			</div>
			<!-- Main Web Body End -->
			
		</div>
	</div>
	
	<?php $this->load->view('pages/assets/footer');?>
	<?php $this->load->view('pages/assets/scripts-bottom');?>
</body>
</html>
