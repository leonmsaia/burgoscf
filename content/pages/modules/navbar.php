<nav class="navbar navbar-expand-lg">
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNav">
    <ul class="navbar-nav">  
      <?php foreach ($navbar_render->result() as $nvbr_elem): ?>
        <?php if ($nvbr_elem->static_pages_navbar_dropdown == 0): ?>
          <li class="nav-item">
            <?php if (strpos($nvbr_elem->static_pages_navbar_slug, "http://") !== false OR strpos($nvbr_elem->static_pages_navbar_slug, "https://") !== false): ?>
              <a class="nav-link" target="_blank" href="<?php echo $nvbr_elem->static_pages_navbar_slug;?>">
                <?php echo $nvbr_elem->static_pages_navbar_title;?>
              </a>
            <?php else: ?>
              <a class="nav-link" href="<?php echo base_url() . $nvbr_elem->static_pages_navbar_slug;?>">
                <?php echo $nvbr_elem->static_pages_navbar_title;?>
              </a>
            <?php endif ?>
          </li>
        <?php else: ?>
          <?php $childs_elements = get_child_elements_for_navbar_elements($nvbr_elem->static_pages_navbar_id);?>
          <?php $childs_elements_direct = get_child_elements_direct_for_navbar_elements($nvbr_elem->static_pages_navbar_id);?>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="<?php echo $nvbr_elem->static_pages_navbar_slug;?>_dropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <?php echo $nvbr_elem->static_pages_navbar_title;?>
            </a>
            <div class="dropdown-menu" aria-labelledby="<?php echo $nvbr_elem->static_pages_navbar_slug;?>_dropdown">
              <?php foreach ($childs_elements->result() as $chld_elem): ?>
                <a class="dropdown-item" href="<?php echo base_url() . 'institucional/' . $chld_elem->static_pages_slug ?>">
                  <?php echo $chld_elem->static_pages_title;?>
                </a>
              <?php endforeach ?>
              <?php foreach ($childs_elements_direct->result() as $chld_elem_drc): ?>
                <?php if (strpos($chld_elem_drc->static_pages_navbar_slug, "http://") !== false OR strpos($chld_elem_drc->static_pages_navbar_slug, "https://") !== false): ?>
                  <a class="dropdown-item" target="_blank" href="<?php echo $chld_elem_drc->static_pages_navbar_slug;?>">
                    <?php echo $chld_elem_drc->static_pages_navbar_title;?>
                  </a>
                <?php else: ?>
                  <a class="dropdown-item" href="<?php echo base_url() . $chld_elem_drc->static_pages_navbar_slug;?>">
                    <?php echo $chld_elem_drc->static_pages_navbar_title;?>
                  </a>
                <?php endif ?>
              <?php endforeach ?>
            </div>
          </li>
        <?php endif ?>
      <?php endforeach ?>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="special_dropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Cantera
        </a>
        <div class="dropdown-menu burgos_promesas_dropdown" aria-labelledby="special_dropdown">
          <a class="dropdown-item special-item" target="_blank" href="http://www.burgospromesas.com/">
           Burgos Promesas
          </a>
        </div>
      </li>
    </ul>
  </div>
</nav>