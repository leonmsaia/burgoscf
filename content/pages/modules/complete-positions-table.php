<div class="col-md-12 fixture-module-container">
	<table class="table main-title-fixture">
		<thead>
			<tr>
				<th>
					2 <small>a</small> B Grupo II
				</th>
			</tr>
		</thead>
	</table>
	<table class="table main-table-title-fixture">
		<thead>
			<tr>
				<th scope="col">#</th>
				<th scope="col">Equipo</th>
				<th scope="col">PTS</th>
				<th scope="col">G</th>
				<th scope="col">E</th>
				<th scope="col">P</th>
			</tr>
		</thead>
		<tbody class="main-body-fixture">
			<tr>
				<td scope="row">1</td>
				<td>UD Logroñes</td>
				<td>62</td>
				<td>18</td>
				<td>18</td>
				<td>18</td>
			</tr>
			<tr>
				<td scope="row">2</td>
				<td>UD Logroñes</td>
				<td>62</td>
				<td>18</td>
				<td>18</td>
				<td>18</td>
			</tr>
			<tr>
				<td scope="row">3</td>
				<td>UD Logroñes</td>
				<td>62</td>
				<td>18</td>
				<td>18</td>
				<td>18</td>
			</tr>
			<tr>
				<td scope="row">4</td>
				<td>UD Logroñes</td>
				<td>62</td>
				<td>18</td>
				<td>18</td>
				<td>18</td>
			</tr>
			<tr>
				<td scope="row">5</td>
				<td>UD Logroñes</td>
				<td>62</td>
				<td>18</td>
				<td>18</td>
				<td>18</td>
			</tr>
			<tr>
				<td scope="row">6</td>
				<td>UD Logroñes</td>
				<td>62</td>
				<td>18</td>
				<td>18</td>
				<td>18</td>
			</tr>
			<tr>
				<td scope="row">7</td>
				<td>UD Logroñes</td>
				<td>62</td>
				<td>18</td>
				<td>18</td>
				<td>18</td>
			</tr>
			<tr class="burgos-club-selector">
				<td scope="row">8</td>
				<td>Burgos CF</td>
				<td>62</td>
				<td>18</td>
				<td>18</td>
				<td>18</td>
			</tr>
			<tr>
				<td scope="row">9</td>
				<td>UD Logroñes</td>
				<td>62</td>
				<td>18</td>
				<td>18</td>
				<td>18</td>
			</tr>
			<tr>
				<td scope="row">10</td>
				<td>UD Logroñes</td>
				<td>62</td>
				<td>18</td>
				<td>18</td>
				<td>18</td>
			</tr>
			<tr>
				<td scope="row">11</td>
				<td>UD Logroñes</td>
				<td>62</td>
				<td>18</td>
				<td>18</td>
				<td>18</td>
			</tr>
			<tr>
				<td scope="row">12</td>
				<td>UD Logroñes</td>
				<td>62</td>
				<td>18</td>
				<td>18</td>
				<td>18</td>
			</tr>
			<tr>
				<td scope="row">13</td>
				<td>UD Logroñes</td>
				<td>62</td>
				<td>18</td>
				<td>18</td>
				<td>18</td>
			</tr>
			<tr>
				<td scope="row">14</td>
				<td>UD Logroñes</td>
				<td>62</td>
				<td>18</td>
				<td>18</td>
				<td>18</td>
			</tr>
			<tr>
				<td scope="row">15</td>
				<td>UD Logroñes</td>
				<td>62</td>
				<td>18</td>
				<td>18</td>
				<td>18</td>
			</tr>
			<tr>
				<td scope="row">16</td>
				<td>UD Logroñes</td>
				<td>62</td>
				<td>18</td>
				<td>18</td>
				<td>18</td>
			</tr>
			<tr>
				<td scope="row">17</td>
				<td>UD Logroñes</td>
				<td>62</td>
				<td>18</td>
				<td>18</td>
				<td>18</td>
			</tr>
			<tr>
				<td scope="row">18</td>
				<td>UD Logroñes</td>
				<td>62</td>
				<td>18</td>
				<td>18</td>
				<td>18</td>
			</tr>
			<tr>
				<td scope="row">19</td>
				<td>UD Logroñes</td>
				<td>62</td>
				<td>18</td>
				<td>18</td>
				<td>18</td>
			</tr>
			<tr>
				<td scope="row">20</td>
				<td>UD Logroñes</td>
				<td>62</td>
				<td>18</td>
				<td>18</td>
				<td>18</td>
			</tr>
		</tbody>
	</table>
</div>