<div class="col-md-12 mobile-header-wrapper">
	<div class="row">
		<div class="col-md-12">
			<a class="navbar-brand" href="<?php echo base_url();?>">
				<img class="bn_logo" src="<?php echo base_url();?>assets/logo_bn.svg" alt="">
				<img class="w_logo" src="<?php echo base_url();?>assets/logo_w.svg" alt="">
			</a>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<nav class="navbar navbar-light">
				<button class="navbar-toggler toggler-example" type="button" data-toggle="collapse" data-target="#navbarSupportedContent1"
				aria-controls="navbarSupportedContent1" aria-expanded="false" aria-label="Toggle navigation">
					<span class="dark-blue-text">
							<i class="fas fa-bars fa-1x"></i>
					</span>
				</button>
				<div class="collapse navbar-collapse" id="navbarSupportedContent1">
					<ul class="navbar-nav">  
				      <?php foreach ($navbar_render->result() as $nvbr_elem): ?>
				        <?php if ($nvbr_elem->static_pages_navbar_dropdown == 0): ?>
				          <li class="nav-item">
				            <a class="nav-link" href="<?php echo base_url() . $nvbr_elem->static_pages_navbar_slug;?>">
				              <?php echo $nvbr_elem->static_pages_navbar_title;?>
				            </a>
				          </li>
				        <?php else: ?>
				          <?php $childs_elements = get_child_elements_for_navbar_elements($nvbr_elem->static_pages_navbar_id);?>
				          <?php $childs_elements_direct = get_child_elements_direct_for_navbar_elements($nvbr_elem->static_pages_navbar_id);?>
				          <li class="nav-item dropdown">
				            <a class="nav-link dropdown-toggle" href="#" id="<?php echo $nvbr_elem->static_pages_navbar_slug;?>_dropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				              <?php echo $nvbr_elem->static_pages_navbar_title;?>
				            </a>
				            <div class="dropdown-menu" aria-labelledby="<?php echo $nvbr_elem->static_pages_navbar_slug;?>_dropdown">
				              <?php foreach ($childs_elements->result() as $chld_elem): ?>
				                <a class="dropdown-item" href="<?php echo base_url() . 'institucional/' . $chld_elem->static_pages_slug ?>">
				                  <?php echo $chld_elem->static_pages_title;?>
				                </a>
				              <?php endforeach ?>
				              <?php foreach ($childs_elements_direct->result() as $chld_elem_drc): ?>
				                <a class="dropdown-item" href="<?php echo base_url() . $chld_elem_drc->static_pages_navbar_slug ?>">
				                  <?php echo $chld_elem_drc->static_pages_navbar_title;?>
				                </a>
				              <?php endforeach ?>
				            </div>
				          </li>
				        <?php endif ?>
				      <?php endforeach ?>
				    </ul>
				</div>
			</nav>
		</div>
	</div>
</div>