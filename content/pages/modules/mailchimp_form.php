<div class="row footer-module-wrapper">
	<div class="col-md-12">
		<h4 class="footer-module-title">NEWSLETTERS</h4>
	</div>
	<div class="col-md-12">
		<div class="row">
			<div class="col-md-2"></div>
			<?php echo form_open('Common/add_to_mailchimp_list', 'class="col-md-8" id="mailchimp_form_trigger"');?>
				<input class="validate-required mailchimp-form-input" type="text" name="mail_data" id="mail_data" placeholder="Ingrese su E-Mail" />
				<button type="submit" id="subscribe_btn" class="btn btn--secondary readmore-button-mailchimp">Subscribirme</button>
			<?php echo form_close();?>
			<div class="col-md-2"></div>
		</div>
		<div class="row">
			<div class="col-md-2"></div>
			<div class="col-md-8">
				<div id="alert_trigger_suc" class="alert alert-primary newsletter-alert success" role="alert">
				  Gracias por sumarte a nuestro Newsletter!
				</div>
				<div id="alert_trigger_dan" class="alert alert-danger newsletter-alert danger" role="alert">
				  Ocurrio un error, vuelva a intentar!
				</div>
				
			</div>
			<div class="col-md-2"></div>
		</div>
	</div>
	<div class="col-md-12">
		<p class="footer-module-desc">PROXIMAMENTE ENTERATE DE<br>LAS ULTIMAS NOTICIAS</p>
	</div>
</div>

<script>
	$('#mailchimp_form_trigger').submit(function(event) {
		event.preventDefault();
		var form = $(this);
		var urlForm = form.attr('action');
		var mail_data = $("#mail_data").val();
		$.ajax({
			url: urlForm,
			type: 'POST',
			data: {
				mail_data: mail_data
			}
		})
		.done(function(res) {
			$('#alert_trigger_suc').css('display', 'inherit');
			$('#subscribe_btn').attr('disabled', true);
		})
		.fail(function() {
			$('#alert_trigger_dan').css('display', 'inherit');
			$('#subscribe_btn').attr('disabled', true);
		});
	});
</script>