<div class="row module-separator three_columns_ads">
	<?php foreach ($horizontal_ads_three_columns->result() as $hztnl_ds): ?>
		<?php
			$banner_component = 
				'<img src="' . base_url() . 'assets/bucket/ads/' . $hztnl_ds->ads_module_image . '" alt="' . $hztnl_ds->ads_module_title . '" class="img-ads-display">';
		?>
		<?php if ($hztnl_ds->ads_module_slug != NULL): ?>
			<div class="col-md-4">
				<a href="<?php echo $hztnl_ds->ads_module_slug;?>" target="_blank">
					<?php echo $banner_component;?>
				</a>
			</div>
		<?php else: ?>
			<div class="col-md-4">
				<?php echo $banner_component;?>
			</div>
		<?php endif ?>
	<?php endforeach ?>
</div>

