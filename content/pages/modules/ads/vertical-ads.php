<?php foreach ($vertical_ads->result() as $vrtcl_ds): ?>	
	<div class="col-lg-4 col-md-2 vertical-ads-wrapper">
		<?php
			$banner_component = 
			'<div class="col-lg-12">' . 
				'<img src="' . base_url() . 'assets/bucket/ads/' . $vrtcl_ds->ads_module_image . '" alt="' . $vrtcl_ds->ads_module_title . '" class="img-ads-display">' .
			'</div>';
		?>
		<?php if ($vrtcl_ds->ads_module_slug != NULL): ?>
			<a href="<?php echo $vrtcl_ds->ads_module_slug;?>" target="_blank">
				<?php echo $banner_component;?>
			</a>
		<?php else: ?>
			<?php echo $banner_component;?>
		<?php endif ?>
	</div>
<?php endforeach ?>