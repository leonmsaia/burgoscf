<?php foreach ($horizontal_ads->result() as $hztnl_ds): ?>
	<div class="row module-separator">
		<?php
			$banner_component = 
			'<div class="col-md-12">' . 
				'<img src="' . base_url() . 'assets/bucket/ads/' . $hztnl_ds->ads_module_image . '" alt="' . $hztnl_ds->ads_module_title . '" class="img-ads-display">' .
			'</div>';
		?>
		<?php if ($hztnl_ds->ads_module_slug != NULL): ?>
			<a href="<?php echo $hztnl_ds->ads_module_slug;?>" target="_blank">
				<?php echo $banner_component;?>
			</a>
		<?php else: ?>
			<?php echo $banner_component;?>
		<?php endif ?>
	</div>
<?php endforeach ?>