<?php 
	$limit = 3;
	$counter = 0;
?>
<div class="row module-separator">
	
<?php foreach ($all_mediavideos->result() as $mdvd): ?>
	<?php
		$link_builder = base_url() . 'multimedia/' . $mdvd->slug . '/' . $mdvd->post_name;
	?>	
	<?php if ($limit != $counter): ?>
		<?php $video_code = get_post_mediavideo($mdvd->ID);?>			
		<div class="col-lg-4 col-md-6">
			<div class="row note-display-wrapper">
				<div class="col-lg-12 note-display-category-wrapper">
					<a class="single-note-slug-featured" href="<?php echo base_url() . 'galeria/video/' . $mdvd->slug;?>">
						<h3 class="note-display-category">
							<?php echo $mdvd->name;?> - <?php echo convert_date($mdvd->post_date);?>
						</h3>
					</a>
				</div>
				<div class="col-md-12">
				  <div class="iframe-wrapper plyr__video-embed" id="player">
					  <iframe
					  	class="mediavideo_wrapper"
					    src="https://www.youtube.com/embed/<?php echo $video_code;?>?origin=https://plyr.io&amp;iv_load_policy=3&amp;modestbranding=1&amp;playsinline=1&amp;showinfo=0&amp;rel=0&amp;enablejsapi=1"
					    allowfullscreen
					    allowtransparency
					    allow="autoplay"
					  ></iframe>
				  </div>
				</div>
				<div class="col-lg-12 note-display-title-wrapper">
					<a class="single-note-slug-featured" href="<?php echo $link_builder;?>">
						<h1 class="note-display-title"><?php echo $mdvd->post_title;?></h1>
					</a>
				</div>
			</div>
		</div>
		<?php $counter ++;?>
	<?php endif ?>

<?php endforeach ?>
</div>