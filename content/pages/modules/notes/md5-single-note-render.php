<?php 
	$limit = 2;
	$counter = 0;
?>
<?php foreach ($all_posts->result() as $nt_rndr): ?>
	<?php
		$link_builder = base_url() . 'noticias/' . $nt_rndr->slug . '/' . $nt_rndr->post_name;
	?>	
	<?php if ($blog_home_config->blog_home_suit_config_single_category == $nt_rndr->slug): ?>
		<?php if ($limit != $counter): ?>
			<div class="col-lg-4 col-md-5 md-single-note-render-wrapper">
				<div class="row note-display-wrapper not-separated">
					<div class="col-lg-12 note-display-category-wrapper">
						<a class="single-note-slug-featured" href="<?php echo base_url() . 'noticias/' . $nt_rndr->slug;?>">
							<h3 class="note-display-category">
								<?php echo $nt_rndr->name;?> - <?php echo convert_date($nt_rndr->post_date);?>
							</h3>
						</a>
					</div>
					<div class="col-lg-12 note-display-image-wrapper">
						<img src="<?php echo get_post_cover_image_featured($nt_rndr->ID);?>" alt="" class="note-display-image">
						<a href="<?php echo base_url() .'noticias/' . $nt_rndr->slug . '/' . $nt_rndr->post_name;?>" class="readmore-button">Leer</a>
					</div>
					<div class="col-lg-12 note-display-title-wrapper">
						<a class="single-note-slug-featured" href="<?php echo $link_builder;?>">
							<h1 class="note-display-title"><?php echo $nt_rndr->post_title;?></h1>
						</a>
					</div>
				</div>
			</div>
			<?php $counter ++;?>
		<?php endif ?>
	<?php endif ?>
<?php endforeach ?>