<div class="col-md-12 fixture-module-container">
	<table class="table main-title-fixture">
		<thead>
			<tr>
				<th>
					2 <small>a</small> B Grupo II
				</th>
			</tr>
		</thead>
	</table>
	<table class="table main-table-title-fixture">
		<thead>
			<tr>
				<th scope="col">#</th>
				<th class="home-table-name-desktop" scope="col">Equipo</th>
				<th scope="col">PTS</th>
				<th scope="col">G</th>
				<th scope="col">E</th>
				<th scope="col">P</th>
			</tr>
		</thead>
		<tbody class="main-body-fixture">
			<?php $counter = 1;?>
			<?php foreach ($team_all->result() as $tm_sts): ?>
				<?php if ($tm_sts->team_id == 8): ?>
				<tr class="burgos-club-selector">
				<?php else: ?>
				<tr>
				<?php endif ?>
					<td scope="row"><?php echo $counter;?></td>
					<td class="home-table-name-desktop">
						<?php echo character_limiter($tm_sts->team_name, 8);?>
					</td>
					<td><?php echo $tm_sts->team_points;?></td>
					<td><?php echo $tm_sts->team_points_win;?></td>
					<td><?php echo $tm_sts->team_points_tie;?></td>
					<td><?php echo $tm_sts->team_points_lose;?></td>
				</tr>
			<?php $counter++;?>
			<?php endforeach ?>
		</tbody>
	</table>
</div>
<div class="col-md-12">
	<a href="<?php echo base_url();?>tabla_posiciones_completa" class="view-class-position">Clasificación Completa</a>
</div>