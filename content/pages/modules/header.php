<div class="row header-container">
	<div class="col-lg-12 regular-header-wrapper">
		<div class="row">
			<div class="col-lg-2">
				<a href="<?php echo base_url();?>">
					<img class="logo-holder" src="<?php echo base_url();?>assets/logo_w.svg" alt="">
				</a>
			</div>
			<div class="col-lg-10">
				<?php $this->load->view('pages/modules/navbar');?>
			</div>
		</div>
	</div>
	<?php $this->load->view('pages/modules/navbar-mobile');?>
</div>