<div class="col-md-12 note-display-inside">
	<div class="row note-display-wrapper">
		<div class="col-md-12 note-display-category-wrapper">
			<h3 class="note-display-category">Conocenos</h3>
		</div>
		<div class="col-md-12 note-display-title-wrapper">
			<h1 class="note-display-title">Primer equipo</h1>
			<br><br>
		</div>
		<div class="col-md-12 note-display-bodytext">
			<div class="row note-display-wrapper team-display-wrapper">
				<?php foreach ($positions_all->result() as $pstn): ?>
					<div class="card col-md-4 element-listing-container">
						<a href="<?php echo base_url();?>primer_equipo/<?php echo $pstn->team_player_position_slug;?>">
							<img src="<?php echo base_url();?>assets/bucket/general_inner/<?php echo $pstn->team_player_position_picture;?>" class="card-img-top" alt="<?php echo $pstn->team_player_position_name;?>">
							<div class="card-body">
								<p class="card-text"><?php echo $pstn->team_player_position_name;?></p>
							</div>
						</a>
					</div>
				<?php endforeach ?>
			</div>
		</div>
		<?php $this->load->view('pages/modules/social/socialmedia-share');?>
	</div>
</div>