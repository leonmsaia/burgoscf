<div class="col-lg-12 note-display-inside">	
	<div class="row note-display-wrapper">
		<div class="col-lg-12 note-display-category-wrapper">
			<?php foreach ($position_data->result() as $pst_data): ?>
				<h3 class="note-display-category"><?php echo $pst_data->team_player_position_name;?></h3>
			<?php endforeach ?>
		</div>
		<div class="col-lg-12 note-display-bodytext">
			<div class="row note-display-wrapper team-display-wrapper">
				<?php foreach ($all_players_in_position->result() as $plyr): ?>
					<div class="card col-lg-4 element-listing-container">
						<a href="<?php echo base_url();?>primer_equipo/<?php echo $position_slug_txt . '/' . $plyr->team_player_slug;?>">
							<img src="<?php echo base_url() . 'assets/bucket/players/' . $plyr->team_player_picture;?>" class="card-img-top" alt="<?php echo $plyr->team_player_name;?>">
							<div class="card-body">
								<p class="card-text" style="text-align: center;">
									<?php echo $plyr->team_player_name;?>
									<?php echo $plyr->team_player_middle;?>
									<?php echo $plyr->team_player_lastname;?>
								</p>
							</div>
						</a>
					</div>
				<?php endforeach ?>
			</div>
			<br><hr>
		</div>
		<div class="col-lg-12">
			<a href="<?php echo base_url();?>primer_equipo" class="readmore-button">Volver</a>
		</div>
		<?php $this->load->view('pages/modules/social/socialmedia-share');?>
	</div>
</div>