<link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/theme.css">

<link rel="stylesheet" href="<?php echo base_url();?>assets/css/font-awesome.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/all.css">

<link rel="stylesheet" href="<?php echo base_url();?>assets/css/jssocials.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/jssocials-theme-plain.css">

<link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,200;0,500;0,900;1,200;1,500;1,900&display=swap" rel="stylesheet">

<link rel="stylesheet" href="<?php echo base_url();?>assets/css/leaflet.css">