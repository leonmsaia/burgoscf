<div class="container-fluid main-footer-container">
	<div class="main-footer-design-white"></div>
	<div class="main-footer-design-grass"></div>
</div>
<div class="container-fluid main-footer-container dark-bg">
	<div class="container main-footer-body">
		<div class="row">
			<div class="col-md-4">
				<div class="row footer-module-wrapper">
					<div class="col-md-12">
						<h4 class="footer-module-title">REDES SOCIALES</h4>
					</div>
					<div class="col-md-12 display-social-share-box footer-social">
						<?php $socialmedia_info = get_footer_socialmedia_icons();?>
						<ul class="social-share-list-container">
							<?php foreach ($socialmedia_info->result() as $scl_dt): ?>
								<li class="social-share-list-item-container">
									<a href="<?php echo $scl_dt->contact_information_url;?>" target="_blank"
									>
										<i class="fab <?php echo $scl_dt->contact_information_icon;?>	"></i>
									</a>
								</li>
							<?php endforeach ?>
						</ul>
						<p class="footer-module-desc">UNETE A NOSOTROS<br>EN LAS REDES SOCIALES</p>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="footer-module-logo-container">
					<a href="<?php echo base_url();?>">
						<img src="<?php echo base_url();?>assets/logo_w.svg" alt="">
					</a>
				</div>
			</div>
			<div class="col-md-4">
				<img src="<?php echo base_url();?>assets/img/assets/footer_legnd.png" alt="" style="width: 100%;">
				<?php // $this->load->view('pages/modules/mailchimp_form');?>
			</div>
		</div>
	</div>
</div>
<div class="container-fluid main-footer-container">
	<div class="container footer-menu-container">
		<ul class="footer-menu-wrapper">
			<?php foreach ($navbar_render->result() as $nvbr_elem): ?>
				<li class="footer-menu-element">
					<a href="<?php echo base_url() . $nvbr_elem->static_pages_navbar_slug;?>">
						<?php echo $nvbr_elem->static_pages_navbar_title;?>
					</a>
				</li>
			<?php endforeach ?>
		</ul>
	</div>
	<div class="container footer-copy-container">
		<p class="copy-txt">&copy; <?php echo date('Y');?>. desarrollado por burgos c.f. 200. Todos los derechos reservados.</p>
		<p class="legal-txt">
			<a href="<?php echo base_url();?>legal/aviso">aviso</a> | 
			<a href="<?php echo base_url();?>legal/politica-de-cookies">politica de cookies</a> | 
			<a href="<?php echo base_url();?>legal/politica-de-privacidad">politica de privacidad</a>
		</p>
	</div>
	<div class="main-footer-design-line"></div>
</div>