<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Burgos CF</title>
	<link rel="apple-touch-icon" sizes="180x180" href="<?php echo base_url();?>assets/favicon/apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="<?php echo base_url();?>assets/favicon/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url();?>assets/favicon/favicon-16x16.png">
	<link rel="manifest" href="<?php echo base_url();?>assets/favicon/site.webmanifest">
	<link rel="mask-icon" href="<?php echo base_url();?>assets/favicon/safari-pinned-tab.svg" color="#5bbad5">
	<meta name="msapplication-TileColor" content="#da532c">
	<meta name="theme-color" content="#ffffff">
	<?php $this->load->view('pages/assets/style');?>
	<?php $this->load->view('pages/assets/scripts-top');?>
</head>