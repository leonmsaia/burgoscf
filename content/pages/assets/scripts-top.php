<script src="<?php echo base_url();?>assets/js/jquery-3.2.1.min.js"></script>
<script src="<?php echo base_url();?>assets/js/bootstrap.js"></script>
<script src="<?php echo base_url();?>assets/js/all.js"></script>
<script src="<?php echo base_url();?>assets/js/leaflet.js"></script>
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-173463186-1"></script>
<script>
	window.dataLayer = window.dataLayer || [];
	function gtag(){dataLayer.push(arguments);}
	gtag('js', new Date());
	gtag('config', 'UA-173463186-1');
</script>