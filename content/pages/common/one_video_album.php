<?php $img_path_construct = $this->config->item('blog_url') . 'wp-content/';?>
<?php
	foreach ($get_current_album_data->result() as $vdo_ct) {
		$album_title = $vdo_ct->name;
		$album_desc = $vdo_ct->description;
	}
?>
<div class="col-md-12 note-display-inside">
	<div class="row note-display-wrapper">
		<div class="col-md-12 note-display-category-wrapper">
			<h3 class="note-display-category">Colecciones Audiovisuales</h3>
		</div>
		<div class="col-md-12 note-display-title-wrapper">
			<h1 class="note-display-title"><?php echo $album_title;?></h1>
			<p><?php echo $album_desc;?></p>
			<br><br>
		</div>
		<div class="col-md-12 note-display-bodytext">
			<div class="row">
				<div class="col-md-12 note-display-title-wrapper">
					<h3 class="subsection-title">Videos</h3>
				</div>
				<?php foreach ($album_video->result() as $pic): ?>
					<?php $video_code = get_post_mediavideo($pic->ID);?>
					<div class="col-md-4">
						<div class="card" style="width: 18rem;">
							<iframe
								class="mediavideo_wrapper"
								src="https://www.youtube.com/embed/<?php echo $video_code;?>?origin=https://plyr.io&amp;iv_load_policy=3&amp;modestbranding=1&amp;playsinline=1&amp;showinfo=0&amp;rel=0&amp;enablejsapi=1"
								allowfullscreen
								allowtransparency
								allow="autoplay"
							></iframe>
							<div class="card-body">
								<h5 class="card-title">
									<?php echo $pic->post_title;?> - <?php echo convert_date($pic->post_date);?>
								</h5>
								<p class="card-text"><?php echo $pic->post_excerpt;?></p>
							</div>
						</div>
					</div>
				<?php endforeach ?>
			</div>
		</div>
		<div class="col-md-12 note-display-title-wrapper">
			<a class="readmore-button" href="<?php echo base_url();?>galeria/">Volver al Listado de Albumes</a>
		</div>
		<?php $this->load->view('pages/modules/social/socialmedia-share');?>
	</div>
</div>