<div class="col-lg-12 note-display-inside">
	<div class="row note-display-wrapper">
		<div class="col-lg-12 note-display-category-wrapper">
			<h3 class="note-display-category">Formulario de solicitud de acreditaciones</h3>
		</div>
		<div class="col-lg-12 note-display-title-wrapper">
			<p>
				Las solicitudes por parte de los medios de comunicación deben solicitarse hasta 48 horas antes del encuentro. Todas aquellas acreditaciones solicitadas fuera de plazo no serán admitidas.
			</p>
			<br><br>
		</div>
		
		<div class="col-lg-12">
		    <?php echo form_open('Common/press_form_process', 'id="form_prss_form"');?>
		        <div class="row">
		            <div class="col-lg-4">
		                <div class="form-group">
		                    <label for="contact_form_name">Nombre <span style="color:red;">*</span></label>
		                    <input name="contact_form_name" type="text" class="form-control" id="contact_form_name" required>
		                </div>
		            </div>
		            <div class="col-lg-4">
		                <div class="form-group">
		                    <label for="contact_form_lastname">Apellido <span style="color:red;">*</span></label>
		                    <input name="contact_form_lastname" type="text" class="form-control" id="contact_form_lastname" required>
		                </div>
		            </div>
		            <div class="col-lg-4">
		                <div class="form-group">
		                    <label for="contact_form_id">D.N.I. <span style="color:red;">*</span></label>
		                    <input name="contact_form_id" type="text" class="form-control" id="contact_form_id" required>
		                </div>
		            </div>
		        </div>
		        <div class="row">
		            <div class="col-lg-4">
		                <div class="form-group">
		                    <label for="contact_form_mail">E-Mail <span style="color:red;">*</span></label>
		                    <input name="contact_form_mail" type="email" class="form-control" id="contact_form_mail" required>
		                </div>
		            </div>
		            <div class="col-lg-4">
		                <div class="form-group">
		                    <label for="contact_form_phone">Teléfono <span style="color:red;">*</span></label>
		                    <input name="contact_form_phone" type="text" class="form-control" id="contact_form_phone" required>
		                </div>
		            </div>
		            <div class="col-lg-4">
		                <div class="form-group">
		                    <label for="contact_form_company">Medio o Club <span style="color:red;">*</span></label>
		                    <input name="contact_form_company" type="text" class="form-control" id="contact_form_company" required>
		                </div>
		            </div>
		        </div>
		        <div class="row">
		            <div class="col-lg-12">
		                <div class="form-group">
		                    <label for="contact_form_message">Mensaje <span style="color:red;">*</span></label>
		                    <textarea name="contact_form_message" class="form-control" id="contact_form_message" rows="3" required></textarea>
		                </div>
		            </div>
		        </div>
		        <div class="row">
		        	<div class="col-lg-12">
		        		<p>
		        			<small style="color:red;">* Todos los campos son obligatorios.</small>
		        		</p>
		        	</div>
		            <div class="col-lg-4">
		                <div class="form-check">
		                    <input name="contact-form-policies" class="form-check-input" type="checkbox" value="1" id="contact_form_policies">
		                    <label class="form-check-label" for="policies">
		                        Acepto las politicas de privacidad
		                    </label>
		                </div>
		            </div>
		        </div>
		        <div class="row">
		            <div class="col-lg-4">
		                <div class="form-group">
		                    <button type="submit" id="submit_press_form" class="btn readmore-button">
		                    	Enviar
		                    </button>
		                </div>
		            </div>
		        </div>
		        <div class="row">
		        	<div class="col-lg-12">
		        		<div id="alert_trigger_suc" class="alert alert-primary newsletter-alert success" role="alert">
					 		Gracias por comunicarte, un representante se contactará contigo.
						</div>
						<div id="alert_trigger_dan" class="alert alert-danger newsletter-alert danger" role="alert">
						  Ocurrio un error, vuelva a intentar!
						</div>
		        	</div>
		        </div>
		    <?php echo form_close();?>
		</div>
		
		
	</div>
</div>

<script>
	$('#form_prss_form').submit(function(event) {

		event.preventDefault();
		var form = $(this);
		var urlForm = form.attr('action');

		var name_data = $('#contact_form_name').val();
		var lastname_data = $('#contact_form_lastname').val();
		var id_data = $('#contact_form_id').val();
		var mail_data = $('#contact_form_mail').val();
		var phone_data = $('#contact_form_phone').val();
		var company_data = $('#contact_form_company').val();
		var message_data = $('#contact_form_message').val();

		$.ajax({
			url: urlForm,
			type: 'POST',
			data: {
				contact_form_name: name_data,
				contact_form_lastname: lastname_data,
				contact_form_id: id_data,
				contact_form_mail: mail_data,
				contact_form_phone: phone_data,
				contact_form_company: company_data,
				contact_form_message: message_data
			}
		})
		.done(function(res) {
			$('#alert_trigger_suc').css('display', 'inherit');
			$('#submit_press_form').attr('disabled', true);
		})
		.fail(function() {
			$('#alert_trigger_dan').css('display', 'inherit');
			$('#submit_press_form').attr('disabled', true);
		});
	});
</script>