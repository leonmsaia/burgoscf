<div class="col-md-12 note-display-inside">
	<div class="row note-display-wrapper">
		<div class="col-md-12 note-display-category-wrapper">
			<h3 class="note-display-category">Conocenos</h3>
		</div>
		<div class="col-md-12 note-display-title-wrapper">
			<h1 class="note-display-title">La Cantera</h1>
			<br><br>
		</div>
		<div class="col-md-12 note-display-bodytext">
			<div class="row note-display-wrapper team-display-wrapper">
				<?php foreach ($static_page_info->result() as $sttc): ?>
					<div class="card col-md-4 element-listing-container">
						<a href="<?php echo base_url();?>institucional/<?php echo $sttc->static_pages_slug;?>">
							<img src="<?php echo base_url();?>assets/bucket/cover/<?php echo $sttc->static_pages_main_image;?>" class="card-img-top" alt="<?php echo $sttc->static_pages_main_title;?>">
							<div class="card-body">
								<p class="card-text"><?php echo $sttc->static_pages_main_title;?></p>
							</div>
						</a>
					</div>
				<?php endforeach ?>
			</div>
		</div>
		<?php $this->load->view('pages/modules/social/socialmedia-share');?>
	</div>
</div>