<div class="col-md-12 note-display-inside">
	<div class="row note-display-wrapper">
		<div class="col-md-12 note-display-category-wrapper">
			<h3 class="note-display-category">Temporada 2019 - 2020</h3>
		</div>
		<div class="col-md-12 fixture-module-container note-display-title-wrapper">
			<table class="table main-title-fixture main-title-fixture-complete">
				<thead>
					<tr>
						<th>
							2 <small>a</small> B Grupo II
						</th>
					</tr>
				</thead>
			</table>
			<table class="table main-table-title-fixture main-table-title-fixture-complete">
				<thead>
					<tr>
						<th scope="col">#</th>
						<th scope="col">Equipo</th>
						<th scope="col">J</th>
						<th scope="col">G</th>
						<th scope="col">E</th>
						<th scope="col">P</th>
						<th scope="col">GF</th>
						<th scope="col">GC</th>
						<th scope="col">DIF</th>
						<th scope="col">PTS</th>
					</tr>
				</thead>
				<tbody class="main-body-fixture">
					<?php $counter = 1;?>
					<?php foreach ($team_all->result() as $tm_sts): ?>
						<?php if ($tm_sts->team_id == 8): ?>
						<tr class="burgos-club-selector">
						<?php else: ?>
						<tr>
						<?php endif ?>
							<td scope="row"><?php echo $counter;?></td>
							<td><?php echo $tm_sts->team_name;?></td>
							<td><?php echo $tm_sts->team_points_plays;?></td>
							<td><?php echo $tm_sts->team_points_win;?></td>
							<td><?php echo $tm_sts->team_points_tie;?></td>
							<td><?php echo $tm_sts->team_points_lose;?></td>
							<td><?php echo $tm_sts->team_points_goalup;?></td>
							<td><?php echo $tm_sts->team_points_goaldown;?></td>
							<td><?php echo $tm_sts->team_points_goaldif;?></td>
							<td><?php echo $tm_sts->team_points;?></td>
						</tr>
					<?php $counter++;?>
					<?php endforeach ?>
				</tbody>
			</table>
		</div>
		
	</div>
</div>