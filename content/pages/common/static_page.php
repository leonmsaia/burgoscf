<?php foreach ($static_page_info->result() as $sttc_nfo): ?>
	<div class="col-md-12 note-display-inside">
		<div class="row note-display-wrapper">
			<div class="col-md-12 note-display-title-wrapper">
				<h1 class="note-display-title"><?php echo $sttc_nfo->static_pages_main_title;?></h1>
				<br><br>
			</div>
			<?php if ($sttc_nfo->static_pages_main_image != NULL AND $sttc_nfo->static_pages_main_image_2 == NULL AND $sttc_nfo->static_pages_main_image_3 == NULL AND $sttc_nfo->static_pages_main_image_4 == NULL AND $sttc_nfo->static_pages_main_image_5 == NULL): ?>
				<div class="col-md-12 note-display-image-wrapper">
					<img src="<?php echo base_url() . 'assets/bucket/cover/' . $sttc_nfo->static_pages_main_image;?>" alt="" class="note-display-image">
				</div>
			<?php elseif ($sttc_nfo->static_pages_main_image != NULL OR $sttc_nfo->static_pages_main_image_2 != NULL OR $sttc_nfo->static_pages_main_image_3 != NULL OR $sttc_nfo->static_pages_main_image_4 != NULL OR $sttc_nfo->static_pages_main_image_5 != NULL): ?>
				<div id="static_page_carousel" class="carousel col-md-12 note-display-image-wrapper static_page_carousel slide" data-ride="carousel">
					<div class="carousel-inner">
						<?php if ($sttc_nfo->static_pages_main_image != NULL): ?>
							<div class="carousel-item active">
								<img class="d-block w-100" src="<?php echo base_url() . 'assets/bucket/cover/' . $sttc_nfo->static_pages_main_image; ?>" alt="<?php echo $sttc_nfo->static_pages_main_title;?>">
							</div>
						<?php endif ?>
						<?php if ($sttc_nfo->static_pages_main_image_2 != NULL): ?>
							<div class="carousel-item">
								<img class="d-block w-100" src="<?php echo base_url() . 'assets/bucket/cover/' . $sttc_nfo->static_pages_main_image_2; ?>" alt="<?php echo $sttc_nfo->static_pages_main_title;?>">
							</div>
						<?php endif ?>
						<?php if ($sttc_nfo->static_pages_main_image_3 != NULL): ?>
							<div class="carousel-item">
								<img class="d-block w-100" src="<?php echo base_url() . 'assets/bucket/cover/' . $sttc_nfo->static_pages_main_image_3; ?>" alt="<?php echo $sttc_nfo->static_pages_main_title;?>">
							</div>
						<?php endif ?>
						<?php if ($sttc_nfo->static_pages_main_image_4 != NULL): ?>
							<div class="carousel-item">
								<img class="d-block w-100" src="<?php echo base_url() . 'assets/bucket/cover/' . $sttc_nfo->static_pages_main_image_4; ?>" alt="<?php echo $sttc_nfo->static_pages_main_title;?>">
							</div>
						<?php endif ?>
						<?php if ($sttc_nfo->static_pages_main_image_5 != NULL): ?>
							<div class="carousel-item">
								<img class="d-block w-100" src="<?php echo base_url() . 'assets/bucket/cover/' . $sttc_nfo->static_pages_main_image_5; ?>" alt="<?php echo $sttc_nfo->static_pages_main_title;?>">
							</div>
						<?php endif ?>
					</div>
					<a class="carousel-control-prev" href="#static_page_carousel" role="button" data-slide="prev">
						<span class="carousel-control-prev-icon" aria-hidden="true"></span>
						<span class="sr-only">Previous</span>
					</a>
					<a class="carousel-control-next" href="#static_page_carousel" role="button" data-slide="next">
						<span class="carousel-control-next-icon" aria-hidden="true"></span>
						<span class="sr-only">Next</span>
					</a>
				</div>
			<?php endif ?>
			

			<div class="col-md-12 note-display-bodytext">
				<?php echo $sttc_nfo->static_pages_main_text;?>
			</div>

			<?php if ($static_page_slug = 'himno'): ?>
				<div class="col-md-12 note-display-bodytext">
					<audio controls class="audiplayer_module">
						<source src="<?php echo base_url();?>assets/audio/himno_burgos.mp3" type="audio/mpeg">
						Your browser does not support the audio element.
					</audio>
				</div>
			<?php endif ?>
			
			<?php if ($sttc_nfo->static_pages_map_lat != NULL AND $sttc_nfo->static_pages_map_lng != NULL AND $sttc_nfo->static_pages_map_zoom != NULL): ?>
				<div class="col-md-12">
					<div id="map" style="height: 450px;"></div>
				</div>
				<script>
					function init_map() {
						var lat = <?php echo $sttc_nfo->static_pages_map_lat;?>;
						var lng = <?php echo $sttc_nfo->static_pages_map_lng;?>;
						var zoom = <?php echo $sttc_nfo->static_pages_map_zoom;?>;
						var mymap = L.map('map').setView([lat, lng], zoom);
						L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
							maxZoom: 17,
							id: 'mapbox/streets-v11',
							tileSize: 512,
							zoomOffset: -1,
						}).addTo(mymap);
						mymap.removeControl(mymap.zoomControl);
						mymap.dragging.disable();
						L.marker([lat, lng]).addTo(mymap);
					}
					$( document ).ready(function() {
						init_map();
					});
			    </script>
			<?php endif ?>

			<?php $this->load->view('pages/modules/social/socialmedia-share');?>

		</div>
	</div>
	
    
<?php endforeach ?>

