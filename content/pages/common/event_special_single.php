<?php foreach ($event_information->result() as $vnt_nfo): ?>
	<div class="col-md-12 note-display-inside">
		<div class="row note-display-wrapper">
			<div class="col-md-12 note-display-title-wrapper">
				<h3 class="note-display-title">Evento Especial</h3>
				<hr>
				<br>
			</div>
			<div class="col-md-12">
				<div class="row">
					<div class="col-md-2"></div>
					<div class="col-md-8 teams-wrap">
						<h1 class="note-display-title" style="text-align: center;"><?php echo $vnt_nfo->event_title;?></h1>
						<h4 style="text-align: center;">
							<?php echo convert_date($vnt_nfo->event_date);?> a las <?php echo $vnt_nfo->event_time;?>
						</h4>
					</div>
					<div class="col-md-2"></div>
				</div>
			</div>
			<div class="col-md-12 note-display-bodytext">
				<?php echo $vnt_nfo->event_text_body;?>
			</div>
			<?php $this->load->view('pages/modules/social/socialmedia-share');?>
		</div>
	</div>
<?php endforeach ?>