<div class="col-md-12 note-display-inside">
	<div class="row note-display-wrapper">
		<div class="col-md-12 note-display-category-wrapper">
			<h3 class="note-display-category">Patrocinadores</h3>
		</div>
		<div class="col-md-12 note-display-title-wrapper"></div>
		
		<?php foreach ($partners_info->result() as $prtnr): ?>
			<?php if ($prtnr->partners_level == 1): ?>
				<div class="col-md-12 partner-display-support">
			<?php elseif ($prtnr->partners_level == 2): ?>
				<div class="col-md-6">
			<?php elseif ($prtnr->partners_level == 3): ?>
				<div class="col-md-4">
			<?php elseif ($prtnr->partners_level == 4): ?>
				<div class="col-md-3">
			<?php endif ?>
					<a href="<?php echo $prtnr->partners_link;?>" class="partner-display-wrapper" target="_blank">
						<img class="img-fluid partner-display-img" src="<?php echo base_url() . 'assets/bucket/partners/' . $prtnr->partners_logo;?>" alt="<?php echo $prtnr->partners_name;?>">
					</a>
				</div>
		<?php endforeach ?>
	</div>
</div>