<div class="col-md-12 note-display-inside">
	<div class="row note-display-wrapper">
		<div class="col-md-12 note-display-category-wrapper">
			<h3 class="note-display-category">Galería Multimedia</h3>
		</div>
		<div class="col-md-12 note-display-title-wrapper">
			<h1 class="note-display-title">Colecciones Fotográficas</h1>
			<br><br>
		</div>
		<div class="col-md-12 note-display-bodytext">
			<div class="row">
				<?php foreach ($common_galleries->result() as $gls): ?>
					<div class="col-md-4">
						<div class="card" style="width: 18rem;">
							<img src="<?php echo $gls->thumbnail;?>" class="card-img-top" alt="<?php echo $gls->title;?>">
							<div class="card-body">
								<h5 class="card-title"><?php echo $gls->title;?></h5>
								<p class="card-text"><?php echo $gls->description;?></p>
								<a href="<?php echo base_url() . 'galeria/' . $gls->ID;?>" class="btn readmore-button">
									Ver Album
								</a>
							</div>
						</div>
					</div>
				<?php endforeach ?>
			</div>
		</div>
		<div class="col-md-12 note-display-title-wrapper" style="margin-top: 85px;">
			<h1 class="note-display-title">Colecciones Audiovisuales</h1>
			<br><br>
		</div>
		<div class="col-md-12 note-display-bodytext">
			<div class="row">
				<?php foreach ($mediavideo_galleries->result() as $mdvds_gl): ?>
					<div class="col-md-4">
						<div class="card" style="width: 18rem;">
							<div class="card-body">
								<img src="<?php echo base_url();?>assets/img/assets/video_icon.png" class="card-img-top" alt="<?php echo $mdvds_gl->name;?>">
								
								<h5 class="card-title"><?php echo $mdvds_gl->name;?></h5>
								<p class="card-text"><?php echo $mdvds_gl->description;?></p>
								<a href="<?php echo base_url() . 'galeria/video/' . $mdvds_gl->slug;?>" class="btn readmore-button">
									Ver Video Album
								</a>
							</div>
						</div>
					</div>
				<?php endforeach ?>
			</div>
		</div>
		<?php $this->load->view('pages/modules/social/socialmedia-share');?>
	</div>
</div>

