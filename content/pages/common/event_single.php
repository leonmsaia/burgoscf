<?php foreach ($event_information->result() as $vnt_nfo): ?>
	<?php
		$team_one = get_square_team_data($vnt_nfo->event_team_one);
		$team_two = get_square_team_data($vnt_nfo->event_team_two);
	?>
	<div class="col-md-12 note-display-inside">
		<div class="row note-display-wrapper">
			<div class="col-md-12 note-display-title-wrapper">
				<h1 class="note-display-title"><?php echo $vnt_nfo->event_title;?></h1>
				<hr>
				<br>
			</div>
			<div class="col-md-12">
				<div class="row">
					<div class="col-md-2"></div>
					<div class="col-md-8 teams-wrap">
						<div class="match-container">
							<img class="team-img" src="<?php echo base_url() . 'assets/bucket/team/' . $team_one;?>" alt="">
							<span class="vs-wrap">VS</span>
							<img class="team-img" src="<?php echo base_url() . 'assets/bucket/team/' . $team_two;?>" alt="">
						</div>
						<h2>
							<?php echo convert_date($vnt_nfo->event_date);?> a las <?php echo $vnt_nfo->event_time;?>
							<br>		
							En <?php echo $vnt_nfo->event_stadium;?>
						</h2>
					</div>
					<div class="col-md-2"></div>
				</div>
			</div>
			<div class="col-md-12 note-display-bodytext">
				<?php echo $vnt_nfo->event_text_body;?>
			</div>
			<?php if ($vnt_nfo->event_buy_link != NULL): ?>
			<div class="col-md-12">
				<div class="row">
					<div class="col-md-3"></div>
					<div class="col-md-6">
						<a class="readmore-button buy-tickets" href="<?php echo $vnt_nfo->event_buy_link;?>">
							Comprar Tickets para Este Evento
						</a>	
					</div>
					<div class="col-md-3"></div>
				</div>
			</div>
			<?php endif ?>
			<?php $this->load->view('pages/modules/social/socialmedia-share');?>
		</div>
	</div>
<?php endforeach ?>