<?php $img_path_construct = $this->config->item('blog_url') . 'wp-content/';?>
<?php
	foreach ($get_current_album_data->result() as $crrnt_dt) {
		$album_title = $crrnt_dt->title;
		$album_desc = $crrnt_dt->description;
	}
?>
<div class="col-md-12 note-display-inside">
	<div class="row note-display-wrapper">
		<div class="col-md-12 note-display-category-wrapper">
			<h3 class="note-display-category">Colecciones Fotográficas</h3>
		</div>
		<div class="col-md-12 note-display-title-wrapper">
			<h1 class="note-display-title"><?php echo $album_title;?></h1>
			<h3 class="subsection-title">Presentación</h3>
			<p><?php echo $album_desc;?></p>
			<br><br>
		</div>
		<div class="col-md-12 note-display-bodytext">
			<div id="gallerie_carousel" class="carousel slide" data-ride="carousel">
				<div class="carousel-inner">
					<?php 
						$flag = 0;
					?>
					<?php foreach ($album_photos->result() as $pic): ?>
						<?php
							if ($flag == 0) {
								$marker = 'active';
							}else{
								$marker = '';
							}
						?>
						<div class="carousel-item <?php echo $marker;?>">
							<img src="<?php echo  $img_path_construct . $pic->thumbnail;?>" class="card-img-top" alt="<?php echo $pic->title;?>">
						</div>
						<?php $flag++; ?>
					<?php endforeach ?>
				</div>
				<a class="carousel-control-prev" href="#gallerie_carousel" role="button" data-slide="prev">
					<span class="carousel-control-prev-icon" aria-hidden="true"></span>
					<span class="sr-only">Previous</span>
				</a>
				<a class="carousel-control-next" href="#gallerie_carousel" role="button" data-slide="next">
					<span class="carousel-control-next-icon" aria-hidden="true"></span>
					<span class="sr-only">Next</span>
				</a>
			</div>
		</div>
		<div class="col-md-12 note-display-bodytext">
			<div class="row">
				<div class="col-md-12 note-display-title-wrapper">
					<h3 class="subsection-title">Fotografías</h3>
				</div>
				<?php foreach ($album_photos->result() as $pic): ?>
					<div class="col-md-4" data-toggle="modal" data-target="#img_id_<?php echo $pic->ID;?>">
						<div class="card" style="width: 18rem;">
							<img src="<?php echo  $img_path_construct . $pic->thumbnail;?>" class="card-img-top" alt="<?php echo $pic->title;?>">
							<div class="card-body">
								<h5 class="card-title"><?php echo $pic->title;?></h5>
								<p class="card-text"><?php echo $pic->description;?></p>
							</div>
						</div>
					</div>
					<div class="modal fade" id="img_id_<?php echo $pic->ID;?>" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
						<div class="modal-dialog modal-xl">
							<div class="modal-content">
								<div class="modal-header">
									<h5 class="modal-title" id="staticBackdropLabel">
										<?php echo $pic->title;?>		
									</h5>
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
								</div>
								<div class="modal-body">
								<img src="<?php echo  $img_path_construct . $pic->file;?>" alt="<?php echo $pic->title;?>" class="img-fluid">
								<p><?php echo $pic->description;?></p>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-secondary" data-dismiss="modal">
										Cerrar
									</button>
								</div>
							</div>
						</div>
					</div>
				<?php endforeach ?>
			</div>
		</div>
		<div class="col-md-12 note-display-title-wrapper">
			<a class="readmore-button" href="<?php echo base_url();?>galeria/">Volver al Listado de Albumes</a>
		</div>
		<?php $this->load->view('pages/modules/social/socialmedia-share');?>
	</div>
</div>