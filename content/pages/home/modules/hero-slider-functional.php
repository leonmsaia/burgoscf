<div id="BlanquiHeroSlide" class="container-fluid carousel slide hero-image-container" data-ride="carousel" data-interval="6000">

	<div class="row hero-slide-container carousel-inner" role="listbox">
		<?php
			$counter = 0;
		?>
		<?php foreach ($get_hero_post->result() as $pst_hr): ?>
			<?php
				$link_builder = base_url() . 'noticias/' . $pst_hr->slug . '/' . $pst_hr->post_name;
				if ($counter == 0) {
					$flag = 'active';
				}else{
					$flag = '';
				}
			?>
			<div class="col-md-12 hero-main-note-container carousel-item <?php echo $flag;?>" style="background-image: url('<?php echo get_post_cover_image_featured($pst_hr->ID);?>'); ">
				<div class="container">
					<div class="col-md-8">
						<div class="row">
							<div class="col-md-10 hero-singular-note-container">
								<h4 class="hero-slide-category">
									<?php echo $pst_hr->name;?> - <?php echo convert_date($pst_hr->post_date);?>
								</h4>
								<h1 class="hero-slide-title">
									<?php echo $pst_hr->post_title;?>
								</h1>
								<a href="<?php echo $link_builder;?>" class="readmore-button">Leer</a>
							</div>
							<div class="col-md-2"></div>
						</div>
					</div>
				</div>
			</div>
			<?php $counter++; ?>
		<?php endforeach ?>

	</div>
	
	<a class="carousel-control-prev" href="#BlanquiHeroSlide" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only"></span>
    </a>
    <a class="carousel-control-next" href="#BlanquiHeroSlide" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only"></span>
    </a>

    <ol class="carousel-indicators hero-sidenotes-container container">
		<?php
			$counter_indicator = 0;
		?>
		<?php foreach ($get_hero_post->result() as $pst_ndctr): ?>
			<?php
				if ($counter_indicator == 0) {
					$flag = 'active';
				}else{
					$flag = '';
				}
			?>
			<li data-target="#BlanquiHeroSlide" data-slide-to="<?php echo $counter_indicator;?>" class="hero-note-sidenote-singular <?php echo $flag;?>">
				<h3><?php echo $pst_ndctr->post_title;?></h3>
			</li>
			<?php $counter_indicator++; ?>
		<?php endforeach ?>
	</ol>

</div>