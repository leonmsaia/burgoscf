<div class="row">
	<div class="col-md-12 hero-slide-container">
		<div class="row hero-image-container">
			<div class="col-md-12 background-container">
				<span class="black-shade-container"></span>
				<img src="<?php echo base_url();?>assets/img/pics/Burgos-Salamanca (124).jpg" alt="">
			</div>
		</div>
		<div class="row hero-main-note-container">
			<div class="col-md-8">
				<div class="row">
					<div class="col-md-10 hero-singular-note-container">
						<h4 class="hero-slide-category">Burgos CF</h4>
						<h1 class="hero-slide-title">El Burgos Club de Futbol presenta un Erte</h1>
						<a href="#" class="readmore-button">Leer</a>
					</div>
					<div class="col-md-2"></div>
				</div>
			</div>
			<div class="col-md-4">
				<ul class="hero-sidenotes-container">
					<li class="hero-note-sidenote-singular">
						<h3>El Burgos Club de Fútbol presenta un erte</h3>
						<span class="date">27 de Marzo 2020</span>
					</li>
					<li class="hero-note-sidenote-singular">
						<h3>El Burgos Club de Fútbol presenta un erte</h3>
						<span class="date">27 de Marzo 2020</span>
					</li>
					<li class="hero-note-sidenote-singular">
						<h3>El Burgos Club de Fútbol presenta un erte</h3>
						<span class="date">27 de Marzo 2020</span>
					</li>
					<li class="hero-note-sidenote-singular">
						<h3>El Burgos Club de Fútbol presenta un erte</h3>
						<span class="date">27 de Marzo 2020</span>
					</li>
				</ul>
			</div>
		</div>
	</div>		
</div>