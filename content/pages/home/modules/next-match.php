<?php
	// Count Events
	$event_number = count($netxt_match->result());
?>

<?php if ($event_number >= 1): ?>
	<div class="col-lg-12 next-match-boxes-container">
		<div class="row">
			<div class="col-lg-4 col-md-6 col-sm-12 next-match-module">
				<div class="row next-match-module-wrapper">
					<div class="col-lg-12">
						<div class="row">
							<div class="col-lg-12">
								<h3 class="module-title">Próximo Partido</h3>
							</div>
						</div>
						<?php
							// Limit for Event Render | Next Event
							$flag_limit = 1;
							$counter_limit = 0;
						?>
						<?php foreach ($netxt_match->result() as $nxt_mtch): ?>
							<?php if ($flag_limit != $counter_limit): ?>
								<?php
									$current_event_id = $nxt_mtch->event_id;
									$team_one = get_square_team_data($nxt_mtch->event_team_one);
									$team_two = get_square_team_data($nxt_mtch->event_team_two);
								?>
								<a class="row match_link" href="<?php echo base_url() . 'eventos/' . $nxt_mtch->event_detail_link ?>">
									<div class="col-lg-12">
										<div class="row">
											<div class="col-lg-12">
												<span class="event_title">
													<?php // echo $nxt_mtch->event_title;?>
												</span>
											</div>
											<div class="col-lg-4 col-sm-5 col-5">
												<img src="<?php echo base_url() . 'assets/bucket/team/' . $team_one;?>" alt="" class="team-square team-1">
											</div>
											<div class="col-lg-4 col-sm-2 col-2">
												<span class="match-vs">VS</span>
											</div>
											<div class="col-lg-4 col-sm-5 col-5">
												<img src="<?php echo base_url() . 'assets/bucket/team/' . $team_two;?>" alt="" class="team-square team-2">								
											</div>
											<div class="col-lg-12 col-12">
												<span class="match-date">
													<?php echo convert_date($nxt_mtch->event_date);?> a las <?php echo $nxt_mtch->event_time;?>
												</span>
											</div>
										</div>
									</div>
								</a>
								<?php $counter_limit++; ?>
							<?php endif ?>
						<?php endforeach ?>
					</div>
				</div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-12 next-match-module">
				<div class="next-match-module-wrapper">
					<div class="row">
						<div class="col-lg-12">
							<h3 class="module-title">Proximos Encuentros</h3>
						</div>
					</div>
					<div class="row">
						<?php
							// Limit for Event Render | Next 2 Events
							$flag_limit = 2;
							$counter_limit = 0;
						?>
						<?php foreach ($netxt_match->result() as $nxt_tw_mtch): ?>
							<?php if ($nxt_tw_mtch->event_id != $current_event_id): ?>
								<?php if ($flag_limit != $counter_limit): ?>
									<?php
										$team_one = get_square_team_data($nxt_tw_mtch->event_team_one);
										$team_two = get_square_team_data($nxt_tw_mtch->event_team_two);
									?>
									<div class="col-lg-6">
										<a class="row match_link" href="<?php echo base_url() . 'eventos/' . $nxt_tw_mtch->event_detail_link ?>">
											<div class="col-lg-12">
												<span class="event_title">
													<?php // echo $nxt_tw_mtch->event_title;?>
												</span>
											</div>
											<div class="col-lg-5 col-sm-5 col-5">
												<img src="<?php echo base_url() . 'assets/bucket/team/' . $team_one;?>" alt="" class="team-square team-1">
											</div>
											<div class="col-lg-2 col-sm-2 col-2">
												<span class="match-vs">VS</span>
											</div>
											<div class="col-lg-5 col-sm-5 col-5">
												<img src="<?php echo base_url() . 'assets/bucket/team/' . $team_two;?>" alt="" class="team-square team-2">		
											</div>
											<div class="col-lg-12 col-12">
												<span class="match-date">
													<?php echo convert_date($nxt_tw_mtch->event_date);?> a las <?php echo $nxt_tw_mtch->event_time;?>
												</span>
											</div>
										</a>
									</div>
									<?php $counter_limit++; ?>
								<?php endif ?>
							<?php endif ?>
						<?php endforeach ?>
					</div>
				</div>
			</div>
			<?php $this->load->view('pages/home/modules/club_stats');?>
		</div>
	</div>
<?php endif ?>