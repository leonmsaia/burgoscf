<div class="col-lg-12 note-display-inside">
	<div class="row note-display-wrapper">
		<?php foreach ($player_information->result() as $plyr_nfo): ?>
			<div class="col-lg-12 note-display-category-wrapper">
				<h3 class="note-display-category">
					<?php
						$team_division_id = $plyr_nfo->team_player_team_division;
						$player_position_id = $plyr_nfo->team_player_position;
					?>
					<?php $team_division_array = get_team_division_data_by_team_division_id($team_division_id);?>
					<?php foreach ($team_division_array->result() as $tmdvsn): ?>
						<a href="<?php echo base_url() . 'primer_equipo';?>" style="color:#1a8f26;">
							<?php echo $tmdvsn->team_division_name;?>
						</a>
					<?php endforeach ?>
					 - 
					<?php $play_position_array = get_player_position_by_player_position_id($player_position_id);?>
					<?php foreach ($play_position_array->result() as $plyrpstn): ?>
						<?php 
							$position_slug = $plyrpstn->team_player_position_slug;
							$position_name = $plyrpstn->team_player_position_name;
							echo $position_name;
						?>
					<?php endforeach ?>
				</h3>
			</div>
			<div class="col-lg-12 note-display-title-wrapper">
				<h1 class="note-display-title">
					<?php echo $plyr_nfo->team_player_name;?> 
					<?php echo $plyr_nfo->team_player_middle;?> 
					<?php echo $plyr_nfo->team_player_lastname;?>
				</h1>
				<br><br>
			</div>
			<div class="col-lg-4 note-display-image-wrapper">
				<img src="<?php echo base_url() . 'assets/bucket/players/' . $plyr_nfo->team_player_picture;?>" alt="" class="note-display-image">
			</div>
			<div class="col-lg-4 note-display-image-wrapper">
				<?php if ($plyr_nfo->team_player_phrase != NULL): ?>
					<blockquote class="blockquote text-center player-phrase-wrapper">
					  <p class="mb-0"><?php echo $plyr_nfo->team_player_phrase;?></p>
					  <footer class="blockquote-footer"><?php echo $plyr_nfo->team_player_name;?></footer>
					</blockquote>
				<?php endif ?>
			</div>
			<?php if ($plyr_nfo->team_player_position != 5): ?>
				<div class="col-lg-4 profile-display-field-wrapper">
					<img src="<?php echo base_url();?>assets/img/assets/soccer-field.svg" alt="" class="profile-display-field-image">
					<span class="profile-display-field-player soccer-player-position soccer-player-position-<?php echo $plyr_nfo->team_player_position;?>">
						<i class="fas fa-male"></i>
					</span>
				</div>
			<?php endif ?>
			
			<?php if ($plyr_nfo->team_player_position == 5): ?>
				<div class="col-lg-4">
					<div class="row">
						<div class="col-lg-12 note-display-image-wrapper profile-display-stats-title">
							<h3 class="profile-display-stats-title-txt">Ficha del Técnico</h3>
						</div>
					</div>
					<table class="table profile-display-tables-container col-lg-12">
						<tbody>
							<tr>
								<td>Nombre</td>
								<td>
									<?php echo $plyr_nfo->team_player_name;?> 
									<?php echo $plyr_nfo->team_player_middle;?> 
									<?php echo $plyr_nfo->team_player_lastname;?>
								</td>
							</tr>
							<tr>
								<td>Posición</td>
								<td><?php echo $position_name;?></td>
							</tr>
							<?php if ($plyr_nfo->team_player_function != NULL): ?>
								<tr>
									<td>Función</td>
									<td><?php echo $plyr_nfo->team_player_function;?></td>
								</tr>
							<?php endif ?>
							<tr>
								<td>Fecha de Nac.</td>
								<td><?php echo convert_date($plyr_nfo->team_player_birth_date);?></td>
							</tr>
							<tr>
								<td>Lugar de Nac.</td>
								<td><?php echo $plyr_nfo->team_player_birth_place;?></td>
							</tr>
							<tr>
								<td>Temp. en el club</td>
								<td><?php echo $plyr_nfo->team_player_season_in_club;?></td>
							</tr>
							<tr>
								<td>Club de Procedencia</td>
								<td><?php echo $plyr_nfo->team_player_original_club;?></td>
							</tr>
						</tbody>
					</table>
				</div>
			<?php endif ?>

			<?php if ($plyr_nfo->team_player_position != 5): ?>
				<div class="col-lg-12 profile-display-tables-wrapper">
					<div class="row">
						<div class="col-lg-12 profile-display-stats-title">
							<h3 class="profile-display-stats-title-txt">Ficha del Jugador</h3>
						</div>
						<div class="col-lg-6">
							<table class="table profile-display-tables-container">
								<tbody>
									<tr>
										<td>Nombre</td>
										<td>
											<?php echo $plyr_nfo->team_player_name;?> 
											<?php echo $plyr_nfo->team_player_middle;?> 
											<?php echo $plyr_nfo->team_player_lastname;?>
										</td>
									</tr>
									<tr>
										<td>Posición</td>
										<td><?php echo $position_name;?></td>
									</tr>
									<?php if ($plyr_nfo->team_player_function != NULL): ?>
										<tr>
											<td>Función</td>
											<td><?php echo $plyr_nfo->team_player_function;?></td>
										</tr>
									<?php endif ?>
									<tr>
										<td>Fecha de Nac.</td>
										<td><?php echo convert_date($plyr_nfo->team_player_birth_date);?></td>
									</tr>
									<tr>
										<td>Lugar de Nac.</td>
										<td><?php echo $plyr_nfo->team_player_birth_place;?></td>
									</tr>
									<tr>
										<td>Temp. en el club</td>
										<td><?php echo $plyr_nfo->team_player_season_in_club;?></td>
									</tr>
									<tr>
										<td>Club de Procedencia</td>
										<td><?php echo $plyr_nfo->team_player_original_club;?></td>
									</tr>
									<tr>
										<td>Dorsal</td>
										<td><?php echo $plyr_nfo->team_player_shirt_number;?></td>
									</tr>
								</tbody>
							</table>
						</div>
						<div class="col-lg-6">
							<table class="table profile-display-tables-container">
								<tbody>
									<?php if ($plyr_nfo->team_player_position == 1): ?>
										<tr>
											<td><i class="fas fa-futbol"></i> Goles recibidos</td>
											<td><?php echo $plyr_nfo->team_player_goals_out;?></td>
										</tr>
									<?php else: ?>
										<tr>
											<td><i class="fas fa-futbol"></i> Goles convertidos</td>
											<td><?php echo $plyr_nfo->team_player_goals_in;?></td>
										</tr>
									<?php endif ?>
									<tr>
										<td><i class="fas fa-futbol"></i> Partidos Jugados</td>
										<td><?php echo $plyr_nfo->team_player_matchs_play;?></td>
									</tr>
									<tr>
										<td><i class="fas fa-futbol"></i> Partidos Titular</td>
										<td><?php echo $plyr_nfo->team_player_matchs_in_team;?></td>
									</tr>
									<tr>
										<td><i class="fas fa-clock"></i> Minutos Jugados</td>
										<td><?php echo $plyr_nfo->team_player_minutes_play;?></td>
									</tr>
									<tr>
										<td><i class="fas fa-square"></i> Tarjetas Amarillas</td>
										<td><?php echo $plyr_nfo->team_player_card_yellow;?></td>
									</tr>
									<tr>
										<td><i class="fas fa-square"></i> Tarjetas Rojas</td>
										<td><?php echo $plyr_nfo->team_player_card_red;?></td>
									</tr>
								</tbody>
							</table>
						</div>
						<div class="col-lg-12">
							<a href="<?php echo base_url() . 'primer_equipo/' . $position_slug;?>" class="readmore-button">Volver</a>
						</div>
					</div>
				</div>
			<?php endif ?>
		<?php endforeach ?>
		<?php $this->load->view('pages/modules/social/socialmedia-share');?>
	</div>
</div>