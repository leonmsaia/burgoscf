<div class="col-md-12 note-display-inside">
	<div class="row note-display-wrapper">
		<?php 
			$post_cat_slug = $current_post_category->result()[0]->slug;
		?>
		<?php foreach ($current_post->result() as $pst_vls): ?>
			<div class="col-md-12 note-display-category-wrapper">
				<h3 class="note-display-category">
					<a class="single-note-slug-featured" href="<?php echo base_url() . 'noticias/' . $post_cat_slug;?>">
						<?php echo $pst_vls->name;?>
					</a>
					 - 
					<?php echo convert_date($pst_vls->post_date);?>
				</h3>
				<h5>Por <?php echo get_post_author_name_by_id($pst_vls->post_author);?></h5>
			</div>
			<div class="col-md-12 note-display-title-wrapper">
				<h1 class="note-display-title"><?php echo $pst_vls->post_title;?></h1>
				<br><br>
			</div>
			<div class="col-md-12 note-display-image-wrapper">
				<img src="<?php echo get_post_cover_image_featured($pst_vls->ID);?>" alt="" class="note-display-image">
			</div>
			<div class="col-md-12 note-display-bodytext">
				<?php //echo $pst_vls->post_content;?>
			</div>
			<?php //print_r(getVideoUrlsFromString($pst_vls->post_content));?>
			<?php echo format_matcher($pst_vls->post_content);?>
		<?php endforeach ?>
		<?php $this->load->view('pages/modules/social/socialmedia-share');?>
	</div>
</div>




<!-- $html = '
    https://www.youtube-nocookie.com/embed/VWrlXsmcL2E
    http://www.youtube.com/v/NLqAF9hrVbY?fs=1&hl=en_US
';
$html = getVideoUrlsFromString($html);
print_r($html); -->