<div class="col-md-12 note-display-inside">
	<div class="row note-display-wrapper">
		<?php 
			$post_cat_slug = $current_post_category->result()[0]->slug;
		?>
		<?php foreach ($current_post->result() as $pst_vls): ?>
			<?php $video_code = get_post_mediavideo($pst_vls->ID);?>
			<div class="col-md-12 note-display-category-wrapper">
				<h3 class="note-display-category">
					<a class="single-note-slug-featured" href="<?php echo base_url() . 'noticias/' . $post_cat_slug;?>">
						<?php echo $pst_vls->name;?>
					</a>
					 - 
					<?php echo convert_date($pst_vls->post_date);?>
				</h3>
				<h5>Por <?php echo get_post_author_name_by_id($pst_vls->post_author);?></h5>
			</div>
			<div class="col-md-12 note-display-title-wrapper">
				<h1 class="note-display-title"><?php echo $pst_vls->post_title;?></h1>
				<br><br>
			</div>
			<div class="col-md-12">
			  <div class="iframe-wrapper plyr__video-embed" id="player">
				  <iframe
				  	class="mediavideo_wrapper full_article"
				    src="https://www.youtube.com/embed/<?php echo $video_code;?>?origin=https://plyr.io&amp;iv_load_policy=3&amp;modestbranding=1&amp;playsinline=1&amp;showinfo=0&amp;rel=0&amp;enablejsapi=1"
				    allowfullscreen
				    allowtransparency
				    allow="autoplay"
				  ></iframe>
			  </div>
			</div>
			<div class="col-md-12 note-display-bodytext">
				<?php echo $pst_vls->post_content;?>
			</div>
		<?php endforeach ?>
		<?php $this->load->view('pages/modules/social/socialmedia-share');?>
	</div>
</div>
<script src='<?php echo base_url();?>assets/plyr/plyr.min.js'></script>
<link rel="stylesheet" href="<?php echo base_url();?>assets/plyr/plyr.css">