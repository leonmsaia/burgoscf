<div class="col-md-12 note-display-inside">
	<div class="row note-display-wrapper">
		<?php
			$category_data = $current_category->result()[0];
			$category_name = $category_data->name;
			$category_path = base_url() . 'noticias/' . $category_data->slug;
		?>
		<div class="col-md-12 note-display-last-news-module">
			<h3 class="note-display-last-news-text"><?php echo $category_name;?> - Ultimas Noticias</h3>
		</div>

		<?php foreach ($all_posts->result() as $ll_nts_n_ctgry): ?>
		<?php $link_builder = base_url() . 'noticias/' . $ll_nts_n_ctgry->slug . '/' . $ll_nts_n_ctgry->post_name;?>
		<div class="col-md-12">
			<div class="row note-display-wrapper">
				<div class="col-md-6 note-display-image-wrapper">
					<a class="single-note-slug-featured" href="<?php echo $link_builder;?>">
						<img src="<?php echo get_post_cover_image_featured($ll_nts_n_ctgry->ID);?>" alt="<?php echo $ll_nts_n_ctgry->name;?>" class="note-display-image">
					</a>
				</div>
				<div class="col-md-6">
					<div class="row">
						<div class="col-md-12 note-display-category-wrapper">
							<h3 class="note-display-category">
								<a class="single-note-slug-featured" href="<?php echo base_url() . 'noticias/' . $ll_nts_n_ctgry->slug;?>">
									<?php echo $ll_nts_n_ctgry->name;?>
								</a>
								 - 
								<?php echo convert_date($ll_nts_n_ctgry->post_date);?>
							</h3>
						</div>
						<div class="col-md-12 note-list-item-display-body-wrapper">
							<h1 class="note-display-title">
								<a class="single-note-slug-featured" href="<?php echo $link_builder;?>">
									<?php echo $ll_nts_n_ctgry->post_title;?>
								</a>
							</h1>
							<div class="note-display-content">
								<?php echo $ll_nts_n_ctgry->post_excerpt;?>		
							</div>
							<a href="<?php echo $link_builder;?>" class="readmore-button">Leer</a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php endforeach ?>
		<div class="col-md-12 note-display-view-all-notices">
			<a href="<?php echo base_url();?>noticias/" class="readmore-button">Ver Más Noticias</a>
		</div>
	</div>
</div>