<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
// Core Engine for Team Manager
// Created by LeonMSaia
// All Rights Reserved.
// 2020

function convert_date($date)
{
  $timestamp = strtotime($date);
  $new_date = date('d/m/Y', $timestamp);
  return $new_date;
}

function get_team_division_data_by_team_division_id($team_division_id)
{
  $ci =& get_instance();
  $ci->db->from('team_division');
  $ci->db->where('team_division_id', $team_division_id);
  $result = $ci->db->get();
  return $result;
}

function get_player_position_by_player_position_id($player_position_id)
{
  $ci =& get_instance();
  $ci->db->from('team_player_position');
  $ci->db->where('team_player_position_id', $player_position_id);
  $result = $ci->db->get();
  return $result;
}

function get_child_elements_for_navbar_elements($navbar_element_id)
{
  $ci =& get_instance();
  $ci->db->from('static_pages');
  $ci->db->where('static_pages_navbar_appear', 1);
  $ci->db->where('static_pages_navbar_parent', $navbar_element_id);
  $result = $ci->db->get();
  return $result;
}

function get_child_elements_direct_for_navbar_elements($parent_id)
{
  $ci =& get_instance();
  $ci->db->from('static_pages_navbar');
  $ci->db->where('static_pages_navbar_parent', $parent_id);
  $ci->db->where('static_pages_navbar_main_element', 0);
  $ci->db->where('static_pages_navbar_dropdown', 0);
  $result = $ci->db->get();
  return $result; 
}

function get_post_cover_image($post_id)
{
  $ci =& get_instance();
  $blogdb = $ci->load->database('blog_development', TRUE);
  $blogdb->limit(1);
  $blogdb->where('post_parent', $post_id);
  $blogdb->where('post_type', 'attachment');
  $blogdb->order_by('ID', 'DESC');
  $result = $blogdb->get('wp_posts');
  $image_path = $result->result()[0]->guid;
  return $image_path;
}

function get_post_cover_image_featured($post_id)
{
  $ci =& get_instance();
  $blogdb = $ci->load->database('blog_development', TRUE);
  $blogdb->limit(1);
  $blogdb->where('post_id', $post_id);
  $blogdb->where('meta_key', '_thumbnail_id');
  $blogdb->order_by('post_id', 'DESC');
  $blogdb->join('wp_posts', 'wp_posts.ID = wp_postmeta.meta_value');
  $result = $blogdb->get('wp_postmeta');
  $image_path = $result->result()[0]->guid;
  // Replace especifico para que funcione con el Alias wp-content:
  $image_path = str_replace("http://gestion8.burgoscf.es/wp-content","https://www.burgoscf.es/wp-content", $image_path);
  // Replace generico para que no se pida nada por http
  // Si algo falla sera cuestion de ver el motivo, pero evitar http SIEMPRE por ranking:
  $image_path = str_replace("http://", "https://", $image_path);
  return $image_path;
}

function get_post_mediavideo($post_id)
{
  $ci =& get_instance();
  $blogdb = $ci->load->database('blog_development', TRUE);
  $blogdb->limit(1);
  $blogdb->where('post_id', $post_id);
  $blogdb->where('meta_key', 'youtube_video_path');
  $blogdb->order_by('post_id', 'DESC');
  $result = $blogdb->get('wp_postmeta');
  $image_path = $result->result()[0]->meta_value;
  $regex = '/^((?:https?:)?\/\/)?((?:www|m)\.)?((?:youtube\.com|youtu.be))(\/(?:[\w\-]+\?v=|embed\/|v\/)?)([\w\-]+)(\S+)?$/m';
  preg_match_all($regex, $image_path, $youtube_video_code, PREG_SET_ORDER, 0);
  return $youtube_video_code[0][5];
}






function get_post_author_name_by_id($author_id)
{
  $ci =& get_instance();
  $blogdb = $ci->load->database('blog_development', TRUE);
  $blogdb->where('ID', $author_id);
  $result = $blogdb->get('wp_users');
  $author_name = $result->result()[0]->display_name;
  return $author_name;
}

function get_footer_socialmedia_icons()
{
  $ci =& get_instance();
  $ci->db->from('contact_information');
  $ci->db->where('contact_information_status', 1);
  $result = $ci->db->get();
  return $result;
}

function get_square_team_data($team_id)
{
  $ci =& get_instance();
  $ci->db->from('team');
  $ci->db->where('team_id', $team_id);
  $result = $ci->db->get();
  $logo_path = $result->result()[0]->team_logo;
  return $logo_path;
}

function format_matcher($string) {
    $prepare_youtube_match = preg_replace('/\s*[a-zA-Z\/\/:\.]*youtube.com\/watch\?v=([a-zA-Z0-9\-_]+)([a-zA-Z0-9\/\*\-\_\?\&\;\%\=\.]*)/i',
      "<iframe class=\"mediavideo_wrapper full_article\" width=\"420\" height=\"315\" src=\"//www.youtube.com/embed/$1\" frameborder=\"0\" allowfullscreen></iframe>"
      , $string);
    
    return $prepare_youtube_match;
}