<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if ($this->config->item('building_mode') == true) {
  $route['default_controller'] = 'Building';
}else{
  $route['default_controller'] = 'Home';
}

$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

// Team
$route['primer_equipo'] = 'Team/first_team';
$route['primer_equipo/(:any)'] = 'Team/filter_position/$1';
$route['primer_equipo/(:any)/(:any)'] = 'Team/player_view/$1/$2';
// Team End

// Legal
$route['legal/(:any)'] = 'Common/legal_page/$1';
// Legal End

// Pages
$route['prensa'] = 'Common/press';
$route['patrocinadores'] = 'Common/partners';
$route['institucional/(:any)'] = 'Common/static_page/$1';
$route['tabla_posiciones_completa'] = 'Common/complete_table';
// Pages End

// Blog
$route['noticias'] = 'Blog/main_blog';
$route['noticias/(:any)'] = 'Blog/categorie/$1';
$route['noticias/(:any)/(:any)'] = 'Blog/inner_notice/$1/$2';

$route['multimedia/(:any)/(:any)'] = 'Blog/inner_notice_mediavideo/$1/$2';
// Blog End

// Common
$route['el_club'] = 'Common/complete_the_club';
$route['cantera'] = 'Common/complete_the_pit';
// Common End

// Events
$route['calendario'] = 'Common/redirect_calendar';
$route['calendario/(:num)/(:num)'] = 'Common/complete_calendar/$1/$2';
$route['eventos/(:any)'] = 'Common/event_day/$1';
$route['eventos_especiales/(:any)'] = 'Common/event_special_day/$1';
// Events End

// Galleries
$route['galeria'] = 'Common/all_galleries';
$route['galeria/(:num)'] = 'Common/get_album_by_id/$1';
$route['galeria/video/(:any)'] = 'Common/get_video_album_by_cat/$1';