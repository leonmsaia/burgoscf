<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$url = 'http://' . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
if (strpos($url, 'localhost') !== false) {
	$appStatus['mode'] = 'development';
}else{
	$appStatus['mode'] = 'production';
}

if ($appStatus['mode'] == 'production') {
 	$config['base_url'] = 'http://burgos.leonmsaia.com/';
	$config['sess_driver'] = 'files';
 	$config['sess_save_path'] = NULL;
 	$config['blog_url'] = 'http://leonmsaia.com/clients/burgoscf_wp/';
}elseif ($appStatus['mode'] == 'development'){
 	$config['sess_driver'] = 'files';
 	$config['sess_save_path'] = NULL;
 	$config['blog_url'] = 'http://localhost/burgoscf_wp/';
 	$config['base_url'] = 'http://localhost/burgoscf';
}

$config['page_name'] = 'Burgos CF';
$config['page_author'] = 'Burgos CF';
$config['page_description'] = 'Web Oficial del Equipo Blanquinegro.';
$config['index_page'] = '';
$config['building_mode'] = FALSE;
$config['uri_protocol']	= 'AUTO';
$config['url_suffix'] = '';
$config['language']	= 'english';
$config['charset'] = 'UTF-8';
$config['enable_hooks'] = FALSE;
$config['subclass_prefix'] = 'MY_';
$config['composer_autoload'] = FALSE;
$config['permitted_uri_chars'] = 'a-z 0-9~%.:_\-';
$config['enable_query_strings'] = FALSE;
$config['controller_trigger'] = 'c';
$config['function_trigger'] = 'm';
$config['directory_trigger'] = 'd';
$config['allow_get_array'] = TRUE;
$config['log_threshold'] = 0;
$config['log_path'] = '';
$config['log_file_extension'] = '';
$config['log_file_permissions'] = 0644;
$config['log_date_format'] = 'Y-m-d H:i:s';
$config['error_views_path'] = '';
$config['cache_path'] = '';
$config['cache_query_string'] = FALSE;
$config['encryption_key'] = 'jghasdkhasdhgqwotyrefqowduaoskdjhqiwuyrqiwjdpalsjdiqyuwgriqwedñqalskdjqoiuwerhlqaskdjqikuwgerqowe';
$config['sess_expiration'] = 7200;
$config['sess_match_ip'] = FALSE;
$config['sess_time_to_update'] = 300;
$config['sess_cookie_name'] = 'ci_session';
$config['sess_regenerate_destroy'] = FALSE;
$config['cookie_prefix']	= '';
$config['cookie_domain']	= '';
$config['cookie_path']		= '/';
$config['cookie_secure']	= FALSE;
$config['cookie_httponly'] 	= FALSE;
$config['standardize_newlines'] = FALSE;
$config['global_xss_filtering'] = FALSE;
$config['csrf_protection'] = FALSE;
$config['csrf_token_name'] = 'csrf_test_name';
$config['csrf_cookie_name'] = 'csrf_cookie_name';
$config['csrf_expire'] = 7200;
$config['csrf_regenerate'] = TRUE;
$config['csrf_exclude_uris'] = array();
$config['compress_output'] = FALSE;
$config['time_reference'] = 'local';
$config['rewrite_short_tags'] = FALSE;
$config['proxy_ips'] = '';
