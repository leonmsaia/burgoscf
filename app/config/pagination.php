<?php
	$config['full_tag_open'] = '<ul class="list-inline">';
	$config['full_tag_close'] = '</ul>';
	$config['num_tag_open'] = '<li>';
	$config['num_tag_close'] = '</li>';
	$config['cur_tag_open'] = '<li><a href="#" class="active">';
	$config['cur_tag_close'] = '</a></li>';
	$config['next_tag_open'] = '<li>';
	$config['next_tag_close'] = '</li>';
	$config['next_link'] = '<i class="fa fa-caret-right" aria-hidden="true"></i>';
	$config['prev_tag_open'] = '<li>';
	$config['prev_tag_close'] = '</li>';
	$config['prev_link'] = '<i class="fa fa-caret-left" aria-hidden="true"></i>';
	$config['first_tag_open'] = '<li>';
	$config['first_tag_close'] = '</li>';
	$config['first_link'] = 'Primeros';
	$config['last_tag_open'] = '<li>';
	$config['last_tag_close'] = '</li>';
	$config['last_link'] = 'Ultimos';
?>