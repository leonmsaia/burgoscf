<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct(){
		parent::__construct();

	}

	public function index() {

		// Template Information
		$data['layout'] = 'home';
		$data['page'] = 'home/home';

		// Site Information
		$this->load->model('Site_model');
		$data['site_info'] = $this->Site_model->getSiteInformation();
		$data['site_contact'] = $this->Site_model->getSiteContactInformation();
		$site_info = $data['site_info']->result()[0];

		// Navbar Render
		$data['navbar_render'] = $this->Site_model->getPagesNavbarElements();

		// Page Information
		$data['title'] = $site_info->site_name . ' | Inicio';
		$data['description'] = $site_info->site_desc;
		$data['keywords'] = $site_info->site_keywords;
		$data['author'] = $site_info->site_author;
		$data['lang'] = $site_info->site_lang;
		$data['charset'] = $site_info->site_charset;

		// Data for Rendering
		$this->load->model('Common_model');
		$data['team_all'] = $this->Common_model->get_teams_information();
		$data['horizontal_ads_three_columns'] = $this->Common_model->get_horizontal_ads_three_columns();
		$data['horizontal_ads'] = $this->Common_model->get_horizontal_ads();
		$data['vertical_ads'] = $this->Common_model->get_vertical_ads();
		$data['get_club_stats'] = $this->Common_model->get_club_stats();
		$this->load->model('Blog_model');
		$data['all_posts'] = $this->Blog_model->get_all_posts();
		$data['all_mediavideos'] = $this->Blog_model->get_all_mediavideos();
		$limit = 4;
		$data['get_hero_post'] = $this->Blog_model->get_post_for_hero($limit);
		$data['blog_home_config'] = $this->Blog_model->get_home_post_configuration()->result()[0];
		$this->load->model('Event_model');
		$data['netxt_match'] = $this->Event_model->get_next_match();
		// $data['netxt_two_match'] = $this->Event_model->get_two_next_match();
		$data['partners_info'] = $this->Common_model->get_partners_all();
		
		$this->load->view('layout/' . $data['layout'], $data);
	}

}
