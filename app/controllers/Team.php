<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Team extends CI_Controller {

	public function __construct(){
		parent::__construct();

	}

	public function first_team() {

		// Template Information
		$data['layout'] = 'basic';
		$data['page'] = 'teams/first_team';

		// Site Information
		$this->load->model('Site_model');
		$data['site_info'] = $this->Site_model->getSiteInformation();
		$data['site_contact'] = $this->Site_model->getSiteContactInformation();
		$site_info = $data['site_info']->result()[0];

		// Navbar Render
		$data['navbar_render'] = $this->Site_model->getPagesNavbarElements();

		// Page Information
		$data['title'] = $site_info->site_name . ' | Inicio';
		$data['description'] = $site_info->site_desc;
		$data['keywords'] = $site_info->site_keywords;
		$data['author'] = $site_info->site_author;
		$data['lang'] = $site_info->site_lang;
		$data['charset'] = $site_info->site_charset;

		// Data for Rendering
		$this->load->model('Team_model');
		$data['positions_all'] = $this->Team_model->getAllPositions();
		


		$this->load->view('layout/' . $data['layout'], $data);
	}

	public function filter_position($position_slug) {

		// Data for Pre-Rendering
		$this->load->model('Team_model');
		$data['position_data'] = $this->Team_model->get_position_by_slug_position($position_slug);
		
		// Template Information
		$data['layout'] = 'basic';
		$data['page'] = 'teams/list_players';

		// Site Information
		$this->load->model('Site_model');
		$data['site_info'] = $this->Site_model->getSiteInformation();
		$data['site_contact'] = $this->Site_model->getSiteContactInformation();
		$site_info = $data['site_info']->result()[0];

		// Navbar Render
		$data['navbar_render'] = $this->Site_model->getPagesNavbarElements();

		// Page Information
		$data['title'] = $site_info->site_name . ' | Inicio';
		$data['description'] = $site_info->site_desc;
		$data['keywords'] = $site_info->site_keywords;
		$data['author'] = $site_info->site_author;
		$data['lang'] = $site_info->site_lang;
		$data['charset'] = $site_info->site_charset;

		// Data for Rendering
		$data['position_slug_txt'] = $position_slug;
		$target_position_id = $data['position_data']->result()[0]->team_player_position_id;
		$data['all_players_in_position'] = $this->Team_model->get_player_listed_in_position_by_position_id($target_position_id);

		$this->load->view('layout/' . $data['layout'], $data);
	}

	public function player_view($position_slug, $player_slug)
	{

		// Data for Pre-Rendering
		$this->load->model('Team_model');
		$data['position_data'] = $this->Team_model->get_position_by_slug_position($position_slug);
		
		// Template Information
		$data['layout'] = 'basic';
		$data['page'] = 'player/player';

		// Site Information
		$this->load->model('Site_model');
		$data['site_info'] = $this->Site_model->getSiteInformation();
		$data['site_contact'] = $this->Site_model->getSiteContactInformation();
		$site_info = $data['site_info']->result()[0];

		// Navbar Render
		$data['navbar_render'] = $this->Site_model->getPagesNavbarElements();

		// Page Information
		$data['title'] = $site_info->site_name . ' | Inicio';
		$data['description'] = $site_info->site_desc;
		$data['keywords'] = $site_info->site_keywords;
		$data['author'] = $site_info->site_author;
		$data['lang'] = $site_info->site_lang;
		$data['charset'] = $site_info->site_charset;

		// Data for Rendering
		$data['player_information'] = $this->Team_model->get_player_information_by_slug($player_slug);

		$this->load->view('layout/' . $data['layout'], $data);

	}

}
