<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Common extends CI_Controller {

	public function __construct(){
		parent::__construct();

	}

	public function press() {
		// Template Information
		$data['layout'] = 'basic';
		$data['page'] = 'common/press_form';

		// Site Information
		$this->load->model('Site_model');
		$data['site_info'] = $this->Site_model->getSiteInformation();
		$data['site_contact'] = $this->Site_model->getSiteContactInformation();
		$site_info = $data['site_info']->result()[0];

		// Navbar Render
		$data['navbar_render'] = $this->Site_model->getPagesNavbarElements();

		// Page Information
		$data['title'] = $site_info->site_name . ' | Inicio';
		$data['description'] = $site_info->site_desc;
		$data['keywords'] = $site_info->site_keywords;
		$data['author'] = $site_info->site_author;
		$data['lang'] = $site_info->site_lang;
		$data['charset'] = $site_info->site_charset;

		// Data for Rendering
		$this->load->model('Team_model');
		$data['positions_all'] = $this->Team_model->getAllPositions();
		
		$this->load->view('layout/' . $data['layout'], $data);
	}

	public function press_form_process() {
		$contact_name = $_POST['contact_form_name'];
		$contact_lastname = $_POST['contact_form_lastname'];
		$contact_dni = $_POST['contact_form_id'];
		$contact_mail = $_POST['contact_form_mail'];
		$contact_phone = $_POST['contact_form_phone'];
		$contact_company = $_POST['contact_form_company'];
		$contact_message = $_POST['contact_form_message'];
		$contact_date = date('y-m-d');
		$contact_data = array(
			'contact_message_first_name' => $contact_name,
			'contact_message_last_name' => $contact_lastname,
			'contact_message_email' => $contact_mail,
			'contact_message_phone' => $contact_phone,
			'contact_message_company' => $contact_company,
			'contact_message_text' => $contact_message,
			'contact_message_date' => $contact_date,
			'contact_message_status' => 0
		);
		$this->db->insert('contact_message', $contact_data);
	}

	public function static_page($static_page_slug) {
		
		// Template Information
		$data['layout'] = 'basic';
		$data['page'] = 'common/static_page';

		// Site Information
		$this->load->model('Site_model');
		$data['site_info'] = $this->Site_model->getSiteInformation();
		$data['site_contact'] = $this->Site_model->getSiteContactInformation();
		$site_info = $data['site_info']->result()[0];

		// Navbar Render
		$data['navbar_render'] = $this->Site_model->getPagesNavbarElements();

		// Page Information
		$data['title'] = $site_info->site_name . ' | Inicio';
		$data['description'] = $site_info->site_desc;
		$data['keywords'] = $site_info->site_keywords;
		$data['author'] = $site_info->site_author;
		$data['lang'] = $site_info->site_lang;
		$data['charset'] = $site_info->site_charset;

		// Data for Rendering
		$this->load->model('Common_model');
		$data['static_page_info'] = $this->Common_model->get_static_page_by_static_page_slug($static_page_slug);
		$data['static_page_slug'] = $static_page_slug;
		
		$this->load->view('layout/' . $data['layout'], $data);
	}

	public function legal_page($legal_page_slug) {
		// Template Information
		$data['layout'] = 'basic';
		$data['page'] = 'common/static_page';

		// Site Information
		$this->load->model('Site_model');
		$data['site_info'] = $this->Site_model->getSiteInformation();
		$data['site_contact'] = $this->Site_model->getSiteContactInformation();
		$site_info = $data['site_info']->result()[0];

		// Navbar Render
		$data['navbar_render'] = $this->Site_model->getPagesNavbarElements();

		// Page Information
		$data['title'] = $site_info->site_name . ' | Inicio';
		$data['description'] = $site_info->site_desc;
		$data['keywords'] = $site_info->site_keywords;
		$data['author'] = $site_info->site_author;
		$data['lang'] = $site_info->site_lang;
		$data['charset'] = $site_info->site_charset;

		// Data for Rendering
		$this->load->model('Common_model');
		$data['static_page_info'] = $this->Common_model->get_legal_page_by_static_page_slug($legal_page_slug);
		
		$this->load->view('layout/' . $data['layout'], $data);
	}

	public function partners() {
		// Template Information
		$data['layout'] = 'basic';
		$data['page'] = 'common/partners';

		// Site Information
		$this->load->model('Site_model');
		$data['site_info'] = $this->Site_model->getSiteInformation();
		$data['site_contact'] = $this->Site_model->getSiteContactInformation();
		$site_info = $data['site_info']->result()[0];

		// Navbar Render
		$data['navbar_render'] = $this->Site_model->getPagesNavbarElements();

		// Page Information
		$data['title'] = $site_info->site_name . ' | Inicio';
		$data['description'] = $site_info->site_desc;
		$data['keywords'] = $site_info->site_keywords;
		$data['author'] = $site_info->site_author;
		$data['lang'] = $site_info->site_lang;
		$data['charset'] = $site_info->site_charset;

		// Data for Rendering
		$this->load->model('Common_model');
		$data['partners_info'] = $this->Common_model->get_partners_all();
		
		$this->load->view('layout/' . $data['layout'], $data);
	}

	public function complete_table() {
		// Template Information
		$data['layout'] = 'basic';
		$data['page'] = 'common/complete_position_table';

		// Site Information
		$this->load->model('Site_model');
		$data['site_info'] = $this->Site_model->getSiteInformation();
		$data['site_contact'] = $this->Site_model->getSiteContactInformation();
		$site_info = $data['site_info']->result()[0];

		// Navbar Render
		$data['navbar_render'] = $this->Site_model->getPagesNavbarElements();

		// Page Information
		$data['title'] = $site_info->site_name . ' | Inicio';
		$data['description'] = $site_info->site_desc;
		$data['keywords'] = $site_info->site_keywords;
		$data['author'] = $site_info->site_author;
		$data['lang'] = $site_info->site_lang;
		$data['charset'] = $site_info->site_charset;

		// Data for Rendering
		$this->load->model('Common_model');
		$data['team_all'] = $this->Common_model->get_teams_information();
		
		$this->load->view('layout/' . $data['layout'], $data);
	}

	public function complete_the_club()
	{
		// Template Information
		$data['layout'] = 'basic';
		$data['page'] = 'common/the_club';

		// Site Information
		$this->load->model('Site_model');
		$data['site_info'] = $this->Site_model->getSiteInformation();
		$data['site_contact'] = $this->Site_model->getSiteContactInformation();
		$site_info = $data['site_info']->result()[0];

		// Navbar Render
		$data['navbar_render'] = $this->Site_model->getPagesNavbarElements();

		// Page Information
		$data['title'] = $site_info->site_name . ' | Inicio';
		$data['description'] = $site_info->site_desc;
		$data['keywords'] = $site_info->site_keywords;
		$data['author'] = $site_info->site_author;
		$data['lang'] = $site_info->site_lang;
		$data['charset'] = $site_info->site_charset;

		// Data for Rendering
		$this->load->model('Common_model');
		$data['static_page_info'] = $this->Common_model->get_club_statics_pages();
		
		$this->load->view('layout/' . $data['layout'], $data);
	}

	public function complete_the_pit()
	{
		// Template Information
		$data['layout'] = 'basic';
		$data['page'] = 'common/the_pit';

		// Site Information
		$this->load->model('Site_model');
		$data['site_info'] = $this->Site_model->getSiteInformation();
		$data['site_contact'] = $this->Site_model->getSiteContactInformation();
		$site_info = $data['site_info']->result()[0];

		// Navbar Render
		$data['navbar_render'] = $this->Site_model->getPagesNavbarElements();

		// Page Information
		$data['title'] = $site_info->site_name . ' | Inicio';
		$data['description'] = $site_info->site_desc;
		$data['keywords'] = $site_info->site_keywords;
		$data['author'] = $site_info->site_author;
		$data['lang'] = $site_info->site_lang;
		$data['charset'] = $site_info->site_charset;

		// Data for Rendering
		$this->load->model('Common_model');
		$data['static_page_info'] = $this->Common_model->get_pit_statics_pages();
		
		$this->load->view('layout/' . $data['layout'], $data);
	}
	public function redirect_calendar()
	{
		$actual_month = date('m');
		$actual_year = date('Y');
		$calendar_url = base_url() . 'calendario/' . $actual_year . '/' . $actual_month; 
		redirect($calendar_url,'refresh');
	}
	public function complete_calendar($year, $month)
	{
		// Template Information
		$data['layout'] = 'basic';
		$data['page'] = 'common/complete_calendar';

		// Site Information
		$this->load->model('Site_model');
		$data['site_info'] = $this->Site_model->getSiteInformation();
		$data['site_contact'] = $this->Site_model->getSiteContactInformation();
		$site_info = $data['site_info']->result()[0];

		// Navbar Render
		$data['navbar_render'] = $this->Site_model->getPagesNavbarElements();

		// Page Information
		$data['title'] = $site_info->site_name . ' | Inicio';
		$data['description'] = $site_info->site_desc;
		$data['keywords'] = $site_info->site_keywords;
		$data['author'] = $site_info->site_author;
		$data['lang'] = $site_info->site_lang;
		$data['charset'] = $site_info->site_charset;

		// Data for Rendering
		$prefs = array(
			'show_next_prev'  => TRUE,
	        'start_day'    => 'saturday',
	        'month_type'   => 'long',
			'day_type'     => 'short',
			'show_next_prev'  => TRUE,
			'next_prev_url'   => base_url() . 'calendario/'
		);
		$prefs['template'] = '

		        {table_open}
		        	<table class="table calendar-wrapper-display">
		        {/table_open}

		        {heading_row_start}
		        <thead class="calendar-head-wrapper">
		        	<tr class="calendar-header-wrapper-row">
		        {/heading_row_start}

		        {heading_previous_cell}
		        	<th>&nbsp;</th>
		        	<th>&nbsp;</th>
		        	<th class="calendar-header-wrapper-cell">
		        		<a href="{previous_url}">&lt;&lt;</a>
		        	</th>
		        {/heading_previous_cell}
		        
		        {heading_title_cell}
		        	<th class="calendar-header-wrapper-cell">
		        		{heading}
		        	</th>
		        {/heading_title_cell}
		        
		        {heading_next_cell}
		        	<th class="calendar-header-wrapper-cell">
		        		<a href="{next_url}">&gt;&gt;</a>
		        	</th>
		        	<th>&nbsp;</th>
		        	<th>&nbsp;</th>
		        {/heading_next_cell}

		        {heading_row_end}
		        	</tr>
		        </thead>
		        {/heading_row_end}

		        {week_row_start}
		        	<tr class="calendar-main-wrapper-row">
		        {/week_row_start}
		        
		        {week_day_cell}
		        	<td class="calendar-main-wrapper-cell">
		        		{week_day}
		        	</td>
		        {/week_day_cell}
		        
		        {week_row_end}
		        	</tr>
		        {/week_row_end}

		        {cal_row_start}
		        	<tr class="calendar-cal-wrapper-row">
		        {/cal_row_start}
		        
		        {cal_cell_start}
		        	<td class="calendar-cal-wrapper-cell">
		        {/cal_cell_start}
		        
		        {cal_cell_start_today}
		        	<td class="calendar-cal-wrapper-cell today">
		        {/cal_cell_start_today}
		        
		        {cal_cell_start_other}
		        	<td class="calendar-main-wrapper-cell other">
		        {/cal_cell_start_other}

		        {cal_cell_content}
			        {content}
		        {/cal_cell_content}

		        {cal_cell_content_today}
		        	<div class="event-wrapper content-today">
						<div class="event-row">
							<div class="event-day">{day}</div>
						</div>
			        </div>
		        {/cal_cell_content_today}

		        {cal_cell_no_content}
			        <div class="event-wrapper">
						<div class="event-row">
							<div class="event-day">{day}</div>
						</div>
			        </div>
		        {/cal_cell_no_content}
		        
		        {cal_cell_no_content_today}
			        <div class="event-wrapper no-content-today">
						<div class="event-row">
							<div class="event-day">{day}</div>
						</div>
			        </div>
		        {/cal_cell_no_content_today}

		        {cal_cell_blank}
		        	&nbsp;
		        {/cal_cell_blank}

		        {cal_cell_other}
		        	{day}
		        {/cal_cel_other}

		        {cal_cell_end}
		        	</td>
		        {/cal_cell_end}
		        
		        {cal_cell_end_today}
		        	</td>
		        {/cal_cell_end_today}
		        
		        {cal_cell_end_other}
		        	</td>
		        {/cal_cell_end_other}
		        
		        {cal_row_end}
		        	</tr>
		        {/cal_row_end}

		        {table_close}
		        	</table>
		        {/table_close}
		';

		$dataCalendar = array();

		$this->load->model('Event_model');
		$data['event_data'] = $this->Event_model->get_event_by_year_month($year, $month);
		$data['event_special_data'] = $this->Event_model->get_event_special_by_year_month($year, $month);
		

		foreach ($data['event_data']->result() as $vnt_dt) {
			$originalDate = $vnt_dt->event_date;
			$day_element = date('d', strtotime($originalDate));
			$team_one = get_square_team_data($vnt_dt->event_team_one);
			$team_two = get_square_team_data($vnt_dt->event_team_two);
			
			if ($vnt_dt->event_buy_link != NULL) {
				$buy_option = '<div class="event-buy-ticket">Comprar Entrada</div>';
			}else{
				$buy_option = '';
			}

			$content_obj = '
			<div class="event-wrapper match">
				<a href="' . base_url() . 'eventos/' . $vnt_dt->event_detail_link . '" class="event-row">
					<div class="event-day">' . $day_element . '</div>
					<div class="event-first-team">
						<img src="' . base_url() . 'assets/bucket/team/' . $team_one . '" alt="">
					</div>
					<div class="event-vs">vs</div>
					<div class="event-second-team">
						<img src="' . base_url() . 'assets/bucket/team/' . $team_two . '" alt="">
					</div>
					<div class="event-stadium">' . $vnt_dt->event_stadium . '</div>
					<div class="event-time">' . $vnt_dt->event_time . '</div>	
				</a>
	        </div>';

	        $dataCalendar[$day_element] = $content_obj;
		}

		foreach ($data['event_special_data']->result() as $vnt_spcl_dt) {
			$originalDate = $vnt_spcl_dt->event_date;
			$day_element = date('d', strtotime($originalDate));

			if ($vnt_spcl_dt->event_detail_show == TRUE) {
				$event_special_link_builder = '<div class="event-view"><a href="' . base_url() . 'eventos_especiales/' . $vnt_spcl_dt->event_detail_link . '">Ver Evento</a>' . '</div>';
			}else{
				$event_special_link_builder = '';
			}
				

			$content_obj = '
			<div class="event-wrapper match">
				<div class="event-row">
					<div class="event-day">' . $day_element . '</div>
					<div class="event-title">' . $vnt_spcl_dt->event_title . '</div>
					<div class="event-time">' . $vnt_spcl_dt->event_time . '</div>
					' . $event_special_link_builder . '	
				</div>
	        </div>';

	        $dataCalendar[$day_element] = $content_obj;
		}

		$data['calendar_info'] = $dataCalendar;
		$this->load->library('calendar', $prefs);

		$this->load->model('Common_model');
		$data['static_page_info'] = $this->Common_model->get_pit_statics_pages();
		
		$data['month_value'] = $month;
		$data['year_value'] = $year;
		
		$this->load->view('layout/' . $data['layout'], $data);
	}

	public function event_day($event_slug)
	{
		// Template Information
		$data['layout'] = 'basic';
		$data['page'] = 'common/event_single';

		// Site Information
		$this->load->model('Site_model');
		$data['site_info'] = $this->Site_model->getSiteInformation();
		$data['site_contact'] = $this->Site_model->getSiteContactInformation();
		$site_info = $data['site_info']->result()[0];

		// Navbar Render
		$data['navbar_render'] = $this->Site_model->getPagesNavbarElements();

		// Page Information
		$data['title'] = $site_info->site_name . ' | Inicio';
		$data['description'] = $site_info->site_desc;
		$data['keywords'] = $site_info->site_keywords;
		$data['author'] = $site_info->site_author;
		$data['lang'] = $site_info->site_lang;
		$data['charset'] = $site_info->site_charset;

		// Data for Rendering
		$this->load->model('Event_model');
		$data['event_information'] = $this->Event_model->get_event_by_slug($event_slug);
		
		$this->load->view('layout/' . $data['layout'], $data);
	}

	public function event_special_day($event_slug)
	{
		// Template Information
		$data['layout'] = 'basic';
		$data['page'] = 'common/event_special_single';

		// Site Information
		$this->load->model('Site_model');
		$data['site_info'] = $this->Site_model->getSiteInformation();
		$data['site_contact'] = $this->Site_model->getSiteContactInformation();
		$site_info = $data['site_info']->result()[0];

		// Navbar Render
		$data['navbar_render'] = $this->Site_model->getPagesNavbarElements();

		// Page Information
		$data['title'] = $site_info->site_name . ' | Inicio';
		$data['description'] = $site_info->site_desc;
		$data['keywords'] = $site_info->site_keywords;
		$data['author'] = $site_info->site_author;
		$data['lang'] = $site_info->site_lang;
		$data['charset'] = $site_info->site_charset;

		// Data for Rendering
		$this->load->model('Event_model');
		$data['event_information'] = $this->Event_model->get_event_special_by_slug($event_slug);
		
		$this->load->view('layout/' . $data['layout'], $data);
	}

	public function all_galleries()
	{
		// Template Information
		$data['layout'] = 'basic';
		$data['page'] = 'common/all_gallerie';

		// Site Information
		$this->load->model('Site_model');
		$data['site_info'] = $this->Site_model->getSiteInformation();
		$data['site_contact'] = $this->Site_model->getSiteContactInformation();
		$site_info = $data['site_info']->result()[0];

		// Navbar Render
		$data['navbar_render'] = $this->Site_model->getPagesNavbarElements();

		// Page Information
		$data['title'] = $site_info->site_name . ' | Inicio';
		$data['description'] = $site_info->site_desc;
		$data['keywords'] = $site_info->site_keywords;
		$data['author'] = $site_info->site_author;
		$data['lang'] = $site_info->site_lang;
		$data['charset'] = $site_info->site_charset;

		// Data for Rendering
		$this->load->model('Common_model');
		$this->load->model('Blog_model');
		$data['common_galleries'] = $this->Common_model->get_all_common_galleries();
		$data['mediavideo_galleries'] = $this->Blog_model->get_mediavideo_all_cats();
		
		
		$this->load->view('layout/' . $data['layout'], $data);
	}

	public function get_album_by_id($album_id)
	{
		// Template Information
		$data['layout'] = 'basic';
		$data['page'] = 'common/one_album';

		// Site Information
		$this->load->model('Site_model');
		$data['site_info'] = $this->Site_model->getSiteInformation();
		$data['site_contact'] = $this->Site_model->getSiteContactInformation();
		$site_info = $data['site_info']->result()[0];

		// Navbar Render
		$data['navbar_render'] = $this->Site_model->getPagesNavbarElements();

		// Page Information
		$data['title'] = $site_info->site_name . ' | Inicio';
		$data['description'] = $site_info->site_desc;
		$data['keywords'] = $site_info->site_keywords;
		$data['author'] = $site_info->site_author;
		$data['lang'] = $site_info->site_lang;
		$data['charset'] = $site_info->site_charset;

		// Data for Rendering
		$this->load->model('Common_model');
		$data['common_galleries'] = $this->Common_model->get_all_common_galleries();
		$data['album_photos'] = $this->Common_model->get_all_photos_by_album_id($album_id);
		$data['get_current_album_data'] = $this->Common_model->get_current_album_data($album_id);
		
		
		$this->load->view('layout/' . $data['layout'], $data);
	}

	public function get_video_album_by_cat($video_category)
	{
		// Template Information
		$data['layout'] = 'basic';
		$data['page'] = 'common/one_video_album';

		// Site Information
		$this->load->model('Site_model');
		$data['site_info'] = $this->Site_model->getSiteInformation();
		$data['site_contact'] = $this->Site_model->getSiteContactInformation();
		$site_info = $data['site_info']->result()[0];

		// Navbar Render
		$data['navbar_render'] = $this->Site_model->getPagesNavbarElements();

		// Page Information
		$data['title'] = $site_info->site_name . ' | Inicio';
		$data['description'] = $site_info->site_desc;
		$data['keywords'] = $site_info->site_keywords;
		$data['author'] = $site_info->site_author;
		$data['lang'] = $site_info->site_lang;
		$data['charset'] = $site_info->site_charset;

		// Data for Rendering
		$this->load->model('Common_model');
		$this->load->model('Blog_model');
		$data['get_current_album_data'] = $this->Blog_model->get_mediavideo_cat_by_slug($video_category);
		$data['album_video'] = $this->Blog_model->get_all_mediavideo_by_cat_slug($video_category);

		
		$this->load->view('layout/' . $data['layout'], $data);
	}

	public function add_to_mailchimp_list()
	{
		// Get Configuration Parameters
		$this->load->model('External_model');
		$mailchipm_api_array = $this->External_model->get_mailchimp_api();
		$mailchipm_configuration_array = $this->External_model->get_mailchimp_configuration();
		$mailchimp_apikey = $mailchipm_api_array->result()[0]->api_external_api_code;
		$mailchimp_list = $mailchipm_configuration_array->result()[0]->mailchimp_conf_list_id;
		$mailchimp_url = $mailchipm_configuration_array->result()[0]->mailchimp_conf_api_url;
		// Get Configuration Parameters End

		// Prepare Configuration Schema
		$list_id = $mailchimp_list;
		$authToken = $mailchimp_apikey;
		// Prepare Configuration Schema End

		// Prepare Member Package
		$postData = array(
			"email_address" => $_POST["mail_data"], 
			"status" => "subscribed"
		);
		// Prepare Member Package End

		// Comunication with MailChimp Server
		$ch = curl_init($mailchimp_url . '/3.0/lists/' . $list_id . '/members/');
		curl_setopt_array($ch, array(
			CURLOPT_POST => TRUE,
			CURLOPT_RETURNTRANSFER => TRUE,
			CURLOPT_HTTPHEADER => array(
			    'Authorization: apikey ' . $authToken,
			    'Content-Type: application/json'
			),
			CURLOPT_POSTFIELDS => json_encode($postData)
		));
		$response = curl_exec($ch);
		// Comunication with MailChimp Server End
	}
}
