<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Blog extends CI_Controller {

	public function __construct(){
		parent::__construct();

	}

	// Basic Article
	public function main_blog() {

		// Template Information
		$data['layout'] = 'basic';
		$data['page'] = 'blog/general_list_note';

		// Site Information
		$this->load->model('Site_model');
		$data['site_info'] = $this->Site_model->getSiteInformation();
		$data['site_contact'] = $this->Site_model->getSiteContactInformation();
		$site_info = $data['site_info']->result()[0];

		// Navbar Render
		$data['navbar_render'] = $this->Site_model->getPagesNavbarElements();

		// Page Information
		$data['title'] = $site_info->site_name . ' | Inicio';
		$data['description'] = $site_info->site_desc;
		$data['keywords'] = $site_info->site_keywords;
		$data['author'] = $site_info->site_author;
		$data['lang'] = $site_info->site_lang;
		$data['charset'] = $site_info->site_charset;

		// Data for Rendering
		$this->load->model('Blog_model');
		$data['all_posts'] = $this->Blog_model->get_all_posts();
		$data['title_section'] = 'Noticias';

		$this->load->view('layout/' . $data['layout'], $data);

	}

	public function categorie($categorie_slug) {

		// Template Information
		$data['layout'] = 'basic';
		$data['page'] = 'blog/list_note';

		// Site Information
		$this->load->model('Site_model');
		$data['site_info'] = $this->Site_model->getSiteInformation();
		$data['site_contact'] = $this->Site_model->getSiteContactInformation();
		$site_info = $data['site_info']->result()[0];

		// Navbar Render
		$data['navbar_render'] = $this->Site_model->getPagesNavbarElements();

		// Page Information
		$data['title'] = $site_info->site_name . ' | Inicio';
		$data['description'] = $site_info->site_desc;
		$data['keywords'] = $site_info->site_keywords;
		$data['author'] = $site_info->site_author;
		$data['lang'] = $site_info->site_lang;
		$data['charset'] = $site_info->site_charset;

		// Data for Rendering
		$this->load->model('Blog_model');
		$data['all_posts'] = $this->Blog_model->get_post_by_category_slug($categorie_slug);
		$data['current_category'] = $this->Blog_model->get_post_category_by_slug($categorie_slug);

		$this->load->view('layout/' . $data['layout'], $data);
	}

	public function inner_notice($category, $post_slug) {

		// Template Information
		$data['layout'] = 'basic';
		$data['page'] = 'blog/simple_note';

		// Site Information
		$this->load->model('Site_model');
		$data['site_info'] = $this->Site_model->getSiteInformation();
		$data['site_contact'] = $this->Site_model->getSiteContactInformation();
		$site_info = $data['site_info']->result()[0];

		// Navbar Render
		$data['navbar_render'] = $this->Site_model->getPagesNavbarElements();

		// Page Information
		$data['title'] = $site_info->site_name . ' | Inicio';
		$data['description'] = $site_info->site_desc;
		$data['keywords'] = $site_info->site_keywords;
		$data['author'] = $site_info->site_author;
		$data['lang'] = $site_info->site_lang;
		$data['charset'] = $site_info->site_charset;

		// Data for Rendering
		$this->load->model('Blog_model');
		$data['all_posts'] = $this->Blog_model->get_all_posts();
		$data['current_post'] = $this->Blog_model->get_post_by_slug($post_slug);
		$data['current_post_category'] = $this->Blog_model->get_post_category_by_slug($category);

		$this->load->view('layout/' . $data['layout'], $data);
	}

	// MediaVideo
	public function inner_notice_mediavideo($category, $post_slug) {

		// Template Information
		$data['layout'] = 'basic';
		$data['page'] = 'blog/simple_mediavideo';

		// Site Information
		$this->load->model('Site_model');
		$data['site_info'] = $this->Site_model->getSiteInformation();
		$data['site_contact'] = $this->Site_model->getSiteContactInformation();
		$site_info = $data['site_info']->result()[0];

		// Navbar Render
		$data['navbar_render'] = $this->Site_model->getPagesNavbarElements();

		// Page Information
		$data['title'] = $site_info->site_name . ' | Inicio';
		$data['description'] = $site_info->site_desc;
		$data['keywords'] = $site_info->site_keywords;
		$data['author'] = $site_info->site_author;
		$data['lang'] = $site_info->site_lang;
		$data['charset'] = $site_info->site_charset;

		// Data for Rendering
		$this->load->model('Blog_model');
		$data['all_posts'] = $this->Blog_model->get_all_mediavideos();
		$data['current_post'] = $this->Blog_model->get_mediavideo_by_slug($post_slug);
		$data['current_post_category'] = $this->Blog_model->get_post_category_by_slug($category);

		$this->load->view('layout/' . $data['layout'], $data);
	}

}
