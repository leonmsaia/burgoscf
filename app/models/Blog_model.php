<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Blog_model extends CI_Model
{
	// Video Type Module
	public function get_all_mediavideos()
	{
		$blogdb = $this->load->database('blog_development', TRUE);
		$blogdb->where('post_status', 'publish');
		$blogdb->where('post_type', 'mediavideos');
		$blogdb->order_by('post_date', 'DESC');
		$blogdb->join('wp_term_relationships', 'wp_term_relationships.object_id = wp_posts.ID');
		$blogdb->join('wp_terms', 'wp_terms.term_id = wp_term_relationships.term_taxonomy_id');

		$result = $blogdb->get('wp_posts');
		return $result;
	}
	public function get_mediavideo_by_slug($slug)
	{
		$blogdb = $this->load->database('blog_development', TRUE);
		$blogdb->where('post_name', $slug);
		$blogdb->where('post_status', 'publish');
		$blogdb->where('post_type', 'mediavideos');
		$blogdb->order_by('post_date', 'DESC');
		$blogdb->join('wp_term_relationships', 'wp_term_relationships.object_id = wp_posts.ID');
		$blogdb->join('wp_terms', 'wp_terms.term_id = wp_term_relationships.term_taxonomy_id');

		$result = $blogdb->get('wp_posts');
		return $result;
	}
	public function get_mediavideo_all_cats()
	{
		$blogdb = $this->load->database('blog_development', TRUE);
		$blogdb->where('taxonomy', 'categorias-mediavideo');
		$blogdb->join('wp_terms', 'wp_term_taxonomy.term_id = wp_terms.term_id');

		$result = $blogdb->get('wp_term_taxonomy');
		return $result;
	}
	public function get_mediavideo_cat_by_slug($cat_slug)
	{
		$blogdb = $this->load->database('blog_development', TRUE);
		$blogdb->where('taxonomy', 'categorias-mediavideo');
		$blogdb->where('slug', $cat_slug);
		$blogdb->join('wp_terms', 'wp_term_taxonomy.term_id = wp_terms.term_id');

		$result = $blogdb->get('wp_term_taxonomy');
		return $result;
	}

	public function get_all_mediavideo_by_cat_slug($video_category)
	{
		$blogdb = $this->load->database('blog_development', TRUE);
		$blogdb->where('post_type', 'mediavideos');
		$blogdb->where('post_status', 'publish');
		$blogdb->where('slug', $video_category);
		$blogdb->order_by('post_date', 'DESC');
		$blogdb->join('wp_term_relationships', 'wp_term_relationships.object_id = wp_posts.ID');
		$blogdb->join('wp_terms', 'wp_terms.term_id = wp_term_relationships.term_taxonomy_id');


		$result = $blogdb->get('wp_posts');
		return $result;
	}

	// Post Type Module
	public function get_all_posts()
	{
        $blogdb = $this->load->database('blog_development', TRUE);
		$blogdb->where('post_status', 'publish');
		$blogdb->where('post_type', 'post');
		$blogdb->order_by('post_date', 'DESC');
		$blogdb->join('wp_term_relationships', 'wp_term_relationships.object_id = wp_posts.ID');
		$blogdb->join('wp_terms', 'wp_terms.term_id = wp_term_relationships.term_taxonomy_id');

		$result = $blogdb->get('wp_posts');
		return $result;
	}

	public function get_post_for_hero ($limit)
	{
		$blogdb = $this->load->database('blog_development', TRUE);
		$blogdb->where('post_status', 'publish');
		$blogdb->where('post_type', 'post');
		$blogdb->order_by('post_date', 'DESC');
		$blogdb->limit($limit);
		$blogdb->join('wp_term_relationships', 'wp_term_relationships.object_id = wp_posts.ID');
		$blogdb->join('wp_terms', 'wp_terms.term_id = wp_term_relationships.term_taxonomy_id');

		$result = $blogdb->get('wp_posts');
		return $result;
	}

	public function get_post_by_slug($slug)
	{
		$blogdb = $this->load->database('blog_development', TRUE);
		$blogdb->where('post_name', $slug);
		$blogdb->where('post_status', 'publish');
		$blogdb->where('post_type', 'post');
		$blogdb->order_by('post_date', 'DESC');
		$blogdb->join('wp_term_relationships', 'wp_term_relationships.object_id = wp_posts.ID');
		$blogdb->join('wp_terms', 'wp_terms.term_id = wp_term_relationships.term_taxonomy_id');

		$result = $blogdb->get('wp_posts');
		return $result;
	}

	public function get_post_category_by_slug($slug_category)
	{
		$blogdb = $this->load->database('blog_development', TRUE);
		$blogdb->where('slug', $slug_category);

		$result = $blogdb->get('wp_terms');
		return $result;
	}

	public function get_post_by_category_slug($slug_category)
	{
		$blogdb = $this->load->database('blog_development', TRUE);
		$blogdb->where('post_status', 'publish');
		$blogdb->where('post_type', 'post');
		$blogdb->where('slug', $slug_category);
		$blogdb->order_by('post_date', 'DESC');
		$blogdb->join('wp_term_relationships', 'wp_term_relationships.object_id = wp_posts.ID');
		$blogdb->join('wp_terms', 'wp_terms.term_id = wp_term_relationships.term_taxonomy_id');

		$result = $blogdb->get('wp_posts');
		return $result;
	}

	public function get_home_post_configuration()
	{
		$this->db->where('blog_home_suit_config_id', 1);
		$result = $this->db->get('blog_home_suit_config');
		return $result;
	}

}
