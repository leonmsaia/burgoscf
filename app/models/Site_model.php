<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Site_model extends CI_Model
{
	// Site Information
	public function getSiteInformation()
	{
		$this->db->where('web_conf_id', 1);
		$result = $this->db->get('site_conf');
		return $result;
	}

	public function getSiteContactInformation()
	{
		$this->db->where('site_contact_id', 1);
		$result = $this->db->get('site_contact');
		return $result;
	}

	public function getPagesNavbarElements()
	{
		$this->db->order_by('static_pages_navbar_order', 'ASC');
		$this->db->where('static_pages_navbar_main_element', 1);
		$result = $this->db->get('static_pages_navbar');
		return $result;
	}

}
