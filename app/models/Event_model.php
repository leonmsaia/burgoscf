<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Event_model extends CI_Model
{

  // Commmon Event
  public function get_event_by_year_month($year, $month)
  {
    $this->db->where('event_status', 1);
    $this->db->like('event_date', $year .'-'. $month);
    $result = $this->db->get('event');
    return $result;
  }

  public function get_event_by_slug($slug)
  {
    $this->db->where('event_detail_link', $slug);
    $this->db->where('event_status', 1);
    $result = $this->db->get('event');
    return $result;
  }

  public function get_next_match()
  {
    $today = date('Y-m-d');
    $today_formatter = strtotime($today);
    // $today_limit_formatter = strtotime('+7 day', $today_formatter);
    // $limit = date('Y-m-d', $today_limit_formatter);
    $this->db->where('event_status', 1);
    $this->db->where('event_date >', $today);
    // $this->db->where('event_date <=', $limit);
    $this->db->order_by('event_date', 'ASC');
    // $this->db->limit(1);
    $result = $this->db->get('event');
    return $result;
  }

  // Event Special
  public function get_event_special_by_year_month($year, $month)
  {
    $this->db->where('event_status', 1);
    $this->db->like('event_date', $year .'-'. $month);
    $result = $this->db->get('event_special');
    return $result;
  }

  public function get_event_special_by_slug($slug)
  {
    $this->db->where('event_detail_link', $slug);
    $this->db->where('event_status', 1);
    $result = $this->db->get('event_special');
    return $result;
  }

}
