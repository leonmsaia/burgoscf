<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class External_model extends CI_Model
{

  public function get_mailchimp_api()
  {
    $this->db->where('api_external_id', 1);
    $result = $this->db->get('api_external');
    return $result;
  }

  public function get_mailchimp_configuration()
  {
    $this->db->where('mailchimp_conf_id', 1);
    $result = $this->db->get('mailchimp_conf');
    return $result;
  }


}
