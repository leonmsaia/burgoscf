<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Common_model extends CI_Model
{

  public function get_static_page_by_static_page_slug($slug_static_page)
  {
    $this->db->where('static_pages_type', 1);
    $this->db->where('static_pages_slug', $slug_static_page);
    $result = $this->db->get('static_pages');
    return $result;
  }

  public function get_legal_page_by_static_page_slug($slug_static_page)
  {
    $this->db->where('static_pages_type', 2);
    $this->db->where('static_pages_slug', $slug_static_page);
    $result = $this->db->get('static_pages');
    return $result;
  }

  public function get_partners_all()
  {
    $this->db->where('partners_status', 1);
    $this->db->order_by('partners_level', 'ASC');
    $result = $this->db->get('partners');
    return $result;
  }

  public function get_teams_information()
  {
    $this->db->order_by('team_points', 'DESC');
    $result = $this->db->get('team');
    return $result;
  }

  public function get_horizontal_ads_three_columns()
  {
    $this->db->limit(3);
    $this->db->where('ads_module_category', 3);
    $this->db->where('ads_module_status', 1);
    $this->db->order_by('ads_module_id', 'DESC');
    $result = $this->db->get('ads_module');
    return $result; 
  }

  public function get_horizontal_ads()
  {
    $this->db->limit(3);
    $this->db->where('ads_module_category', 2);
    $this->db->where('ads_module_status', 1);
    $this->db->order_by('ads_module_id', 'DESC');
    $result = $this->db->get('ads_module');
    return $result; 
  }

  public function get_vertical_ads()
  {
    $this->db->limit(1);
    $this->db->where('ads_module_category', 1);
    $this->db->where('ads_module_status', 1);
    $this->db->order_by('ads_module_id', 'DESC');
    $result = $this->db->get('ads_module');
    return $result; 
  }

  public function get_club_stats()
  {
    $this->db->limit(1);
    $this->db->where('main_club_stats_id', 1);
    $this->db->order_by('main_club_stats_id', 'DESC');
    $result = $this->db->get('main_club_stats');
    return $result; 
  }

  public function get_club_statics_pages()
  {
    $this->db->where('static_pages_navbar_parent', 3);
    $this->db->where('static_pages_status', 1);
    $this->db->order_by('static_pages_id', 'ASC');
    $result = $this->db->get('static_pages');
    return $result; 
  }

  public function get_pit_statics_pages()
  {
    $this->db->where('static_pages_navbar_parent', 8);
    $this->db->where('static_pages_status', 1);
    $this->db->order_by('static_pages_id', 'ASC');
    $result = $this->db->get('static_pages');
    return $result; 
  }

  // Galleries
  public function get_all_common_galleries()
  {
    $blogdb = $this->load->database('blog_development', TRUE);
    $blogdb->where('gallery_id', 1);
    $blogdb->order_by('sort', 'ASC');

    $result = $blogdb->get('wp_fg_album');
    return $result;
  }

  public function get_current_album_data($album_id)
  {
    $blogdb = $this->load->database('blog_development', TRUE);
    $blogdb->where('ID', $album_id);

    $result = $blogdb->get('wp_fg_album');
    return $result;
  }

  public function get_all_photos_by_album_id($album_id)
  {
    $blogdb = $this->load->database('blog_development', TRUE);
    $blogdb->where('album_id', $album_id);
    $blogdb->order_by('sort', 'ASC');

    $result = $blogdb->get('wp_fg_media');
    return $result;
  }


}
