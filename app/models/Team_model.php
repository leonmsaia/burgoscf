<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Team_model extends CI_Model
{

  // General Models
  public function getAllPositions()
  {
    $result = $this->db->get('team_player_position');
    return $result;
  }

  public function filter_position($slug_position)
  {
    $this->db->join('team_player_position', 'team_player_position.team_player_position_id = team_player.team_player_id');
    $this->db->where('team_player_position_slug', $slug_position);
    $result = $this->db->get('team_player');
    return $result;
  }

  public function get_position_by_slug_position($slug_position)
  {
    $this->db->where('team_player_position_slug', $slug_position);
    $result = $this->db->get('team_player_position');
    return $result;
  }

  public function get_position_by_position_id($id_position)
  {
    $this->db->where('team_player_position_id', $id_position);
    $result = $this->db->get('team_player_position');
    return $result;
  }

  public function get_player_listed_in_position_by_position_id($position_id)
  {
    $this->db->where('team_player_position', $position_id);
    $result = $this->db->get('team_player');
    return $result;
  }

  public function get_player_information_by_slug($slug_player)
  {
    $this->db->where('team_player_slug', $slug_player);
    $result = $this->db->get('team_player');
    return $result;
  }
  
}
